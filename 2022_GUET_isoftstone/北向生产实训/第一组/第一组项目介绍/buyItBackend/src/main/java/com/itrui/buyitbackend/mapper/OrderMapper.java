package com.itrui.buyitbackend.mapper;

import com.itrui.buyitbackend.pojo.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {

    /**
     * 获取用户订单
     * @param account
     * @return
     */
    public List<Order> getALlOrderByUserAccount(@Param("account") Integer account);

    /**
     * 支付成功后修改用户订单装状态
     * @param order
     * @return
     */
    public int updateOrderStatusById(Order order);

    /**
     * 分类查找
     * @param account
     * @param type
     * @return
     */
    public List<Order> getAllOrderByType(@Param("account") Integer account, @Param("type") String type);

    /**
     * 添加订单
     * @param order
     * @return
     */
    public int addOrder(Order order);

    /**
     * 删除订单通过id
     * @param id
     * @return
     */
    public int delOrderById(@Param("id") Integer id);
}
