package com.itrui.buyitbackend.controller;

import com.itrui.buyitbackend.common.Code;
import com.itrui.buyitbackend.common.Result;
import com.itrui.buyitbackend.pojo.Video;
import com.itrui.buyitbackend.service.VideoService;
import com.itrui.buyitbackend.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/video")
public class VideoController {

    @Autowired
    private VideoService videoService;

    /**
     * 获取全部视频
     * @return
     */
    @GetMapping
    public Result getALlVideos(HttpServletRequest request){
        String url = request.getRequestURL().toString();
        List<Video> aLlVideos = videoService.getALlVideos();
        int indexOf = Util.getIndexOf(url, "/", 3);

        for (int i = 0; i < aLlVideos.size(); i++){
            String _video = aLlVideos.get(i).getVideo();
            String video = url.substring(0,indexOf) + _video;
            aLlVideos.get(i).setVideo(video);
        }

        return new Result(Code.GET_OK,aLlVideos,"查询成功");
    };
}
