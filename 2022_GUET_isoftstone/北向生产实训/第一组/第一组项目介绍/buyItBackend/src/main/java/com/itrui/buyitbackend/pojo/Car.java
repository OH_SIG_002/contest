package com.itrui.buyitbackend.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Car {
    private Integer carId;// '购物车id',

    private Integer carProid      ;//'商品	',
    private Product carPro; //一对一

    private Integer carUserid     ;//'用户',
    private User carUser; //一对一

    private Date carCreatetime;//'创建时间',
    private Integer proNum;//'商品数量',
    private String proBusinessName;
}
