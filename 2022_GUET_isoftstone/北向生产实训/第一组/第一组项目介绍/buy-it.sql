-- 创建数据库表并使用
create schema buy-it;
use buy-it;

-- 地址表
-- auto-generated definition
create table address
(
    address_id     int auto_increment,
    address_name   varchar(10)  not null,
    address_phone  varchar(11)  not null,
    address_main   varchar(200) not null,
    user_account   int          not null,
    address_detail varchar(200) not null,
    constraint address_address_id_uindex
        unique (address_id)
)
    comment '我的地址';

alter table address
    add primary key (address_id);

-- 历史消息表
-- auto-generated definition
create table all_message
(
    message_id        int auto_increment,
    message_sender    int          not null comment '发送人id',
    message_target    int          not null comment '发送目标',
    message_content   varchar(250) null comment '发送消息',
    message_send_time varchar(50)  not null comment '发送时间',
    message_type      int          null comment '消息类型',
    constraint message_receive_message_id_uindex
        unique (message_id)
)
    comment '消息接收表';

alter table all_message
    add primary key (message_id);

-- 黑名单表
-- auto-generated definition
create table blacklist
(
    blacklist_id          int auto_increment,
    blacklist_userid      int not null,
    blacklist_target      int not null,
    blacklist_shield_type int null,
    constraint blacklist_blacklist_id_uindex
        unique (blacklist_id)
)
    comment '黑名单';

alter table blacklist
    add primary key (blacklist_id);

--商家表
-- auto-generated definition
create table business
(
    business_id       int auto_increment comment '商家id',
    business_name     varchar(50) not null comment '商家名',
    business_account  int         not null comment '商家账号',
    business_password varchar(15) not null comment '商家密码',
    constraint business_business_account_uindex
        unique (business_account),
    constraint business_business_id_uindex
        unique (business_id)
)
    comment '商家表';

alter table business
    add primary key (business_id);

-- 购物车表
-- auto-generated definition
create table car
(
    car_id         int auto_increment comment '购物车id',
    car_proid      int      not null comment '商品	',
    car_userid     int      not null comment '用户',
    car_createtime datetime not null comment '创建时间',
    pro_num        int      not null comment '商品数量',
    constraint car_car_id_uindex
        unique (car_id)
)
    comment '购物车';

alter table car
    add primary key (car_id);

-- 收藏表
-- auto-generated definition
create table collection
(
    collection_id         int auto_increment comment '收藏id',
    collection_createtime datetime not null comment '收藏时间',
    collection_userid     int      not null comment '收藏人',
    collection_proid      int      not null comment '收藏商品',
    constraint collection_collection_id_uindex
        unique (collection_id)
)
    comment '收藏表';

alter table collection
    add primary key (collection_id);

--未读消息表
-- auto-generated definition
create table message_receive
(
    mr_id     int auto_increment,
    sender    int          not null comment '发送人id',
    target    int          not null comment '发送目标',
    content   varchar(250) null comment '发送消息',
    send_time varchar(50)  not null comment '发送时间',
    type      int          null comment '消息类型',
    constraint message_receive_mr_id_uindex
        unique (mr_id)
)
    comment '消息接收表';

alter table message_receive
    add primary key (mr_id);

--订单表
-- auto-generated definition
create table `order`
(
    order_id         int auto_increment comment '订单id',
    order_buyer      int                         not null comment '购买人',
    order_createtime datetime                    not null comment '订单创建时间',
    send_add         varchar(100)                null comment '发货地址',
    accp_add         varchar(100)                null comment '收货地址',
    order_status     varchar(5)                  not null comment '订单状态',
    pay_money        decimal(20, 2)              null comment '支付金额',
    remake           varchar(250) default '暂无备注' not null comment '备注',
    order_product    int                         not null comment '订单商品',
    order_send       int                         not null comment '发货人',
    constraint order_order_id_uindex
        unique (order_id)
)
    comment '订单表';

alter table `order`
    add primary key (order_id);

-- 商品表
-- auto-generated definition
create table product
(
    pro_id           int auto_increment comment '商品id',
    pro_type         varchar(10)                      not null comment '商品分类',
    pro_name         varchar(50)                      not null comment '商品名',
    price            decimal(20, 2)                   not null comment '价格',
    stock            int                              not null comment '库存',
    pro_main_picture varchar(200)                     null comment '商品主图',
    pro_picture      int                              null comment '商品图片',
    business         int                              not null comment '商家',
    buyer            int                              null comment '购买人',
    pro_createtime   datetime                         not null comment '发布时间',
    pro_buytime      datetime                         null comment '被购买时间',
    pro_status       varchar(5)   default '在售'        not null,
    introduce        varchar(200) default '该商品没有任何介绍' null,
    constraint product_pro_id_uindex
        unique (pro_id)
)
    comment '商品表';

alter table product
    add primary key (pro_id);

-- 商品图片表
-- auto-generated definition
create table product_picture
(
    pic_id     int auto_increment comment 'id
',
    pic_url    varchar(250) null comment '商品图片url',
    pro_pic_id int          not null comment '商品',
    constraint product_picture_pic_id_uindex
        unique (pic_id)
)
    comment '商品图片表';

alter table product_picture
    add primary key (pic_id);

-- 用户表
-- auto-generated definition
create table user
(
    user_id         int auto_increment comment '用户id',
    username        varchar(10)    default '买为用户13622'                not null comment '用户名',
    user_account    int                                               not null comment '用户账号',
    user_password   varchar(15)                                       not null comment '密码
',
    user_phone      varchar(11)                                       not null comment '联系方式',
    user_email      varchar(20)                                       null comment '用户邮箱',
    head_photo      varchar(250)   default '/images/2022.07.05/3.jpg' null comment '用户头像',
    user_createtime datetime                                          not null comment '创建时间',
    user_address    varchar(20)                                       null comment '收货地址',
    user_role       varchar(10)                                       null comment '角色',
    user_level      int                                               null comment '等级',
    user_introduce  varchar(200)   default '该用户暂无介绍'                  null comment '用户简介',
    user_status     int            default 0                          not null comment '用户状态',
    money           decimal(20, 2) default 0.00                       not null comment '用户余额',
    constraint user_user_account_uindex
        unique (user_account),
    constraint user_user_id_uindex
        unique (user_id)
)
    comment '用户表';

alter table user
    add primary key (user_id);

-- 视频表
-- auto-generated definition
create table video
(
    video_id     int auto_increment,
    like_num     int          default 0                         not null,
    save_num     int          default 0                         not null,
    commentn_num varchar(100)                                   null,
    share_num    int          default 0                         not null,
    like_src     varchar(100) default '/common/icons/like1.png' not null,
    save_src     varchar(100) default '/common/icons/star1.png' not null,
    video_name   varchar(50)                                    not null,
    video_title  varchar(50)                                    not null,
    video        varchar(100)                                   not null,
    constraint video_video_id_uindex
        unique (video_id)
);

alter table video
    add primary key (video_id);
