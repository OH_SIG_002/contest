# 桂林电子科技大学&软通动力OpenHarmony训练营作业



## 简介

欢迎你写下你在OpenHarmony训练营中的收获和心得，在记录下你的OpenHarmony学习之路吧。无论是收获还是吐槽，我们都期待着你的反馈哦！



## 具体提交步骤

学习心得完成后，我们需要通过git的方式上传到我们本次训练营目录下：

特别提示：如果你从未访问过相关gitee网站/git工具使用经验， 请先学习一下FAQ1中提到的相关文档。

1、访问[本仓库地址](https://gitee.com/openharmony-sig/contest/)，登录你的gitee账号，fork本仓库;

2、在你的本地PC上，使用git windows/linux 工具克隆你的个人 knowledge 仓库； 

3、**2022-GUET_and_isoftstone/软通动力培训班或者其他活动培训班目录 下面，文件夹按同学们的分组划分，后续所有的个人作业都可以放置到你们各自的分组里面**

特别注意：文本文件格式是统一用 md 文件， 你可以使用typora 软件来进行相关md文档的编辑。

附带：[Typora一站式使用教程](https://blog.csdn.net/feizuiku0116/article/details/119898062)



4、发起你提交PR；

特别注意：在发起PR 提交前请特别注意FAQ1 中"PR提交文档"中提到的 签署 dco 协议和 添加signed-off信息， 如果这两点中有一点没有完成，仓库的CI门禁会提示dco 检查失败，从而拒绝你的提交。



## FAQ

### 1、我是一个小白，如何在本仓库上提交学习心得？

1）如果你之前从来没有访问过gitee，请参考[gitee 账号创建文档](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/openharmony_getstarted/register_account/readme.md)。

2）如果你有了gitee账号，没有提交过PR，请参考[PR提交文档](https://gitee.com/openharmony-sig/knowledge/tree/master/docs/openharmony_getstarted/push_pr)。与文档中不同的地方只在于你fork 和提交的仓库不一样，我们本次提交的仓库在[这里](https://gitee.com/openharmony-sig/contest)

### 2、我有一个开发样例，怎么贡献给到OpenHarmony知识体系？

如果同学们基于OpenHarmony开发了一些样例，希望更多人看到。 请参考[相关文档](https://gitee.com/openharmony-sig/knowledge/tree/master/docs/openharmony_getstarted/co-helloworld_demos)将样例贡献到OpenHaromny知识体系中来，优秀的样例将获得OpenHarmony官网展示的机会哦！