
### 简述搭建OpenHarmony环境


#### 0.电脑版本的要求
```
Win10,且版本 18362.1049+ 或 18363.1049+
```

#### 1. 安装WLS2

```

1.
以管理员权限 打开 PowerShell

2.
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

3.
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

4.
重启电脑

5.
  点击安装:  wsl_update_x64.msi


6.
    mkdir g:\soft\wsl_ubuntu
    wsl --import Ubuntu20.04 g:\soft\wsl_ubuntu e:\Ubuntu_WSL.tar


```



### 2. 打开WLS2        

```
1. 输入 wsl, 密码是：123456


2. su openharmony 可以切换到openharmony用户
vim /etc/wsl.conf
输入
[user]
default=openharmony
可以永久使用openharmony用户 登录




```


### 3. 安装Docker
```
1. 点击安装docker.exe
  重启电脑

2. 设置docker的内存
在 c/users/<电脑名>/.wslconfig

[wsl2]
memory=4GB
swap=2GB
localhostForwarding=true

3. 注销原有镜像,并重新导入

wsl -l -v --all

wsl --unregister docker-destop
wsl --unregister docker-destop-data


wsl --import docker-destop <新建的docker-destop> <附件的docker-destop>
wsl --import docker-destop-data <新建的docker-destop-data> <附件的docker-destop-data>


4. 重启docker

```


### 3. 操作Docker

```
wsl

sudo docker images 查看所有构建的docker镜像


运行一次创建一个全新的镜像
sudo docker run -it openharmony：V1

也是查看docker 镜像， 没什么特别用
sudo docker images

运行一次创建一个全新的镜像,并且进入镜像
sudo docker run -it openharmony：V1 .


查看当前的docker 容器
sudo docker ps -a


激活docker 容器
sudo docker start id


之后就可以使用VsCode 了

```


### 4. 操作vscode

```
1. 安装插件 remote - container

2. attach to container --- 会打开新的窗口
    就可以打开对应id 的docker 打开

3. 在新窗口打开文件 : 输入 open/openharmony
   就可以进行编码了

```





### 5. 编译

```
1. hb set 可以选择对应的方式--- 只需要选择一次

2. 修改BUILD.gn 文件 可以修改启动的代码

3. hb build -f 正式开始编译
```

### 6. 烧录程序
```
1. 安装烧录的软件
    HiBurm.exe

2. 编译的文件目录

/home/openharmony/out/qihang/qihang/Hi3861_wifiiot_app_allinone.bin

3. 拷贝出来
docker cp id:/home/openharmony/out/qihang/qihang/Hi3861_wifiiot_app_allinone.bin D:/out

4. 正式烧录
1.
左上角 setting 选择 115200 的波特率
2.
type-c 连接板子, 并在HiBurn 中选择对应的 COM
----- 这里可能有驱动的问题,可能需要安装 sscom5131 这个腹肌
3.
选择 Auto burn
4
select file 选择对应的烧制文件 .bin
----------- 上述只需要设置一次----- 正式烧录
delete
add 
Connet
-----注意这个时候可能失败, 可能是MobaXterm 导致的, 需要关闭连接窗口--------
板子复位

.... 开始烧录----
diconnet
打开 MobaXterm
-----------   Sesson -> Serial -> Serial port-> Speed:115200-----
板子复位




```





























