# 【项目名称】：基于openharmony的生物图鉴应用

# 【负责人】 ：庄思杰

## 一、方案介绍
本方案本意是为了普及生物知识，提高人们对于生态环境、物种的保护意识，从而对陆地生物亦或水下生物的保护起到积极作用，缓解许多物种因为人为因素而导致濒危甚至灭绝的情况；
解决问题 14.水下生物 15.陆地生物；
本方案使用九联科技UnionPI开发板进行开发，由服务端以及应用端两部分组成，通过深度学习的方式使用pytorch框架对数据集进行训练，生成训练的模型，之后部署深度学习模型，使能应用能够对图片中的生物进行判别。

## 二、开发板开发环境搭建

 1. [开发板信息](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/UnionpiTiger_helloworld)

 2. [开发环境搭建](https://ost.51cto.com/posts/13294)

## 三、方案实施流程

### 1. 模型训练

本方案采用resnet50深度学习框架对一个大约300张图片的数据集进行训练，

参考资料：https://gitee.com/cluo29/cv/blob/master/code/fruits/fruit1.ipynb

### 2. 服务器搭建

![输入图片说明](image/image4.jpg)

### 3. 应用端通过调用系统相机拍下图片（目前尚未解决）；

### 4. 之后通过调用系统API使用http请求将图片POST到服务器；

![输入图片说明](image/image1.jpg)

### 5. 最后交由服务器计算出结果再传回给应用

![输入图片说明](image/image2.jpg)

![输入图片说明](image/image3.jpg)

## 演示视频
[视频地址](https://www.bilibili.com/video/BV1aU4y1C7Fq?spm_id_from=333.999.0.0&vd_source=0ce05cb97705d5b8d2099203033b1d6e)
