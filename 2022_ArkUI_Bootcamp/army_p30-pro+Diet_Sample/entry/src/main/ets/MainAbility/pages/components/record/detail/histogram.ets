import { HistogramLegend, OneMealStatisticsInfo } from '../../../../model/DataModels'

@Component
export struct Histogram {
    @Consume("dietData") dietData: Array<OneMealStatisticsInfo>
    @BuilderParam content: any
    @BuilderParam legendComponent: any
    private title: string | Resource
    private legend: HistogramLegend[]

    build() {
        Column() {
            Text(this.title)
                .textAlign(TextAlign.Start)
                .fontSize(24)
                .fontColor('#000000')
                .fontFamily('HarmonyHeTi-Medium')
                .width('100%')
                .height(46)

            Stack({ alignContent: Alignment.Bottom }) {
                Column() {
                    ForEach(new Array<number>(6), (item) => {
                        Divider()
                            .strokeWidth(1)
                            .color('#D8D8D8')
                    })
                }
                .height('100%')
                .margin({ top: 20 })
                .justifyContent(FlexAlign.SpaceBetween)

                Column() {
                    Flex({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceEvenly, alignItems: ItemAlign.Start }) {
                        ForEach(this.dietData, (item: OneMealStatisticsInfo) => {
                            if (item.mealFoods.length > 1) {
                                Column() {
                                    this.content(item)
                                    Text(item.mealTime.name).fontSize(14)
                                        .fontColor('#7E7E7E')
                                        .fontFamily('HarmonyHeTi')
                                        .margin({ top: 10 })
                                }
                                .justifyContent(FlexAlign.End)
                                .height('100%')
                            }
                        })
                    }
                }
                .height(236)
            }
            .height(190)

            Row() {
                ForEach(this.legend, item => {
                    Row() {
                        Rect({ width: 9, height: 9, radius: 9 }).fill(item.color).margin({ right: 18 })
                        this.legendComponent(item)
                    }
                })
            }
            .justifyContent(FlexAlign.SpaceEvenly)
            .width('100%')
            .margin({ top: 70 })
        }
        .height('100%')
        .padding({ left: 32, right: 32 })
        .borderRadius(12)
        .backgroundColor('#FFFFFF')
    }
}

