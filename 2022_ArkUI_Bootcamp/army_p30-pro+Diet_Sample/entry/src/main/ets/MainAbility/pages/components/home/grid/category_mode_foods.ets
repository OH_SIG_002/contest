import { FoodInfo, Category } from '../../../../model/type/DietType'
import { getFoods, getFoodCategories } from '../../../../model/data/DataUtil'

import { FoodGrid } from './food_grid'

@Component
export struct CategoryModeFoods {
    @State currentTabIndex: number = 0
    private foodItems: FoodInfo[] = getFoods()
    private foodCategories: Category[] = getFoodCategories()

    @Builder tabBarItemBuilder(value: Resource, index: number) {
        Text(value)
            .fontColor(this.currentTabIndex === index ? 'rgba(0,0,0,0.9)' : 'rgba(0,0,0,0.6)')
            .fontSize(this.currentTabIndex === index ? 24 : 18)
            .margin({ top: 2 })
            .height(56)
    }

    build() {
        Tabs() {
            TabContent() {
                FoodGrid({ foodItems: this.foodItems })
            }.tabBar(this.tabBarItemBuilder($r('app.string.category_all'), 0))

            ForEach(this.foodCategories, (foodCategory: Category, index) => {
                TabContent() {
                    FoodGrid({ foodItems: this.foodItems.filter(item => (item.categoryId === foodCategory.id)) })
                }.tabBar(this.tabBarItemBuilder(foodCategory.name, index + 1))
            })
        }
        .animationDuration(0)
        .barMode(BarMode.Scrollable)
        .onChange((index) => {
            this.currentTabIndex = index
        })
    }
}
