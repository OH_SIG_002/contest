import { FoodInfo } from '../../../model/type/DietType'
import { NutritionElement } from '../../../model/DataModels'

import { CardTitle } from './card_title'

@Component
export struct NutritionPercent {
    private foodInfo: FoodInfo
    private nutritionElements: NutritionElement[]

    build() {
        Column() {
            CardTitle({ title: $r('app.string.nutrition_element'), subtitle: $r('app.string.unit_weight') })

            Row() {
                ForEach(this.nutritionElements, (item: NutritionElement) => {
                    Column() {
                        Stack({ alignContent: Alignment.Center }) {
                            Progress({ value: item.percent, type: ProgressType.Ring })
                                .style({ strokeWidth: 10 })
                                .color(item.color)
                                .margin(4)
                            Text(item.percent + '%').fontSize(17)
                        }

                        Text(item.element)
                            .fontSize(13)
                            .margin({ top: 24 })
                        Text($r('app.string.weight_with_gram_unit', item.weight.toString()))
                            .fontSize(13)
                    }.layoutWeight(1)

                })
            }
            .width('100%')
            .margin({ top: 50 })
        }
        .cardStyle()
    }
}

@Styles function cardStyle () {
    .height('100%')
    .padding({ top: 20, right: 20, left: 20 })
    .backgroundColor(Color.White)
    .borderRadius(12)
}
