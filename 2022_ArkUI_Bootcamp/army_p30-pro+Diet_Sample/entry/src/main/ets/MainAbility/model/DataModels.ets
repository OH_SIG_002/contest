import { MealTimeEnum } from './enum/DietEnum'


/**
 * 用餐实体类
 */
export class MealTime {
    name: Resource
    id: MealTimeEnum

    constructor(id: MealTimeEnum) {
        this.id = id
        switch (id) {
            case MealTimeEnum.Breakfast:
                this.name = $r('app.string.meal_time_breakfast')
                break
            case MealTimeEnum.Lunch:
                this.name = $r('app.string.meal_time_lunch')
                break
            case MealTimeEnum.Dinner:
                this.name = $r('app.string.meal_time_dinner')
                break
            case MealTimeEnum.Supper:
                this.name = $r('app.string.meal_time_supper')
                break
        }
    }
}

/**
 * 饮食记录实体类
 */
export class DietRecord {
    id: number
    foodId: number
    mealTime: MealTime
    weight: number

    constructor(id: number, foodId: number, mealTime: MealTime, weight: number) {
        this.id = id
        this.foodId = foodId
        this.mealTime = mealTime
        this.weight = weight
    }
}

/**
 * 用餐统计信息
 */
@Observed
export class OneMealStatisticsInfo {
    mealTime: MealTime
    mealFoods: Array<MealFoodInfo> = []
    totalCalories: number = 0
    totalFat: number = 0
    totalCarbohydrates: number = 0
    totalProtein: number = 0

    constructor(mealTime: MealTime) {
        this.mealTime = mealTime
    }
}

/**
 * 餐饮信息
 */
export class MealFoodInfo {
    recordId: number
    name: string | Resource
    image: Resource
    calories: number
    protein: number
    fat: number
    carbohydrates: number
    weight: number

    constructor(recordId: number, name: string | Resource, image: Resource, calories: number, protein: number, fat: number, carbohydrates: number, weight: number) {
        this.recordId = recordId
        this.name = name
        this.image = image
        this.calories = calories
        this.protein = protein
        this.fat = fat
        this.carbohydrates = carbohydrates
        this.weight = weight
    }
}

/**
 * 营养元素实体类
 */
export class NutritionElement {
    element: Resource
    weight: number
    percent: number
    beginAngle: number
    endAngle: number
    color: string

    constructor(element: Resource, weight: number, color: string) {
        this.element = element
        this.weight = weight
        this.color = color
    }
}

/**
 * 柱状图实体类
 */
export class HistogramLegend {
    public color: string
    public value: Resource

    constructor(color: string, value: Resource) {
        this.color = color
        this.value = value
    }
}