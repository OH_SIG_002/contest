#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "genki_base.h"
#include "genki_web_plotclock.h"
#include "genki_web.h"
#include "genki_pin.h"
#include "pca9685.h"


static void init_service(void) {
    genki_web_register(newPlotService());

}
static void start(void) {
    IoTIoSetFunc(IOT_IO_NAME_10, IOT_IO_FUNC_10_I2C0_SDA);
    IoTIoSetFunc(IOT_IO_NAME_9, IOT_IO_FUNC_9_I2C0_SCL);
    IoTI2cInit(0, 400000);

    pca9685_servo_reset();
    pca9685_servo_init();

    genki_services_start();
    // 192.168.10.1
    init_service();
}

APP_FEATURE_INIT(start);