#ifndef FKIK_H__
#define FKIK_H__

#include <math.h>

#define L0 25.4
#define L1 40.0
#define L2 60.0

#define DEG 57.295779513082
#define RAD 0.0174532925199

#define PI  3.141592653589793238
float Round(float in,int num);                          // 小数点控制

void FK(float j1,float j2,float j3,float point[]);      // 正解函数
void IK(float x,float y,float z,float Angle[]);         // 逆解函数

#endif
