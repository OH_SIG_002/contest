// 支持基本功能的数据
export const items = [
    {
        text: '浙江',
        children: [
            {id: 1,text: '温州'},
            {id: 2,text: '杭州'}
        ]
    },
    {
        text: '江苏',
        children: [
            {id: 3,text: '南京'},
            {id: 4,text: '苏州'},
            {id: 5,text: '扬州'},
            {id: 6,text: '淮安'}
        ]
    },
    {
        text: '安徽',
        children: [
            {id: 7,text: '合肥'},
            {id: 8,text: '芜湖'},
            {id: 9,text: '淮南'}
        ]
    }
];

// 支持提示信息的数据，设置dot属性后，会在图标右上角展示一个小红点。设置info属性后，会在图标右上角展示相应的徽标
export const dotItems = [
    {
        text: '江苏',
        children: [
            {id: 3,text: '南京'},
            {id: 4,text: '苏州'},
            {id: 5,text: '扬州'}
        ],
        info: "1"
    },
    {
        text: '安徽',
        children: [
            {id: 7,text: '合肥'},
            {id: 8,text: '芜湖'}
        ],
        dot: true
    }
];