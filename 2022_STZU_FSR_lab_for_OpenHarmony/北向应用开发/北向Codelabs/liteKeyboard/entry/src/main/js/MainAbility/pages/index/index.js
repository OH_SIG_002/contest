export default {
    data: {
        inputText:''
    },
    onInit() {
    },
    textClick(e){
        this.inputText = e.detail.text
        console.info('receive text = '+this.inputText)
    },
    showKeyboard(){
        this.$element('keyboard').show()
    },
    deleteKeyboard(){
        this.$element('keyboard').close()
    }

}
