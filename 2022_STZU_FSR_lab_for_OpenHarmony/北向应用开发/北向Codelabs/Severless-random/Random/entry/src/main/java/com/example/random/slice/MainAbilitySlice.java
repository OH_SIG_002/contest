package com.example.random.slice;

import com.example.random.ResourceTable;
import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.AGConnectOptionsBuilder;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.function.AGCFunctionException;
import com.huawei.agconnect.function.AGConnectFunction;
import com.huawei.agconnect.function.FunctionResult;
import com.huawei.hmf.tasks.HarmonyTask;
import com.huawei.hmf.tasks.OnHarmonyCompleteListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;

import java.util.HashMap;
import java.util.Random;

public class MainAbilitySlice extends AbilitySlice {
    private AGConnectFunction function2 ;
    private TextField textField;
    private Text tx;
    private Button btn;
    private int random_num;
    private static final HiLogLabel LABEL = new HiLogLabel(0, 0, "=>JAVA:" );
    @Override
    public void onStart(Intent intent) {

        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        //启动AGC服务
        start();
        //生成随机数
        Random r = new Random();
        random_num = r.nextInt(100);
        //测试控件
        textField = (TextField) findComponentById(ResourceTable.Id_tf);
        btn = (Button) findComponentById(ResourceTable.Id_btn);
        tx= (Text) findComponentById(ResourceTable.Id_tx);

        //绑定事件，点击一次进行数字猜测
        btn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                getWeek(Integer.parseInt(textField.getText()));
                HiLog.info(LABEL, String.valueOf(Integer.parseInt(textField.getText())));
            }

        });

    }

    private void start(){
        //启动函数
        try {
            function2= AGConnectFunction.getInstance();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    private void getWeek(int num) {

        HashMap<String,Integer> map = new HashMap();
        //两个对象，分别对应JS云函数中的 random_num  和 guess_num
        map.put("random_num",random_num);
        map.put("guess_num", num);
        /*
        注意！ 这里的“guess-test” 就是我们在触发器配置信息里面的标识
         */
        function2.wrap("guess-$latest").call(map)
                .addOnCompleteListener(new OnHarmonyCompleteListener<FunctionResult>() {
                    @Override
                    public void onComplete(HarmonyTask<FunctionResult> task) {
                        if (task.isSuccessful()) {
                            //获取返回值，对应JS云函数中的res
                            String value = task.getResult().getValue();
                            HiLog.info(LABEL,value);
                            tx.setText(value);
                            //如果正确 再刷新随机数
                            if(value=="正确"){
                                random_num=new Random().nextInt(100);
                            }

                        } else {
                            Exception e = task.getException();
                            if (e instanceof AGCFunctionException) {
                                AGCFunctionException functionException = (AGCFunctionException) e;
                                int errCode = functionException.getCode();
                                String message = functionException.getMessage();
                                HiLog.error(LABEL,message);
                            }
                            // ...
                        }
                    }
                });
    }
    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
