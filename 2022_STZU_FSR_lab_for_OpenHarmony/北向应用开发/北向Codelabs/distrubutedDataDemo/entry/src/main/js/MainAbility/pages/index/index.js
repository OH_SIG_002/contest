import featureAbility from '@ohos.ability.featureAbility';
import KvStoreModel from '../../model/kv.js';
export default {
    data: {
        kvStoreModel: new KvStoreModel(),
        isReceiveInfo:false,
        changeType:1,
        key:"openharmony",
        testReceiveStr:"nothing"
    },
    onInit() {
        console.log("chw---init start")
        this.grantPermission();
        // type表示操作0：插入数据、1：修改数据：2：删除数据
        this.kvStoreModel.setOnMessageReceivedListener((k, v, type1) => {
            console.log("chw---is listening")
            //传过来是的字符串，要解序列化为 JSON对象
            this.testReceiveStr =v;
            this.isReceiveInfo= true;
            this.changeType=type1;
            console.log("chw---receive data:"+v);
        });
        console.log("chw---init end")
    },
    grantPermission() {
        let context = featureAbility.getContext()
        context.requestPermissionsFromUser(['ohos.permission.DISTRIBUTED_DATASYNC'], 666, function (res) {
            //            console.log("chw---get permission result:"+res)
        })
    },
    send(){
        var that=this;
        //这里注意如果发送数据类型是对象或者数组需要将对象或者数组序列化成 JSON字符串
        this.kvStoreModel.sendMessage(that.key,"消息123");
        console.log("chw---is sending")
    },
}
