
#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio_ex.h"
#include "iot_gpio.h"
#include "iot_uart.h"



intusr_uart2_config(void)
{
    int ret;
//初始化UART2配置，115200，数据bit为8,停止位1，奇偶校验为NONE，流控为NONE
    IotUartAttribute g_uart2_cfg = {115200, 8, 1, IOT_UART_PARITY_NONE, 0,0,0};

    ret = IoTUartInit(2, &g_uart2_cfg);

    if (ret != 0)
    {
        printf("uart2 init fail\r\n");
    }
    return ret;
}

static void* Uart2Demo_Task(const char* arg)
{
    unsigned char buff[50] = {0};
    unsigned int len = 0;

    (void)arg;
    printf("[Uart2Demo] Uart2Demo_Task()\n");
    IoTGpioInit(11);
    IoTGpioInit(12);
    IoSetFunc(11,IOT_IO_FUNC_GPIO_11_UART2_TXD);
    IoSetFunc(12,IOT_IO_FUNC_GPIO_12_UART2_RXD);
    //IoTUartWrite(2, , 50);//数据发送给串口2
    printf("UART2 init...\r\n");
    intusr_uart2_config();
    while(1)

        len = IoTUartRead(2,buff, 50);//数据发送给串口2
        printf(buff);
    }
    return NULL;
}

static void Uart2Demo_Entry(void)
{

    osThreadAttr_t attr = {0};
    printf("[Uart2Demo] Uart2Demo_Entry()\n");

    attr.name="Uart2Demo_Task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024;//堆栈大小
    attr.priority = osPriorityNormal;//优先级
    if (osThreadNew((osThreadFunc_t)Uart2Demo_Task, NULL, &attr) ==NULL)
    {
    printf("[Uart2Demo] Falied to create LedTask!\n");
    }
}

SYS_RUN(Uart2Demo_Entry);