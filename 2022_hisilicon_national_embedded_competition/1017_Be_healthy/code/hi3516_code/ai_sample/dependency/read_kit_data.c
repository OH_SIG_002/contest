#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read_kit_data.h"
// #define PEOPLE_INF_FILE     "./people_file.txt"  // People Inf File
// #define DRUG_INF_FILE       "./drug_file.txt"    // Drug Inf File

PeopleInf data[Users_Num_MAX];
DrugInf   DrugData[Drug_Num_MAX];
void ReadPeopleInf(PeopleInf *data,char *path)
{
    int idTmp,emptyTmp,drugnumTmp;
    FILE * fp;
    char buf[1024];
     if((fp = fopen(path,"rb")) == NULL)
    {
        printf("Can't open PEOPLE_INF file\n");
    }
    for(int i=0;i < Users_Num_MAX; i++)                 
    {   
        if(!feof(fp))
        {
            fgets(buf, sizeof(buf), fp);     //换行
            fscanf(fp,"ID:%d EMPTY:%d ",&idTmp,&emptyTmp);
            data[i].ID = idTmp;
            data[i].Empty = emptyTmp;
            if(!emptyTmp)
            {   
                fscanf(fp,"NAME:%s DRUGNUM:%d ",&data[i].Name,&drugnumTmp); //读取姓名和服药种类   
                if(drugnumTmp!=0)
                {   
                    data[i].DrugNum = drugnumTmp;
                    for(int j=0;j<drugnumTmp;j++)
                    {   
                        fscanf(fp,"DRUGID:%d ",&data[i].DrugID[j]);       //读取药物ID
                        fscanf(fp,"TIMES:%d ",&data[i].Times[j]);         //读取服药次数
                        for(int k=0; k<data[i].Times[j];k++)
                        {
                            fscanf(fp,"%d ",&data[i].DosageTime[j][k]);   //读取服药时间
                        } 
                        fscanf(fp,"TIMEOUT:%d ",&data[i].TimeOut[j]);        //读取超时时间
                    }
                }
            }
            else
            {
                strcpy(data[i].Name,"NULL");
            }
        }
    }
   fclose(fp);
}

void WritePeopleInf(PeopleInf *data,char *path)
{
    FILE * fp;
    char buf[1024];
     if((fp = fopen(path,"wb")) == NULL)
    {
        printf("Can't open PEOPLE_INF file\n");
    }
    fprintf(fp,"%s\n","#People_Information");
    for(int i=0;i < Users_Num_MAX; i++)                 
    {   

        fprintf(fp,"ID:%d EMPTY:%d ",data[i].ID,data[i].Empty);
        if(!data[i].Empty)
        {   
            fprintf(fp,"NAME:%s DRUGNUM:%d ",data[i].Name,data[i].DrugNum); //写入姓名和服药种类   
            if(data[i].DrugNum!=0)
            {   
                for(int j=0;j<data[i].DrugNum;j++)
                {   
                    fprintf(fp,"DRUGID:%d ",data[i].DrugID[j]);       //写入药物ID
                    fprintf(fp,"TIMES:%d ",data[i].Times[j]);         //写入服药次数
                    for(int k=0; k<data[i].Times[j];k++)
                    {
                        fprintf(fp,"%d ",data[i].DosageTime[j][k]);   //写入服药时间
                    } 
                    fprintf(fp,"TIMEOUT:%d",data[i].TimeOut[j]);      //写入超时时间
                }
            }
        }
        else
        {
            fprintf(fp,"NAME:%s",data[i].Name);
        }
        fprintf(fp,"%c\n",';');
    }
   fclose(fp);
}

void ReadDrugInf(DrugInf *data,char *path)
{
    int idTmp,emptyTmp,drugnumTmp;
    FILE * fp;
    char buf[1024];
     if((fp = fopen(path,"rb")) == NULL)
    {
        printf("Can't open DRUG_INF file\n");
    }
    for(int i=0;i < Drug_Num_MAX; i++)                 
    {   
        if(!feof(fp))
        {
            fgets(buf, sizeof(buf), fp);     //换行
            fscanf(fp,"ID:%d EMPTY:%d ",&idTmp,&emptyTmp);
            data[i].ID = idTmp;
            data[i].Empty = emptyTmp;
            if(!emptyTmp)
            {   
                fscanf(fp,"NAME:%s DRUGTOTAL:%d ",&data[i].Name,&data[i].DrugTotal); //读取药名与数量  
            }
            else
            {
                strcpy(data[i].Name,"NULL");
            }
        }
    }
   fclose(fp);
}

void WriteDrugInf(DrugInf *data,char *path)
{
    FILE * fp;
    char buf[1024];
     if((fp = fopen(path,"wb")) == NULL)
    {
        printf("Can't open PEOPLE_INF file\n");
    }
    fprintf(fp,"%s\n","#Drug_Information");
    for(int i=0;i < Drug_Num_MAX; i++)                 
    {   

        fprintf(fp,"ID:%d EMPTY:%d ",data[i].ID,data[i].Empty);
        if(!data[i].Empty)
        {   
            fprintf(fp,"NAME:%s DRUGTOTAL:%d",data[i].Name,data[i].DrugTotal); //写入姓名和服药种类   
        }
        else
        {
            fprintf(fp,"NAME:%s",data[i].Name);
        }
        fprintf(fp,"%c\n",';');
    }
   fclose(fp);
}