#ifndef FACE_DETECT_H
#define FACE_DETECT_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "hi_comm_video.h"
#include "osd_img.h"


#if __cplusplus
extern "C" {
#endif

#define FETIRES_LEN         128
#define NORM_BUF_SIZE       256 // normal buf size


/* ARGB1555 common colors */
#define ARGB1555_RED        0xFC00 // 1 11111 00000 00000
#define ARGB1555_GREEN      0x83E0 // 1 00000 11111 00000
#define ARGB1555_BLUE       0x801F // 1 00000 00000 11111
#define ARGB1555_YELLOW     0xFFE0 // 1 11111 11111 00000
#define ARGB1555_YELLOW2    0xFF00 // 1 11111 11111 00000
#define ARGB1555_WHITE      0xFFFF // 1 11111 11111 11111
#define ARGB1555_BLACK      0x8000 // 1 00000 00000 00000


#define TXT_FONT_SIZE_40      40        
#define TXT_FONT_SIZE_80      80 

/* Information of detected faces */
typedef struct FaceFileInfo {
    
    HI_U32 imgHeight;
    HI_U32 imgWidth;
    char   imgpath[300];
    char   yuvpath[300];
    float  features[FETIRES_LEN];
    HI_BOOL Face_Exist_FLAG;
    char    who[100];
    char  id;       
} FaceFileInfo;

/* load face detect model */
HI_S32 RetinaFaceDetectLoad(uintptr_t* model, OsdSet* osds);

/* unload face detect model */
HI_S32 RetinaFaceDetectUnload(uintptr_t model);

/* face detect calculation */
HI_S32 RetinaFaceDetectCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm);

#ifdef __cplusplus
}
#endif
#endif
