#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //新建四个界面
    page2=new Page2(this);//每个箱子有什么书、几本书,日志
    page1=new Page1(page2);//设备管理-开始工作、停止工作、IP、端口
    page3=new Page3(this);//学生数据库-姓名、学号、图片、。。。
    page4=new Page4(this);//书籍数据库-编码、书名
    //主窗口------------------------------------------------------
    setWindowIcon(QIcon(":/icos/library.ico"));
    setWindowTitle("图书馆智能还书系统");
    QMenuBar* bar=menuBar();//创建一个菜单栏
    setMenuBar(bar);
    QMenu* MainMenu=bar->addMenu("首页");
    QMenu* machineMenu=bar->addMenu("日志");
    QMenu* databaseMenu=bar->addMenu("数据库");

    QAction* studentsAction=databaseMenu->addAction("学生数据库");
    studentsAction->setIcon(QIcon(":/icos/studentdatabase.ico"));
    databaseMenu->addSeparator();//添加分割线
    QAction* booksAction=databaseMenu->addAction("书籍数据库");
    booksAction->setIcon(QIcon(":/icos/bookdatabase.ico"));


    ui->stackedWidget->addWidget(page1);
    ui->stackedWidget->addWidget(page2);
    ui->stackedWidget->addWidget(page3);
    ui->stackedWidget->addWidget(page4);

    ui->stackedWidget->setCurrentWidget(page1);
    connect(MainMenu,&QMenu::aboutToShow,[=](){
        ui->stackedWidget->setCurrentWidget(page1);
    });
    connect(machineMenu,&QMenu::aboutToShow,[=](){
        //page2->paint();
        ui->stackedWidget->setCurrentWidget(page2);
    });
    connect(studentsAction,&QAction::triggered,[=](){
        page3->update();
        ui->stackedWidget->setCurrentWidget(page3);

    });
    connect(booksAction,&QAction::triggered,[=](){
        page4->update();
        ui->stackedWidget->setCurrentWidget(page4);
    });

}

MainWindow::~MainWindow()
{
    delete ui;
}
