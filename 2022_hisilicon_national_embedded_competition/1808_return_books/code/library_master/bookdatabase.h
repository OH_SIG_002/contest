#ifndef BOOKDATABASE_H
#define BOOKDATABASE_H
#include <QObject>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlQueryModel>
#include "student.h"
#include <QBuffer>
#include "customsqlquerymodel.h"
class BookDatabase
{
public:
    BookDatabase();
    QSqlDatabase bookdb;
    QSqlQuery bookquery;
    QSqlQueryModel *bookmodel;
};

#endif // BOOKDATABASE_H
