#ifndef LIBRARYSQLITE_H
#define LIBRARYSQLITE_H

#include <QObject>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>

class LibrarySqlite : public QObject
{
    Q_OBJECT
public:
    explicit LibrarySqlite(QObject *parent = nullptr);
    bool create();
signals:
    QSqlDatabase db;

public slots:
};

#endif // LIBRARYSQLITE_H
