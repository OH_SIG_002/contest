#ifndef PAGE3_H
#define PAGE3_H

#include <QWidget>
#include "QSqlQueryModel"
#include "QTableView"
#include "librarydatabase.h"
#include "student.h"
#include <QDebug>
#include <QPushButton>
#include "dialog.h"
extern libraryDataBase* database;
extern QFont font1,font2,font3,font4;
namespace Ui {
class Page3;
}

class Page3 : public QWidget
{
    Q_OBJECT

public:
    explicit Page3(QWidget *parent = 0);
    void update();
    ~Page3();
    QSqlQueryModel *page3Model;
private:
    Ui::Page3 *ui;
};

#endif // PAGE3_H
