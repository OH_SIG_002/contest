#include "page1dialog.h"
#include "ui_page1dialog.h"

Page1Dialog::Page1Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Page1Dialog)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/icos/wifi.ico"));
    setWindowTitle("网络设置");
    ui->label_ip->setFont(font2);
    ui->label_ip->setText("  IP:");
    ui->label_port->setFont(font2);
    ui->label_port->setText("PORT:");
    ui->lineEdit_ip->setFont(font2);
    ui->lineEdit_ip->setText(ip);
    ui->lineEdit_port->setFont(font2);
    ui->lineEdit_port->setText(port);

    ui->pushButton_cancel->setFont(font2);
    ui->pushButton_cancel->setText("取消");
    ui->pushButton_save->setFont(font2);
    ui->pushButton_save->setText("保存");

    connect(ui->pushButton_save,&QPushButton::clicked,[=](){
        ip=ui->lineEdit_ip->text();
        port=ui->lineEdit_port->text();
        QMessageBox::information(this,"infor","保存成功！",QMessageBox::Ok);
        this->close();
    });

    connect(ui->pushButton_cancel,&QPushButton::clicked,[=](){
        this->close();
        ui->lineEdit_ip->setText(ip);
        ui->lineEdit_port->setText(port);
    });
}

Page1Dialog::~Page1Dialog()
{
    delete ui;
}
