#include "page3.h"
#include "ui_page3.h"

Page3::Page3(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Page3)
{
    ui->setupUi(this);
    ui->tableView->setFont(font2);
    ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
    ui->tableView->setAutoScroll(true);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);


    this->page3Model=database->model;
    //只显示学号和姓名,image用于显示按键
    for(int i=3;i<ui->tableView->colorCount();i++)
    {
        ui->tableView->hideColumn(i);
    }
    ui->tableView->setModel(page3Model);
    for(int i=0;i<ui->tableView->height();i++)
    {
        QPushButton *button = new QPushButton("详情");
        connect(button,&QPushButton::clicked,[=](){
            Dialog* messageDialog=new Dialog(this);
            messageDialog->setID(database->model->data(database->model->index(i,0)).toString());//学号
            messageDialog->setName(database->model->data(database->model->index(i,1)).toString());//姓名
            messageDialog->setPicture(database->model->data(database->model->index(i,2)).toString());//图片
            messageDialog->setMajor(database->model->data(database->model->index(i,3)).toString());//专业
            messageDialog->setGrade(database->model->data(database->model->index(i,4)).toString());//年级
            messageDialog->setBookList(database->getLine(database->model->data(database->model->index(i,0)).toString())->borrowBookList);
            messageDialog->show();
            messageDialog->setAttribute(Qt::WA_DeleteOnClose);
        });
        ui->tableView->setIndexWidget(database->model->index(i,2),button);
    }
    ui->tableView->show();
}
void Page3::update()
{
    this->page3Model=database->model;
    //只显示学号和姓名,image用于显示按键
    for(int i=3;i<ui->tableView->colorCount();i++)
    {
        ui->tableView->hideColumn(i);
    }
    ui->tableView->setModel(page3Model);
    for(int i=0;i<ui->tableView->height();i++)
    {
        QPushButton *button = new QPushButton("详情");
        connect(button,&QPushButton::clicked,[=](){
            Dialog* messageDialog=new Dialog(this);
            messageDialog->setID(database->model->data(database->model->index(i,0)).toString());//学号
            messageDialog->setName(database->model->data(database->model->index(i,1)).toString());//姓名
            messageDialog->setPicture(database->model->data(database->model->index(i,2)).toString());//图片
            messageDialog->setMajor(database->model->data(database->model->index(i,3)).toString());//专业
            messageDialog->setGrade(database->model->data(database->model->index(i,4)).toString());//年级
            messageDialog->setBookList(database->getLine(database->model->data(database->model->index(i,0)).toString())->borrowBookList);
            messageDialog->show();
            messageDialog->setAttribute(Qt::WA_DeleteOnClose);
        });
        ui->tableView->setIndexWidget(database->model->index(i,2),button);
    }
    ui->tableView->show();
}
Page3::~Page3()
{
    delete ui;
}
