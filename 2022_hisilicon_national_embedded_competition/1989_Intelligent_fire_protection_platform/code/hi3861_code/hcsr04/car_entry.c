#include "car_config.h"     // 头文件
#include "car_hcsr04.h"     // 超声波

osThreadId_t thread_id1 = NULL;
// 超声波
hi_s32 distance = 0;


// 任务前初始化
hi_void Car_Task_Init(hi_void)
{
    // GPIO模块初始化
    hi_gpio_init();

    // 超声波 初始设置
    Hcsr04_Init();
}

//  25
static void *DianJi(const char *arg)
{
    (void)arg;
    while (1)
    {
        distance = Hcsr04_Measure();  //传回来的数据是毫米  计划左右检测到30cm右左拐  前方检测到20cm后退
        printf("\n car_hcsr04 [Hcsr04_Measure] distance = %d \n", distance);
    }

    return NULL;
}

// //舵机摇头  25
// static void *CntrolDemo(const char *arg)
// {
//     (void)arg;
//     while (1)
//     {
//         engine_turn1(0);
//         usleep(1000000);
//         engine_turn1(0);
//         usleep(1000000);
//         engine_turn1(0);
//         usleep(1000000);
//     }
//     return NULL;
// }


// //超声波检测障碍物  25
// static void *Car_Task(const char *arg)
// {
//     (void)arg;

//     while (1)
//     {
//         // distance = Hcsr04_Measure();  //传回来的数据是毫米  计划左右检测到30cm右左拐  前方检测到20cm后退
//         //printf("\n car_hcsr04 [Hcsr04_Measure] distance = %d \n", distance);
//         if (distance <= 200)
//         {
//             // osThreadSuspend (thread_id1);
//             // for (int i = 0; i < 50; i++)
//             // {
//                 //printf("\n car_hcsr04 [Hcsr04_Measure] distance = %d \n", distance);
//                 UART(3);
//             // }
//             // osThreadResume (thread_id1);
//         }else{
//             UART(5);
//         }
//     }

//     return NULL;
// }

// //检测到障碍物  25
// static void *ZhangAiWu(const char *arg)
// {
//     (void)arg;
//     printf("11111111111111111111");
//     if (distance <= 600)
//     {
//         UART(3);
//     }else{
//         UART(5);
//     }
//     return NULL;
// }


// 应用入口
static void Car_Entry(void)
{
    // 初始设置
    Car_Task_Init();

    // osThreadAttr_t attr;

    // attr.name = "Car_Task";
    // attr.attr_bits = 0U;
    // attr.cb_mem = NULL;
    // attr.cb_size = 0U;
    // attr.stack_mem = NULL;
    // attr.stack_size = 1024;
    // attr.priority = 25;

    // if(osThreadNew((osThreadFunc_t)Car_Task, NULL, &attr) == NULL)
    // {
    //     printf(" [Car_Task] Falied to create Car_Task! \n");
    // }


    // osThreadAttr_t att;

    // att.name = "CntrolDemo";
    // att.attr_bits = 0U;
    // att.cb_mem = NULL;
    // att.cb_size = 0U;
    // att.stack_mem = NULL;
    // att.stack_size = 1024; /* 堆栈大小为1024 */
    // att.priority = 25;

    // if (osThreadNew((osThreadFunc_t)CntrolDemo, NULL, &att) == NULL) {
    //     printf("[CntrolDemo] Falied to create LedTask!\n");
    // }


    osThreadAttr_t at;

    at.name = "DianJi";
    at.attr_bits = 0U;
    at.cb_mem = NULL;
    at.cb_size = 0U;
    at.stack_mem = NULL;
    at.stack_size = 1024*4; /* 堆栈大小为1024 */
    at.priority = 25;

    if (thread_id1 = osThreadNew((osThreadFunc_t)DianJi, NULL, &at) == NULL) {
        printf("[DianJi] Falied to create LedTask!\n");
    }

    // osThreadAttr_t at22;

    // at22.name = "ZhangAiWu";
    // at22.attr_bits = 0U;
    // at22.cb_mem = NULL;
    // at22.cb_size = 0U;
    // at22.stack_mem = NULL;
    // at22.stack_size = 1024; /* 堆栈大小为1024 */
    // at22.priority = 0;

    // if (osThreadNew((osThreadFunc_t)ZhangAiWu, NULL, &at22) == NULL) {
    //     printf("[ZhangAiWu] Falied to create LedTask!\n");
    // }
}

SYS_RUN(Car_Entry);




