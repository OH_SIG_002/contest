#include "iot_gpio_ex.h"
#include "iot_gpio.h"


typedef unsigned char            u8;
typedef unsigned short int       u16;  
#define ACK	 0
#define	NACK 1               //不应答或否定的应答
#define SA				0x00 //从机地址，单个MLX90614时地址为0x00,多个时地址默认为0x5a
#define RAM_ACCESS		0x00 //RAM access command
#define EEPROM_ACCESS	0x20 //EEPROM access command
#define RAM_TOBJ1		0x07 //To1 address in the eeprom

//#define SMBUS_PORT	    GPIOB
#define SMBUS_SCK		11
#define SMBUS_SDA		12

#define RCC_APB2Periph_SMBUS_PORT		RCC_APB2Periph_GPIOB
//拉高时钟线
#define SMBUS_SCK_H()	    IoTGpioSetOutputVal(SMBUS_SCK, 1)
//拉低时钟线
#define SMBUS_SCK_L()	    IoTGpioSetOutputVal(SMBUS_SCK, 0)
//拉高数据线
#define SMBUS_SDA_H()	    IoTGpioSetOutputVal(SMBUS_SDA, 1)
//拉低数据线
#define SMBUS_SDA_L()	    IoTGpioSetOutputVal(SMBUS_SDA, 0)

#define SMBUS_SDA_PIN()	    SMBUS_PORT->IDR & SMBUS_SDA //读取引脚电平


void SMBus_StartBit(void);
void SMBus_StopBit(void);
void SMBus_SendBit(u8);
u8 SMBus_SendByte(u8);
u8 SMBus_ReceiveBit(void);
u8 SMBus_ReceiveByte(u8);
void SMBus_Delay(u16);
void SMBus_Init(void);
u16 SMBus_ReadMemory(u8, u8);
u8 PEC_Calculation(u8*);
float SMBus_ReadTemp(void);    //获取温度值

