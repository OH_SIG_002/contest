#ifndef _RECEIVEPARTAN_H
#define _RECEIVEPARTAN_H

typedef struct
{
    unsigned int ULCX;
    unsigned int ULCY;
    unsigned int LLCX;
    unsigned int LLCY;

}Bbox;

void* getShareMem(void*);
bool getBoxValue(Bbox& tmp);

#endif
