#include <iot_log.h>

static EnIotLogLevel gIoTLogLevel = EN_IOT_LOG_LEVEL_TRACE;
static const char *gIoTLogLevelNames[] = {
    "TRACE",
    "DEBUG",
    "INFO ",
    "WARN ",
    "ERROR",
    "FATAL"
};

int IoTLogLevelSet(EnIotLogLevel level)
{
    int ret = -1;
    if (level < EN_IOT_LOG_LEVEL_MAX) {
        gIoTLogLevel = level;
        ret = 0;
    }
    return ret;
}

EnIotLogLevel IoTLogLevelGet(void)
{
    return gIoTLogLevel;
}

const char *IoTLogLevelGetName(EnIotLogLevel logLevel)
{
    if (logLevel >= EN_IOT_LOG_LEVEL_MAX) {
        return "NULL ";
    } else {
        return gIoTLogLevelNames[logLevel];
    }
}
