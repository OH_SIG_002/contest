from pickletools import long1
from platform import python_branch
from websocket_server import WebsocketServer
import pyautogui 
x_i=0
y_i=0
numI=0

# Called for every client connecting (after handshake)
def new_client(client, server):
	print("New client connected and was given id %d" % client['id'])
	server.send_message_to_all("Hey all, a new client has joined us")


# Called for every client disconnecting
def client_left(client, server):
	print("Client(%d) disconnected" % client['id'])


# Called when a client sends a message
def message_received(client, server, message):
	if len(message) > 200:
		message = message[:200]+'..'
	print("Client(%d) said: %s" % (client['id'], message))
	str_x=message[0:4]
	str_y=message[4:8]
	x_i=int(str_x)+560
	Str=str_y.replace("-","0")
	y_i=int(Str)+260
	screenWidth,screenHeight=pyautogui.size()
	currenWidth,cureenMouseY=pyautogui.position()
	pyautogui.moveTo(x=x_i,y=y_i+140,duration=0.1,tween=pyautogui.linear)
	# pyautogui.click()
	


PORT=9001
server = WebsocketServer(port = PORT)
server.set_fn_new_client(new_client)
server.set_fn_client_left(client_left)
server.set_fn_message_received(message_received)
server.run_forever()