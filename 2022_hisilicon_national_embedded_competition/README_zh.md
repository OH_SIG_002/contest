# 社区代码提交流程简介
##  简介

欢迎将自己的作品提交到本仓库。相关提交请遵守git的具体操作，并满足各个仓库的具体规范要求。

### 仓库目录介绍

首先在2022-hisilicon-national-embedded-competition目录下，用不同的文件夹来区分不同的组别，文件命名规则为团队编号_作品名称。

![](./media/目录_1.png)

进到每个团队的目录下，我们需要将代码上传到code 目录，还需要撰写README.md 文档和你的作品设计报告。 

**特别注意在OpenHarmony/OpenHarmony-sig 仓库中，相关文档都是以md格式存在的。关于如何编辑md 文档，请参考相关[md编辑指导文档](../docs/怎么编辑Markdown文档/README.md)**

![](./media/目录_2.png)



### git简介

关于git的历史和原理，笔者找了一个介绍比较好的文章，大家可以参考[git原理](https://zhuanlan.zhihu.com/p/66506485)
对于初学者而言，下面的图来理解更直观。
![](./media/git基本流程.png)

+ 1.fork：指的是从官网仓库中复制一份拷贝到自己的账号仓库下，在这个时间节点下两者的内容一致；后续需要不断的手动完成同步；
+ 2.clone:指的是从自己的账号仓库下下载到本地端;
+ 3.commit:指的是将克隆的代码，根据需要修改更正某些内容或者增加新内容、删除冗余内容，形成记录。
+ 4.push:指的是将自己的修改提交到本人账号仓库下；
+ 5.pr:指的是将自己的修改从自己的账号仓库下提交到官方账号仓库下；
+ 6.merge:指的是官方账号仓库的commiter接受了你的修改；
+ 7.fetch:指的是将官方账号仓库的内容拉取到本地。

### git/gitee老鸟点这里

**如果您已经有gitee账号并非常熟悉git的流程和操作，那么你只需要关注以下几点：**

+ <a href='#sign_dco'>邮箱签署DCO]</a>
+ <a href='#add_content'>增加或者修改文件</a>
+ <a href='#sign_off'>commit时需要-s来增加提交时的签名</a>
+ <a href='#comment'>提交PR之后，记得输入sig start build触发CI门禁<a>

### git/gitee新手看这里

**如果你没有操作过git并且上传代码的经验，请按照下文顺序来熟悉提交的具体流程**

+ <a href='#create_account'>创建gitee账号</a>
+ <a href='#bind_email'>账号绑定邮箱</a>
+ <a href='#sign_dco'>邮箱签署DCO</a>
+ <a href='#fork'>Fork官方仓库</a>
+ <a href='#install_gitbash'>安装git客户端gitbash</a>
+ <a href='#set_gitbash'>设置git客户端</a>
+ <a href='#clone'>克隆仓库内容到本地</a>
+ <a href='#add_content'>增加或者修改文件</a>
+ <a href='#sign_off'>签名并提交</a>
+ <a href='#push'>推送本地修改到账号仓库</a>
+ <a href='#pull'>从本人账号仓库向官方仓库发起PR</a>
+ <a href='#comment'>评论触发CI门禁检查</a>
  
  下面本文将按照流程介绍，如果已经熟悉某个流程可以略过。


## 提交作品流程实操

### <a name='create_account'>创建gitee账号</a>
+ 首先你准备一个自己的手机以及邮箱，为方便后续的操作方便，该手机号码以及邮箱没有和gitee平台发生关联。

+ 在[gitee官网](https://gitee.com/)完成注册
  ![](media/gitee官网.png)

+ 根据自己情况设置信息，图里所示个人空间地址很重要，它就是你的用户名。姓名可以是相同的，但用户名是唯一的。
  ![](media/注册信息填写.png)

+ **如无意外则创建成功**
  ![](media/账号创建成功.png)

### <a name='bind_email'>账号绑定邮箱</a>

  在注册账号的时候，有些信息我们是没有补充的，基本信息基本只有电话号和密码，能够满足我们的登录并使用该账号“发言”。那么问题来了，为了让我们后续发言更有说服力，我们需要补充相关的信息。邮箱作为互联网的比较典型的通用信息，需要补充完善。
  ![](./media/个人设置界面.png)
  ![](./media/设置邮箱信息.png)
  按照图示补充邮箱信息，并设置自己的提交邮箱。为自己后续的打怪升级做准备，人民会记住你的贡献的。
  ![](./media/邮箱设置完毕.png)

### <a name='sign_dco'>签署DCO</a>

**这个很重要，一定要签署 ！！**
可以到[DCO查询签署页面](https://dco.openharmony.cn/sign#/check-sign-status)查看一下是否签署，如果没有签署，需要签署。

![](./media/DCO查询签署界面.png)
可先查询自己配置的gitee账号邮箱是否签署：

**特别注意：签署DCO的邮箱要与账号信息补充章节中的邮箱一致**

  ![](./media/DCO未签署.png)
  按照提示完成DCO签署动作
  ![](./media/DCO签署界面.png)
  再次查询自己配置的gitee账号邮箱可以看到已经完成签署
  ![](./media/DCO签署完成.png)
  问题:不签署DCO会有什么问题？

  你在OpenHarmony社区提交的PR会因为流程中的DCO check不通过，导致PR提交失败。
  ![](./media/DCO检查失败.png)

+ 从此以后你就有了一个自己的账号,gitee江湖上已经有了你的身影;但是显然，此时我们这个角色啥也没有，类似刚创建完的游戏角色一样，修行练级之旅已然开始。

  ![](media/交流界面.png)

### <a name='fork'>Fork官方仓库</a>

所谓Fork，就是把官方仓库当前时间点内容搬迁到自己账号下面，直接在网页上操作即可完成。如我们把赛事活动仓库Fork到自己账号下面。
[活动仓库的官方地址](https://gitee.com/openharmony-sig/contest)
![](./media/fork官方仓库.png)

fork之后，在我们的gitee账号就可以看到这个仓库啦。

![](./media/fork到个人账号.png)

作为社区友爱认识，希望能够做到对自己喜欢的仓库一键三连（star/watch/fork）

### <a name='install_gitbash'>安装GIT客户端工具：gitbash</a>
WINDOWS环境下建议大家使用命令行的工具，如果你是MACOS或者Linux,我相信你使用起来会更简单，此处不表，本文仅仅以WINDOWS环境介绍。
可以从[git bash下载地址](https://git-scm.com/download/win)下载git bash工具并安装。
安装完毕之后，在你的工作目录下右键点击即可出现git bash。

&nbsp;![](./media/gitbash启动.png)

点击启动git bash之后会进入一个linux终端的界面，这就是我们后续将修改内容从本地上传到

Gitee上的个人仓库的主要战场了。

&nbsp;![](./media/gitbash界面.png)

一些linux的基本命令（cd：切换目录；cat:参看文件等）都可以在这个界面使用，使用help + 命令可以查看具体的命令的使用方法。请记住，你可以输入命令的前几个字符然后使用tab键补全，毕竟大家的记忆都不是很好。

&nbsp;![](./media/linux命令查看.png)


### <a name='set_gitbash'>设置git客户端</a>
#### 配置SSH公钥
使用SSH公钥可以让你在你的电脑和 Gitee 通讯的时候使用安全连接。
那么怎么获取到我们PC的SSH公钥呢？在桌面右键打开git bash。
![](./media/%E6%89%93%E5%BC%80gitbash.png)

输入**ssh-keygen.exe** 并回车，再次回车，然后输入y，继续回车两次，这样即可生成个人的SSH公钥保持文件。

![](./media/sshkeygen.png)

git无法直接ctrl+c/v实现复制粘贴，但可以鼠标选中ssh公钥保持文件（即Your public key has been saved in 后面的内容）然后右键Copy复制，Paste粘贴实现这个功能。

使用cat命令查看生成的id_rsa.pub文件，输入cat （右键Paste粘贴ssh公钥保持文件）回车即可查看具体信息。

![](./media/id_rsapub.png)
从ssh-rsa开始，整段选中然后复制，打开gitee官网在设置里面找到ssh公钥，粘贴确定即可将公钥添加到我们的gitee账号中。
![](./media/ssh公钥设置.png)
![](./media/添加完毕SSH公钥.png)

#### 配置个人信息
我们向gitee个人仓库提交修改内容，需要告知大家这些修改内容是谁发起提交的，不然大家怎么知道是哪位英雄好汉为开源社区出了力。所以为了避免每次都重复输入一些提交信息（个人账号信息），我们需要使用git bash统一配置一下提交信息。

首先，先记住自己的个人空间地址，在个人主页的网页链接上可看到。

![](./media/个人空间地址.png)

打开git bash，依次输入以下命令并回车，前两个命令没有反应就证明配置成功。

```
git config --global user.name "xxxx"   （配置用户名，xxxx为账号用户名，即个人空间地址）
git config --global user.email "xxxxxx@xxx"   // 与你的gitee 账号邮箱和你签署DCO 的邮箱保持一致即可   
git config --list         （查看配置情况）
```
![](./media/提交信息配置.png)


### <a name='clone'>克隆仓库内容到本地</a>

到个人账号点击并进入这个仓库，进入到clone界面，复制clone的链接地址。

![](./media/进入到clone界面.png)


在git bash工具下面使用git clone命令完成clone动作。

```
git clone git@gitee.com:kenio_zhang/contest.git --depth=1
```
&nbsp;![](./media/clone过程.png)

--depth=1意思是只clone当前仓库最新版本，省去一些历史log，避免仓库历史记录过于庞大花费太多clone时间。需要注意的是开发者需要克隆自己账号下的仓库，原则上这个地址构成如下
```
git clone git@gitee.com:账号名/仓库名.git  --depth=1
```

clone完毕之后，即可在本地目录下看到这个clone的仓库。补充说明一下，本地目录所在位置是根据git bash的位置决定的，比如你在桌面启动git bash，则clone的仓库会出现在桌面。

![](./media/clone完毕.png)



### <a name='add_content'>增加或者修改文件</a>

按照要求增加目录、文件，或者修改部分文件内容。

首先在2022-hisilicon-national-embedded-competition目录下，按照文件命名规则：**团队编号_作品名称** 新建你们团队的个人文件夹。

![](./media/目录_1.png)

进到每个团队的目录下，我们需要将代码上传到**code** 目录，还需要撰写**README.md 文档**和你的**作品设计报告文档**。 

**特别注意在OpenHarmony/OpenHarmony-sig 仓库中，相关文档都是以md格式存在的。关于如何编辑md 文档，请参考相关[md编辑指导文档](../docs/怎么编辑Markdown文档/README.md)**

![](./media/目录_2.png)



### <a name='sign_off'>签名并提交</a>
修改完成并保存之后，然后开始使用git命令查看并提交。此时需要在本地仓库目录下打开git bash，不然git找不到我们修改后的内容。提交的主要步骤如下：

#### 查看修改变更后的文件
```
git  status
```
 ![](./media/查看变更文件.png)



#### <a name='git_lfs'>利用git lfs机制添加特殊文件</a>

**如果在你的提交中不涉及特殊的文件类型，请跳过本步骤**

为了统一方便仓库的管理，OpenHarmony_CI 机制规定特殊的文件类型，例如静/动态库、pdf、docs、PPT等类型的文件，如需上传到OpenHarmony sig 下，需要用git lfs 机制来上传：

```
1、git lfs track 2022-hisilicon-national-embedded-competition/团队编号_作品名称（关键词英文）/code/hi3861_code/test_lfs.a  // 声明该特殊文件到git lfs机制

2、git add .gitattributes // 添加配置文件

3、git add 2022-hisilicon-national-embedded-competition/团队编号_作品名称（关键词英文）/code/hi3861_code/test_lfs.a  // 添加具体特殊文件到暂存区中

4、git lfs ls-files  
// 确认相关test_lfs.a 文件是否已经添加到lfs 机制中。
```

![](./media/git_lfs.png)



#### 将变更文件加入到暂存区

```
git  add *
```
&nbsp;![](./media/添加文件到暂存区.png)
此处的“*”表达的意思是增加所有，为正则表达式的那种,开发者可以根据自己的需要有选择的增加自己想要提交的内容。



#### <a id="signoff">将暂存区内容签名并提交到本地</a>

```
git commit -s -m  "add:知识体系7_10 日心得"
```
**请务必注意commit的时候一定要加-s参数**
```
-s是签名表明这次提交者签名(signoff)，-m是对此次提交行为进行备注，向大家说明你提交修改了什么。
```

&nbsp;![](./media/提交修改到本地仓库并签名.png)

#### 最后我们再用git status查看一下，可看到已没有修改变更内容存在了。

![](./media/无修改内容.png)

### <a name='push'>推送本地修改到账号仓库</a>
1. 完成本地修改提交之后，这个修改内容会保存到本地仓库，并且形成了LOG。LOG是提交记录的意思，所有人在这个仓库内的操作都会形成LOG存储起来，方便随便回退修改。使用git log命令可以查看到我们此次的提交记录。（输入git log命令后无法输入其他命令，同时按ctrl+c就能退出来了。）
```
git log
```
&nbsp;![](./media/提交日志.png)
2. 现在我们需要将本地仓库的修改内容推送到gitee上的个人仓库，使用git push命令来完成这个动作。
```
git push origin master
```
&nbsp;![](./media/推送成功.png)

origin指的是自己的仓库对应的原始远程服务器地址；master标识的是想要提交的分支。
可以使用 git remote -v查看配置的远程服务器；git branch -a查看所有的分支。


3. 进入我们的账号下面，我们查看这个仓库，发现已经发生了变化。

![](./media/账号仓库发生变化.png)


### <a name='pull'>从个人账号仓库下向官方仓库下提交PR</a>

进入个人账号的该仓库下，点击增加PR即可开始提交PR。
![](./media/提交PR入口.png)
![](./media/书写PR内容.png)
提交之后就可以看到我们提交的PR了。
![](./media/提交的PR.png)


### <a name='comment'>增加评论触发CI</a>
**此时已经结束了吗？**

No.我们需要在该PR下增加一个评论，门禁才开始检查（门禁指管理员设置的一系列合规性检查，检测代码是否合规，质量是否合格，是否签署DCO协议）。
![](./media/PR评论.png)
然后我们就可以看到CI的各种合规检查开始进行了，泡杯咖啡，坐等检查通过，然后仓库的管理员来进行合并（PS：有些提交会因为各种原因被管理员拒绝，注意留意管理员回复及时修改重新提交）

![](./media/PR检查.png)

**注意项**

openharmony下面的组织gitee.com/openharmony和gitee.com/openharmony-tpc下仓库的触发CI门禁的评论是"start build"; gitee.com/openharmony-sig为"sig start build"



## 重要FAQ

### 本地推送到个人仓库时出现网络拒绝
如果你在推送修改时，出现了下图的问题：

&nbsp;![](./media/git_push失败.png)

首先我们应该确认你推送的仓库是否为你的私人仓库，如下图所示，通过查看仓库目录下的 .git/config 文件，可以看到remote "origin"项目，其实这个同学推送的是openharmony-sig 官方仓库。（根据ci 配置，我们个人是无法通过git push 的方式往官方仓库推代码的）。 所以我们需要先 **<a href='#fork'>Fork官方仓库</a>**，然后再**<a href='#clone'>克隆仓库内容到本地</a>**。

&nbsp;![](./media/git_config.png)



如果确认是remote "origin"目的仓库是你的个人仓库，那么相关错误的原因在于，你的gitbash 工具的客户端信息没有配置好，请参考**<a href='#set_gitbash'>设置git客户端</a>**章节，完成**SSH公钥**和**配置个人信息**操作。



### 提交个人仓库下的修改到官方仓库出现CI检查DCO失败

dco 检查失败有两种情况，

其中第一种，相关提交没有signoff 信息：

![](./media/dco失败情况1.png)

解决办法：

参考 **<a href='#sign_off'>签名并提交</a>**  章节 , 其中在使用 git commit 命令时必须 加上-s 选项。



其中第二种，是没有签署dco 协议：

![](./media/dco失败情况2.png)

解决办法：

其中提示相关邮箱没有签署dco 协议，在**<a href='#bind_email'>账号绑定邮箱</a>**之后，参考**<a href='#sign_dco'>邮箱签署DCO</a>** 章节，完成签署dco的操作。

**特别注意在 账号"绑定邮箱章节"中使用的 邮箱地址、"签署dco章节" 时使用的邮箱地址、"配置个人信息章节"中配置的邮箱地址必须保持一致 **



### 发起PR后门禁显示合规检测失败

![](./media/合规检查失败.png)



如果你的提交被CI门禁添加了**代码合规检测失败**的标签，可以看到上面的门禁信息提示，你的本次提交中包含二进制文件，需要移除或者通过**<a href='#git_lfs'>git lfs 机制</a>**上传即可。