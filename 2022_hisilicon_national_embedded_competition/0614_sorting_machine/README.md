 通过果蔬分拣机，首先通过TaurusHi3516DV300套件来正确判别水果及蔬菜的类型（苹果、橙子、哈密瓜、土豆等），在判别水果和蔬菜类别的基础上，通过多角度观察来进一步判断水果的新鲜程度，并且与传送带装置进行UART通讯，将水果的种类以及质量信息传送给Hi3861实现对不同类型的水果蔬菜以及根据新鲜程度实现自动分类，目的是将水果蔬菜质量分类的任务由机器视觉来完成，将人力解放出来。

该产品主要用于大型水果批发市场，供卖家使用，不需要单纯的依靠人力来实现对水果种类和质量的判断。通过结合传送带的方式实现全自动分拣。

技术特点：1，采用YOLOv2实现特征提取。 2，可以对水果实现多角度检测，降低水果质量误判的几率。3，也可以利用CNN网络对Yolov2检测到的物体进行分类。