#ifndef __HX711_DEMO_H__
#define __HX711_DEMO_H__

#define LED_TEST_GPIO 9
#define GapValue 430
#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0
#define LED_INTERVAL_TIME_US 3000000

double hi_hx711_task(hi_void);
unsigned long Sensor_Read(void);
double Get_Sensor_Read(void);

#endif