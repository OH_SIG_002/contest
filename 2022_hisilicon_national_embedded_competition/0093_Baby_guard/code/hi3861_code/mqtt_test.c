#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include "ohos_init.h"
#include "cmsis_os2.h"

#include "hi_wifi_api.h"
#include "lwip/ip_addr.h"
#include "lwip/netifapi.h"
#include "lwip/sockets.h"

#include "MQTTClient.h"
#include "mqtt_test.h"
#include "cJSON.h"
#include <hi_stdlib.h>
#include <hi_uart.h>
#include "app_demo_uart.h"
#include <iot_uart.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "iot_gpio.h"
#include "electronic_fence.h"
#include "my_tcpclient.h"

#define LED_TEST_GPIO 9
#define LED_INTERVAL_TIME_US 300000

static MQTTClient mq_client;

unsigned char *onenet_mqtt_buf;
unsigned char *onenet_mqtt_readbuf;
int buf_size;

char *dataRegister = NULL; // register for border frame
char *babyRegister = NULL; // register for baby frame, including head and body
char *baby_frame[8];
char *topic = NULL;
char *postBody = NULL;
int BeiJingTime[6] = {0}, sleepMode = -1, ifSleepModeOn = 0;
int locusResult = -1, locusFlag = -1, eyeStatus[24] = {-1}, detectMode = -1, eyeResult = -1, eyeFlag = -1, preLocus_x = -1, preLocus_y = -1;
int babyFrame[40] = {-2}, borderFrame[4] = {-2, -2, -2, -2}, babyLen = -1;
int bodyRegister[40][2] = {-2}, headRegister[40][2] = {-2}, bodyStoreCnt = 0, lostCnt = 0, headLostCnt = 0;
int asleepCnt = -1, asleepFlag = -1, awakeFlag = -1, ifAwake = 0, babyNum = 0;
hi_bool ifHead = HI_FALSE, ifBody = HI_FALSE;

enum assertionType {flee = 1, wake, lost, error};


int verdictCnt = 0;

UartDefConfig uartDefConfig = {0};

Network n;
MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
MQTTMessage message;

// the message from mqtt server recived will be retained in the msg_data->message->payload
// also, there are some other parameters, such as the length of message(msg_data->message->payloadlen),
//		the active topic(msg_data->topicName->lenstring.data) and its length(msg_data->topicName->lenstring.len)
// ATTENTION: the payload has been stored in the pointer: *dataRegister

/*
	the format of the data:
	12,123,1,23,12,213,-1,-1	//baby_head_upper_left_x, baby_head_upper_left_y, and so on
*/

void assertionSend(int type){
	switch (type)
	{
	case flee:
		message.payload = (void *)"FLEE_ASSERTION";
	message.payloadlen = strlen("FLEE_ASSERTION");
	printf("FLEE_ASSERTION sent!\r\n");
		break;
	case wake:
	message.payload = (void *)"WAKE_ASSERTION";
	message.payloadlen = strlen("WAKE_ASSERTION");
	printf("WAKE_ASSERTION sent!\r\n");
		break;
	case lost:
	message.payload = (void *)"LOST_ASSERTION";
	message.payloadlen = strlen("LOST_ASSERTION");
	printf("LOST_ASSERTION sent!\r\n");
		break;
	case error:
	message.payload = (void *)"ERROR_ASSERTION";
	message.payloadlen = strlen("ERROR_ASSERTION");
	printf("ERROR_ASSERTION sent!\r\n");
		break;
	
	default:
		break;
	}

	if (MQTTPublish(&mq_client, topicAssertion, &message) < 0)
	{
		printf("MQTTPublish faild while asserting the lost message\r\n");
	}

}
void my_uartWrite(void)
{
	int cnt = 0;
	sprintf(dataRegister, "%d,%d,%d,%d,%d,", borderFrame[0], borderFrame[1], borderFrame[2], borderFrame[3], babyNum);
	printf("message sent to taurus vis uart:\r\n");
	do
	{
		IoTUartWrite(DEMO_UART_NUM, dataRegister, strlen(dataRegister));
		printf("%s\r\n", dataRegister);
		cnt++;
	} while (cnt < 3);
}

static void Uart1GpioCOnfig(void)
{
	IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
	IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
}
static void swapArray(int a, int b)
{
	int swap_a = 0, swap_b = 0;

	swap_a = bodyRegister[a + verdictCnt * 5][0];
	swap_b = bodyRegister[a + verdictCnt * 5][1];
	bodyRegister[a + verdictCnt * 5][0] = bodyRegister[b + verdictCnt * 5][0];
	bodyRegister[a + verdictCnt * 5][1] = bodyRegister[b + verdictCnt * 5][1];
	bodyRegister[b + verdictCnt * 5][0] = swap_a;
	bodyRegister[b + verdictCnt * 5][1] = swap_b;
}

void GetMedianNum(void)
{
	int i, j; // 循环变量

	// buble sort
	for (j = 0; j < 5 - 1; j++)
		for (i = 0; i < 5 - j - 1; i++)
			if (bodyRegister[i + verdictCnt * 5][0] > bodyRegister[i + 1 + verdictCnt * 5][0])
				swapArray(i, i + 1);

	for (j = 0; j < 5 - 1; j++)
		for (i = 0; i < 5 - j - 1; i++)
			if (headRegister[i + verdictCnt * 5][0] > headRegister[i + 1 + verdictCnt * 5][0])
				swapArray(i, i + 1);
}
void locusVerdict(void)
{
	// locusResult = 1, means baby has already got up

	if (babyFrame[4 + verdictCnt * 3] == -1 && babyFrame[0 + verdictCnt * 3] == -1)
	{
		locusFlag = -1;
	}
	//abolished: bodyRegister[0 + verdictCnt * 5][0] - bodyRegister[4 + verdictCnt * 5][0] > 40 || headRegister[0 + verdictCnt * 5][0] - headRegister[4 + verdictCnt * 5][0] > 40
	if (abs(preLocus_x - headRegister[2 + verdictCnt * 5][0]) > 100 || abs(preLocus_y - headRegister[2 + verdictCnt * 5][1]) > 50)
	{
		printf("\r\n*got here*\r\n");
		locusResult = 1;
		locusFlag = 0;
	}
	//printf("pre:%d, now:%d, eyeFlag:%d\r\n", preLocus_x, headRegister[2 + verdictCnt * 5][0], eyeFlag);
	preLocus_x = headRegister[2 + verdictCnt * 5][0];
	preLocus_y = headRegister[2 + verdictCnt * 5][1];    

	//#ifdef _LOG_
	printf("locusFlag:%d, locusResult:%d, eyeResult:%d, eyeFlag:%d\r\n", locusFlag, locusResult, eyeResult, eyeFlag);
	//#endif
	if ((locusFlag == -1 && eyeResult == 0 && eyeFlag == 1) || (locusResult == 1 && eyeResult == 0 && eyeFlag == 1) || (eyeFlag = -1 && locusResult == 1))
	{
		assertionSend(wake);
	}
	locusFlag = -1;
	locusResult = -1;
	eyeResult = -1;
	eyeFlag = -1;
}
int eyeVerdict(void)
{
	if (eyeStatus[0 + verdictCnt * 3] == -1)
	{
		eyeFlag = -1;
		return -1;
	}
	//printf("class0: %d, class1:%d\r\n", eyeStatus[verdictCnt * 3 + 0], eyeStatus[verdictCnt * 3 + 1]);
	if (eyeStatus[verdictCnt * 3 + 0] > eyeStatus[verdictCnt * 3 + 1]) // justice if baby got awake
	{
		// printf("eyeVerdict  1 if\r\n");
		awakeFlag = 1;
		ifAwake = 1; // means that the baby has been awake
		eyeResult = 0;
		asleepCnt = 0;
	}
	else if (eyeStatus[verdictCnt * 3 + 0] < eyeStatus[verdictCnt * 3 + 1])
	{
		// printf("eyeVerdict  2 if\r\n");
		asleepCnt++;
		if (asleepCnt == 4)
		{
			asleepFlag = 1;
			ifAwake = 0;
		}
	}

	eyeFlag = 1;

	return 0;
}
void sleepModeSwitch(void)
{
	if (sleepMode)
	{
		printf("\r\n[sleep mode is on]\r\n\r\n");
		ifSleepModeOn = 1;
	}
	else
	{
		printf("\r\n[sleep mode is off]\r\n\r\n");
		ifSleepModeOn = 0;
	}
	my_uartWrite();
	sleepMode = -1;
}

// data process
int borderGet(void)
{
	int length = 0;
	double frameCal = 0;

	// printf("border frame:");
	sscanf(dataRegister, "%d,%d,%d,", &borderFrame[0], &borderFrame[1], &borderFrame[2]);
	// for (int i = 0; i < 3; i++)printf("%d ", borderFrame[i]);
	// printf("\r\n");

	length = borderFrame[2];
	borderFrame[2] = borderFrame[0] + length;
	borderFrame[3] = borderFrame[1] + length;

	frameCal = (borderFrame[0] - 38.0) / 38 * 120 + 0.5;
	borderFrame[0] = (int)frameCal;
	frameCal = (borderFrame[2] - 38.0) / 38 * 120 + 0.5;
	borderFrame[2] = (int)frameCal;
	frameCal = (borderFrame[1] - 23.0) / 38 * 120 + 0.5;
	borderFrame[1] = (int)frameCal;
	frameCal = (borderFrame[3] - 23.0) / 38 * 120 + 0.5;
	borderFrame[3] = (int)frameCal;

	return 0;
}
int babyGet(void)
{
	ifHead = HI_FALSE;
	ifBody = HI_FALSE;
	int s[24] = {-2};

	//printf("baby frame:");
	if (babyNum == 1)//-1,-1,-1,-1,;-1,-1,-1,-1,;-1,-1,-1,
		sscanf(babyRegister, "%d,%d,%d,%d,;%d,%d,%d,%d,;%d,%d,%d,", &babyFrame[0], &babyFrame[1],
			   &babyFrame[2], &babyFrame[3], &babyFrame[4], &babyFrame[5], &babyFrame[6], &babyFrame[7], s, s + 1, s + 2);
	else if (babyNum == 2)
		sscanf(babyRegister, "%d,%d,%d,%d,%d,%d,%d,%d,;%d,%d,%d,%d,%d,%d,%d,%d,;%d,%d,%d,%d,%d,%d,", &babyFrame[0], &babyFrame[1],
			   &babyFrame[2], &babyFrame[3], &babyFrame[4], &babyFrame[5], &babyFrame[6], &babyFrame[7], &babyFrame[8], &babyFrame[9],
			   &babyFrame[10], &babyFrame[11], &babyFrame[12], &babyFrame[13], &babyFrame[14], &babyFrame[15], s, s + 1, s + 2, s + 3, s + 4, s + 5);
	else if (babyNum == 3)
		sscanf(babyRegister, "%d,%d,%d,%d,%d,%d,%d,%d,;%d,%d,%d,%d,%d,%d,%d,%d,;%d,%d,%d,%d,%d,%d,%d,%d,%d,", &babyFrame[0], &babyFrame[1],
			   &babyFrame[2], &babyFrame[3], &babyFrame[4], &babyFrame[5], &babyFrame[6], &babyFrame[7], &babyFrame[8], &babyFrame[9],
			   &babyFrame[10], &babyFrame[11], &babyFrame[12], &babyFrame[13], &babyFrame[14], &babyFrame[15], s, s + 1, s + 2, s + 3, s + 4, s + 5, s + 6, s + 7, s + 8);
	else if (babyNum == 4)
		sscanf(babyRegister, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,;%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,;%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,", &babyFrame[0], &babyFrame[1],
			   &babyFrame[2], &babyFrame[3], &babyFrame[4], &babyFrame[5], &babyFrame[6], &babyFrame[7], &babyFrame[8], &babyFrame[9],
			   &babyFrame[10], &babyFrame[11], &babyFrame[12], &babyFrame[13], &babyFrame[14], &babyFrame[15], &babyFrame[16], &babyFrame[17],
			   &babyFrame[18], &babyFrame[19], &babyFrame[20], &babyFrame[21], &babyFrame[22], &babyFrame[23], s, s + 1, s + 2, s + 3, s + 4, s + 5, s + 6, s + 7, s + 8, s + 9, s + 10, s + 11);

	//printf("babyRegister:%s\r\n", babyRegister);

	//printf("s[0~2]:%d %d %d\r\n", s[0], s[1], s[2]);
	for (int i = 0; i < 3; i++)
		if (!(s[2 + i * 3] > s[1 + i * 3] && s[2 + i * 3] > s[0 + i * 3]))
		{
			eyeStatus[0 + i * 3] = s[0 + i * 3];
			eyeStatus[1 + i * 3] = s[1 + i * 3];
			eyeStatus[2 + i * 3] = s[2 + i * 3];
		}
	//printf("eyestatus[0~5]:%d %d %d %d %d %d\r\n", eyeStatus[0], eyeStatus[1], eyeStatus[2], eyeStatus[3], eyeStatus[4], eyeStatus[5]);

	/* for (int i = 0; i < 8; i++)
		printf("%d ", babyFrame[i]);

	printf("\r\n"); */
	/* printf("babyRegister: %.*s\r\n", babyLen, babyRegister);
	for (int  i = 0; i < 8; i++)
	{
		printf("%d ", babyFrame[i]);
	}
	printf("\r\n");
 */
	if (babyFrame[0] != -1)
		ifHead = HI_TRUE;
	if (babyFrame[0 + babyNum * 4 + 1] != -1)
		ifBody = HI_TRUE;

	if (babyNum == 1)
	{
		bodyRegister[bodyStoreCnt][0] = (babyFrame[4] + babyFrame[6]) / 2;
		bodyRegister[bodyStoreCnt][1] = (babyFrame[5] + babyFrame[7]) / 2;
		headRegister[bodyStoreCnt][0] = (babyFrame[0] + babyFrame[2]) / 2;
		headRegister[bodyStoreCnt][1] = (babyFrame[1] + babyFrame[3]) / 2;
	}
	else if (babyNum == 2)
	{
		bodyRegister[bodyStoreCnt][0] = (babyFrame[8] + babyFrame[10]) / 2;
		bodyRegister[bodyStoreCnt][1] = (babyFrame[9] + babyFrame[11]) / 2;
		headRegister[bodyStoreCnt][0] = (babyFrame[0] + babyFrame[2]) / 2;
		headRegister[bodyStoreCnt][1] = (babyFrame[1] + babyFrame[3]) / 2;
		bodyRegister[bodyStoreCnt + 5][0] = (babyFrame[12] + babyFrame[14]) / 2;
		bodyRegister[bodyStoreCnt + 5][1] = (babyFrame[13] + babyFrame[15]) / 2;
		headRegister[bodyStoreCnt + 5][0] = (babyFrame[4] + babyFrame[6]) / 2;
		headRegister[bodyStoreCnt + 5][1] = (babyFrame[5] + babyFrame[7]) / 2;
		
	}
	else if (babyNum == 3)
	{
		bodyRegister[bodyStoreCnt][0] = (babyFrame[12] + babyFrame[14]) / 2;
		bodyRegister[bodyStoreCnt][1] = (babyFrame[13] + babyFrame[15]) / 2;
		headRegister[bodyStoreCnt][0] = (babyFrame[0] + babyFrame[2]) / 2;
		headRegister[bodyStoreCnt][1] = (babyFrame[1] + babyFrame[3]) / 2;
		bodyRegister[bodyStoreCnt + 5][0] = (babyFrame[12 + 4] + babyFrame[14 + 4]) / 2;
		bodyRegister[bodyStoreCnt + 5][1] = (babyFrame[13 + 4] + babyFrame[15 + 4]) / 2;
		headRegister[bodyStoreCnt + 5][0] = (babyFrame[0 + 4] + babyFrame[2 + 4]) / 2;
		headRegister[bodyStoreCnt + 5][1] = (babyFrame[1 + 4] + babyFrame[3 + 4]) / 2;
		bodyRegister[bodyStoreCnt + 10][0] = (babyFrame[12 + 4 * 2] + babyFrame[14 + 4 * 2]) / 2;
		bodyRegister[bodyStoreCnt + 10][1] = (babyFrame[14 + 4 * 2] + babyFrame[15 + 4 * 2]) / 2;
		headRegister[bodyStoreCnt + 10][0] = (babyFrame[0 + 4 * 2] + babyFrame[2 + 4 * 2]) / 2;
		headRegister[bodyStoreCnt + 10][1] = (babyFrame[1 + 4 * 2] + babyFrame[3 + 4 * 2]) / 2;
	}
	else if (babyNum == 4)
	{
		bodyRegister[bodyStoreCnt][0] = (babyFrame[16] + babyFrame[18]) / 2;
		bodyRegister[bodyStoreCnt][1] = (babyFrame[17] + babyFrame[19]) / 2;
		headRegister[bodyStoreCnt][0] = (babyFrame[0] + babyFrame[2]) / 2;
		headRegister[bodyStoreCnt][1] = (babyFrame[1] + babyFrame[3]) / 2;
		bodyRegister[bodyStoreCnt + 5][0] = (babyFrame[16 + 4] + babyFrame[18 + 4]) / 2;
		bodyRegister[bodyStoreCnt + 5][1] = (babyFrame[17 + 4] + babyFrame[19 + 4]) / 2;
		headRegister[bodyStoreCnt + 5][0] = (babyFrame[0 + 4] + babyFrame[2 + 4]) / 2;
		headRegister[bodyStoreCnt + 5][1] = (babyFrame[1 + 4] + babyFrame[3 + 4]) / 2;
		bodyRegister[bodyStoreCnt + 10][0] = (babyFrame[16 + 4 * 2] + babyFrame[18 + 4 * 2]) / 2;
		bodyRegister[bodyStoreCnt + 10][1] = (babyFrame[17 + 4 * 2] + babyFrame[19 + 4 * 2]) / 2;
		headRegister[bodyStoreCnt + 10][0] = (babyFrame[0 + 4 * 2] + babyFrame[2 + 4 * 2]) / 2;
		headRegister[bodyStoreCnt + 10][1] = (babyFrame[1 + 4 * 2] + babyFrame[3 + 4 * 2]) / 2;
		bodyRegister[bodyStoreCnt + 15][0] = (babyFrame[16+ 4 * 3] + babyFrame[18 + 4 * 3]) / 2;
		bodyRegister[bodyStoreCnt + 15][1] = (babyFrame[17 + 4 * 3] + babyFrame[19 + 4 * 3]) / 2;
		headRegister[bodyStoreCnt + 15][0] = (babyFrame[0 + 4 * 3] + babyFrame[2 + 4 * 3]) / 2;
		headRegister[bodyStoreCnt + 15][1] = (babyFrame[1 + 4 * 3] + babyFrame[3 + 4 * 3]) / 2;
	}

	bodyStoreCnt++;
	/* if (bodyStoreCnt == 4 && babyNum == 1)
		bodyStoreCnt = 0;
	else if (bodyStoreCnt == 9 && babyNum == 2)
		bodyStoreCnt = 0;
	else if (bodyStoreCnt == 14 && babyNum == 3)
		bodyStoreCnt = 0;
	else if (bodyStoreCnt == 19 && babyNum == 4)
		bodyStoreCnt = 0; */
		if(bodyStoreCnt == 4)
		bodyStoreCnt = 0;

	return 0;

}

int borderVerdict(void)
{
	//printf("ifNody:%d, head:%d\r\n", ifBody, babyFrame[0 + verdictCnt * 4]);
	if (ifBody)
	{
		if (babyFrame[0 + verdictCnt * 4] != -1)
		{
			headLostCnt++;
			if (headLostCnt == 3)
			{
				assertionSend(error);
			}
			return -1;
		}
		if (fenceJudge(borderFrame[0], borderFrame[1], borderFrame[2], borderFrame[3], bodyRegister[2 + verdictCnt * 5][0], bodyRegister[2 + verdictCnt * 5][1]) && borderFrame[0] != -2)
		{
			assertionSend(flee);	
			return 1;	
		}
		
	}
	else if (babyFrame[0 + verdictCnt * 4] != -1 && borderFrame[0] != -2)
	{
		if (fenceJudge(borderFrame[0], borderFrame[1], borderFrame[2], borderFrame[3], (babyFrame[0 + verdictCnt * 4] + babyFrame[2 + verdictCnt * 4]) / 2, (babyFrame[1 + verdictCnt * 4] + babyFrame[3 + verdictCnt * 4]) / 2))
		{
			assertionSend(flee);
			return 1;
		}
	}
	else
	{
		lostCnt++;
		if (lostCnt == 4)
		{
			printf("sending lost assertion\r\n");
			assertionSend(lost);
			lostCnt = 0;
		}
		return 2;
	}
	
	return 0;
}

//消息回调函数
void mqtt_callback(MessageData *msg_data)
{
	size_t res_len = 0;

	LOS_ASSERT(msg_data);

	/* 	printf("\n****New Message****\n");
		printf("topic:\t%.*s\r\n", msg_data->topicName->lenstring.len, msg_data->topicName->lenstring.data);
		printf("message:%.*s\r\n", msg_data->message->payloadlen, msg_data->message->payload);
		printf("******EOF******\n");
	*/
	sscanf(msg_data->topicName->lenstring.data, "/ohos/%s", topic);
	//printf("topic: %s\r\n", topic);
	if (topic[0] == 's')
	{
		sprintf(dataRegister, "%.*s", msg_data->message->payloadlen, msg_data->message->payload);
		borderGet();
		my_uartWrite();
	}
	else if (topic[0] == 't')// time format 2022022-07-03, 21:06:18
	{
		sscanf(msg_data->message->payload, "%d-%d-%d, %d:%d:%d", &BeiJingTime[0], &BeiJingTime[1], &BeiJingTime[2], &BeiJingTime[3], &BeiJingTime[4], &BeiJingTime[5]);
		// printf("%d-%d-%d, %d:%d:%d\r\n", BeiJingTime[0], BeiJingTime[1], BeiJingTime[2], BeiJingTime[3], BeiJingTime[4], BeiJingTime[5]);
	}
	else if (topic[0] == 'p')
	{
		sscanf(msg_data->message->payload, "sleepMode:%1d", &sleepMode);
		//printf("msg:%s\r\n", msg_data->message->payload);
		sleepModeSwitch();
		//sleepMode = -1;
	}
	else if (topic[0] == 'd')
	{
		sscanf(msg_data->message->payload, "detectMode:%1d", &detectMode);
		if (detectMode == 0)
		{
			printf("detect mode off\r\n");
		}
		else if (detectMode == 1)
		{
			printf("detect mode on\r\n");
		}
	}
	else if (topic[0] == 'b')
	{
		sscanf(msg_data->message->payload, "babyNum:%1d", &babyNum);
		printf("baby number refresh:%d\r\n", babyNum);
		my_uartWrite();
	}
}

int mqtt_connect(void)
{
	hi_u8 uartBuff[230] = {0};
	int rc = -3;

	// uart init
	hi_u32 ret = 0;
	IoTGpioInit(LED_TEST_GPIO);
	IoTGpioSetDir(LED_TEST_GPIO, IOT_GPIO_DIR_OUT);
	IotUartAttribute uartAttr = {
		.baudRate = 115200, /* baudRate: 115200 */
		.dataBits = 8,		/* dataBits: 8bits */
		.stopBits = 1,		/* stop bit */
		.parity = 0,
	};
	/* Initialize uart driver */
	ret = IoTUartInit(DEMO_UART_NUM, &uartAttr);
	if (ret != HI_ERR_SUCCESS)
	{
		printf("Failed to init uart! Err code = %d\n", ret);
		return -1;
	}
	// Uart1GpioCOnfig();
	IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
	IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
	printf("UART: inited!\r\n");

	// mqtt init
	NetworkInit(&n);
	NetworkConnect(&n, "hydpp.top", 1883);

	buf_size = 4096 + 2048;
	onenet_mqtt_buf = (unsigned char *)malloc(buf_size);
	onenet_mqtt_readbuf = (unsigned char *)malloc(buf_size);

	dataRegister = (char *)malloc(sizeof(char *) * 200);
	babyRegister = (char *)malloc(sizeof(char *) * 200);
	topic = (char *)malloc(sizeof(char *) * 50);

	if (!(onenet_mqtt_buf && onenet_mqtt_readbuf))
	{
		printf("No memory for MQTT client buffer!");
		return -2;
	}

	MQTTClientInit(&mq_client, &n, 1000, onenet_mqtt_buf, buf_size, onenet_mqtt_readbuf, buf_size);

	MQTTStartTask(&mq_client);

	data.keepAliveInterval = 2;
	data.cleansession = 1;
	data.clientID.cstring = "ohos_hi3861";
	data.username.cstring = "admin";
	data.password.cstring = "public";
	data.cleansession = 1;

	mq_client.defaultMessageHandler = mqtt_callback;

	//连接服务器
	while(MQTTConnect(&mq_client, &data) != 0){
		IoTGpioSetOutputVal(LED_TEST_GPIO, 1);
    	usleep(LED_INTERVAL_TIME_US);
    	IoTGpioSetOutputVal(LED_TEST_GPIO, 0);
    	usleep(LED_INTERVAL_TIME_US);
	}
	printf("MQTT: connected!\r\n");

	//订阅消息，并设置回调函数
	MQTTSubscribe(&mq_client, topicSub, 0, mqtt_callback);

	// babyRegister = "11,12,13,14,21,22,23,24,";
	while (1)
	{
		uartDefConfig.g_uartLen = IoTUartRead(DEMO_UART_NUM, uartBuff, 100);
		if ((uartDefConfig.g_uartLen > 0) && (uartBuff[0] == 0xaa) && (uartBuff[1] == 0x55))
		{
			babyLen = uartDefConfig.g_uartLen - 2;
			memcpy(babyRegister, uartBuff + 2, babyLen);
			// printf("%s\r\n", babyRegister);
			babyGet(); // get the frame data from string
			for (verdictCnt = 0; verdictCnt < babyNum; verdictCnt++)
			{
				if (ifSleepModeOn)
				{
					eyeVerdict();
					locusVerdict();
				}
				else
					locusResult = -1;
				GetMedianNum(); // filter, must be behind the ifSleepMode as well as front of the detectMode
				if (detectMode == 1)
				{
					rc = borderVerdict();
					if (rc == 1)
					{
						printf("[%d-%02d-%02d, %02d:%02d]out of border\r\n", BeiJingTime[0], BeiJingTime[1], BeiJingTime[2], BeiJingTime[3], BeiJingTime[4]);
					}
					else if (rc == 0)
					{
						printf("OK\r\n");
					}
					else if(rc == -1){
						printf("error\r\n");
					}
					else if(rc == 2){
						printf("failed to find the baby\r\n");
					}
				}
			}

			// printf("%d\r\n", borderVerdict()); // verdict if the baby get out the border
		}
		usleep(4 * 10 * 1000);
	}

	return 0;
}

void mqtt_test(void)
{
	mqtt_connect();
}
