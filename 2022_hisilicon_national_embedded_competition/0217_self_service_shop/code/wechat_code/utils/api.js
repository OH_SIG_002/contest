var Promise = require('./Promise');
var Util = require('./util');

var app = getApp();

var FETCH_URL = 'http://59.110.237.148:6601/';
var PLATID = /android/i.test(wx.getSystemInfoSync().system) ? 1 : 0;



//创建一个空投
function GetHomeCakeData(data) {
  return new Promise((resolve, reject) => {
    app.request({
      url: FETCH_URL + 'index',
      data: data,
      method: "GET",
      header: {
        'content-type': 'application/json'
      },
      success: resolve,
      fail: reject,
      complete: function(res) {
        // 处理request:fail的客户端bug
        if (res.errMsg && /^request:fail/.test(res.errMsg)) {
          Util.handleRequestError(res);
        }
      }
    })
  })
}

// 加入购物车
function AddShoppingCart(data) {
  return new Promise((resolve, reject) => {
    app.request({
      url: FETCH_URL + 'cart',
      data: data,
      method: "POST",
      header: {
        'content-type': 'application/json'
      },
      success: resolve,
      fail: reject,
      complete: function(res) {
        // 处理request:fail的客户端bug
        if (res.errMsg && /^request:fail/.test(res.errMsg)) {
          Util.handleRequestError(res);
        }
      }
    })
  })
}

// 用户注册
function UserRegister(data) {
  return new Promise((resolve, reject) => {
    app.request({
      url: FETCH_URL + 'user/register',
      data: data,
      method: "POST",
      header: {
        'content-type': 'application/json'
      },
      success: resolve,
      fail: reject,
      complete: function(res) {
        // 处理request:fail的客户端bug
        if (res.errMsg && /^request:fail/.test(res.errMsg)) {
          Util.handleRequestError(res);
        }
      }
    })
  })
}
// 用户登录
function UserLogin(data) {
  return new Promise((resolve, reject) => {
    app.request({
      url: FETCH_URL + 'user/login',
      data: data,
      method: "POST",
      header: {
        'content-type': 'application/json'
      },
      success: resolve,
      fail: reject,
      complete: function(res) {
        // 处理request:fail的客户端bug
        if (res.errMsg && /^request:fail/.test(res.errMsg)) {
          Util.handleRequestError(res);
        }
      }
    })
  })
}



module.exports = {
  GetHomeCakeData, //获取首页蛋糕数据
  AddShoppingCart, //加入购物车
  UserRegister, //用户注册
  UserLogin, //用户登录
}