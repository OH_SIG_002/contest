 

 

2022嵌入式芯片与系统设计竞赛

 

车载全景停车位检测系统

 

Vehicle-mounted panoramic parking space detection system

 

队伍名称：卷土重来队

指导老师：桑海峰

参赛队员：朱光普、张腾飞、黄茂 

 

 

 

 



摘 要

本系统是一种基于车载相机的实时停车位检测系统。用计算机视觉系统来进行停车位和车位线的检测，相比于超声波检测和红外检测，本系统能够检测地面上的一些细节，给驾驶人员提供有效数据，提高泊车的准确性和安全性。

本系统的设计包括硬件设计和软件设计。主要研究车位的特征提取，判断为停车位的条件以及嵌入式的移植，使该系统能够满足现场实时检测并且清楚地显示数据。硬件设计采用大视角、高动态范围的车规级摄像头作为视觉传感器，依托高性能、低功耗嵌入式异构计算平台及配套高效处理算法，实现对车位的实时高精度检测识别。利用选择好的设备，进行检测系统的搭建，选择合适的位置安装四个视觉传感器，实现对车辆周围环境的采集。

本系统具有如下特点：(1)设计并搭建了一个视野范围广阔的360度全景环视系统，用以图像采集，提高车位检测率。（2）采用基于深度学习的关键点检测算法，能够快速并准确检测处车位关键点。

实验结果表明，本文所设计的全景环视系统获得的图像拼接效果良好，可以作为车位检测和定位系统的输入，所搭建的基于深度学习的车位检测系统可以在图像信息非常复杂的情况下完成对车位的高准确率检测，车位检测平均正确率达到了99.10%，所设计的车位定位系统可以实现对车位的定位，并且四个顶点定位精确，可以作为自动泊车系统的车位坐标输入。

 

关键字：停车位检测；360度全景；深度学习；关键点检测 

 

 

 

 



Abstract

This system is a real-time parking space detection system based on car camera. Compared with ultrasonic detection and infrared detection, this system can detect some details on the ground, provide effective data for drivers, and improve the accuracy and safety of parking.

The design of this system includes hardware design and software design. This paper mainly studies the feature extraction of parking space, the condition of judging parking space and embedded transplantation, so that the system can meet the real-time detection and display data clearly. In the hardware design, the vehicle size camera with large viewing angle and high dynamic range is used as the vision sensor. Relying on the high-performance, low-power embedded heterogeneous computing platform and supporting efficient processing algorithm, the real-time and high-precision detection and recognition of parking space is realized. By using the selected equipment, the detection system is built, and four vision sensors are installed in the appropriate location to realize the collection of the vehicle surrounding environment.

The system has the following characteristics: (1) a 360 degree panoramic look around system with wide field of vision is designed and built to collect images and improve the detection rate of parking spaces.(2) The key points detection algorithm based on deep learning can detect the key points of parking space quickly and accurately.

The experimental results show that the image mosaic effect of the panoramic looking system designed in this paper is good, which can be used as the input of parking space detection and positioning system. The parking space detection system based on deep learning can complete the high accuracy detection of parking space in the case of very complex image information, and the average accuracy of parking space detection reaches 99.10%, The designed parking space positioning system can realize the positioning of parking space, and the positioning of four vertices is accurate, which can be used as the parking space coordinate input of automatic parking system.

**Keywords****：**Parking space detection; 360-degree panorama;Deep learning;Key point detection

 

 

 

 

 

 

 

 

 

 

 

目录

[摘 要................................................................................................................... II](#_Toc10762)

[Abstract................................................................................................................ III](#_Toc26561)

[第1章 研究内容及意义...................................................................................... 1](#_Toc29113)

[1.1 研究意义................................................................................................. 1](#_Toc18022)

[1.2 研究内容................................................................................................. 1](#_Toc31952)

[1.2.1图像获取方案的研究................................................................... 1](#_Toc22450)

[1.2.2停车位检测算法的研究............................................................... 2](#_Toc14940)

[第2章 项目难点与创新...................................................................................... 3](#_Toc12123)

[2.1 项目重点与难点..................................................................................... 3](#_Toc21656)

[2.2 项目创新性............................................................................................. 3](#_Toc1702)

[第3章 方案论证与设计...................................................................................... 4](#_Toc16347)

[3.1 硬件方案论证......................................................................................... 4](#_Toc18447)

[3.1.1 四个相机单独采集处理.............................................................. 4](#_Toc28208)

[3.1.2 四个相机合并采集处理.............................................................. 4](#_Toc28930)

[3.2 软件方案论证......................................................................................... 4](#_Toc13462)

[3.2.1 基于边界框的检测方式.............................................................. 4](#_Toc17067)

[3.2.2 基于标记点的检测方法.............................................................. 4](#_Toc5932)

[第4章 原理分析与硬件设计.............................................................................. 6](#_Toc16862)

[4.1 硬件系统方案的设计............................................................................. 6](#_Toc7128)

[4.1.1 图像采集系统.............................................................................. 6](#_Toc18343)

[4.1.2 图像处理系统.............................................................................. 7](#_Toc32565)

[4.1.3 完整的硬件系统.......................................................................... 8](#_Toc12041)

[第5章 软件设计与流程...................................................................................... 9](#_Toc17858)

[5.1 软件设计原理及流程图介绍................................................................. 9](#_Toc20971)

[5.2 图像采集............................................................................................... 10](#_Toc13220)

[5.2.1鱼眼相机模型............................................................................. 10](#_Toc22093)

[5.2.2张正友摄像机标定法................................................................. 11](#_Toc18309)

[5.2.3 图像俯视变换............................................................................ 13](#_Toc5684)

[5.2.4 图像拼接与融合........................................................................ 13](#_Toc31056)

[5.3 停车位检测........................................................................................... 16](#_Toc32756)

[5.3.1 HRNet算法................................................................................ 16](#_Toc17365)

[5.3.2 停车位检测算法........................................................................ 17](#_Toc31162)

[5.3.3 损失函数.................................................................................... 19](#_Toc30883)

[5.3.4 数据集........................................................................................ 20](#_Toc27274)

[第6章 系统测试与误差分析............................................................................ 21](#_Toc6042)

[6.1结果与分析............................................................................................ 21](#_Toc23568)

[第7章 总结........................................................................................................ 22](#_Toc28660)

[参考文献.............................................................................................................. 23](#_Toc997)

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 



# 第1章 研究内容及意义

## 1.1 研究意义

近年来，随着电子技术、互联网、人工智能等学科领域的迅猛发展，汽车产业面临着安全化、智能化、互联网化的深刻变革。未来汽车不仅是人们出行的代步工具，更是集智能驾驶车联网于一身，融合各领域先进技术的智能移动终端[1]。21世纪最具颠覆性的技术之一就是无人驾驶技术，无人驾驶汽车利用智能车辆环境感知技术通过车载传感系统感知道路环境，并根据感知所获得的道路、车辆位置和障碍物信息，自动规划行车路线并控制车辆到达预定目标的智能汽车[2]。

从技术角度看，智能汽车关键技术具体可分为感知、决策和控制三个维度，具体包括：车载环境信息采集与处理，包括车外环境感知（停车位、车道线、行人、障碍物、交通标志等）、车内环境感知（车辆位置、车辆状态、驾驶人状态等）；基于采集信息的驾驶决策，包括换道并道、冲突避让、路径规划、路线导航等；基于生成决策的驾驶控制，包括横向、纵向、垂向以及集成控制。在此背景下，汽车的安全性和智能性引起了人们前所未有的重视。所以智能车辆的外部环境感知技术就显得尤其的重要，它就像智能车辆的“眼睛”，为智能车提供所有需要的信息。其中，自动泊车又是智能汽车中比较重要的一个环节，为了保障泊车过程的安全性，辅助驾驶员掌握泊车过程中车辆周围的盲区情况，各类泊车辅助产品应运而生，而这方面的研究一直是汽车智能安全技术发展的热点之一。

本文所做的就是辅助驾驶系统中泊车辅助系统的一部分，通过汽车在找车位过程中对停车位进行实时检测，对驾驶员及时进行提醒，辅助驾驶。

## 1.2 研究内容

### 1.2.1图像获取方案的研究

针对图像获取方案的研究分为以下三个方面：

（1）鱼眼相机相比于普通非鱼眼相机可以拍摄更大范围的画面，但同时也因此会产生非常大的畸变，为了去除图像中的畸变，本论文首先对鱼眼相机的成像原理等内容进行了研究，利用张正友标定法得到鱼眼相机的参数，之后利用标定得到的参数完成相机的去畸变处理；

（2）由于相机的安装与地面有一个倾斜夹角，从而导致了校正后的画面是一个斜视拍摄的画面，并不是垂直于地面的俯视鸟瞰图，为此，本文对图像的俯视变换算法进行了研究，以获得四个鱼眼相机垂直于地面的俯视图；

（3）为了得到 360 度全景环视鸟瞰图，本论文对图像的拼接融合算法进行了研究。

### 1.2.2停车位检测算法的研究

针对停车位检测算法的研究分为以下三个方面：

（1）对基于深度学习的图像关键点检测算法进行研究，在此基础上搭建了一个车位检测模型并完成训练和车位检测；

（2）为判断车位角度，研究融合关键点信息和角度信息。

（3）为更好提取图像关键点信息，研究多分辨率信息采集，并对不同分辨率的特征进行融合。



# 第2章 项目难点与创新

## 2.1 项目重点与难点

本系统工作重点与难点主要以下几点：

（1）车位为规格大小比较固定的矩形，但是不是俯视看得话，就会成为不太规则的四边形，比较难判断其是否为车位。

（2）外界环境干扰较为复杂，车位大小、车位线的颜色，宽度、光照的强弱，阴影以及遮挡问题都会影响车位的检测结果。

（3）为了驾驶人的安全以及能够及时的获取想要的车位信息，实时检测速度要求比较高。

## 2.2 项目创新性

本系统的创新点有以下几点：

(1)   采用四台车载相机同时采集车辆四周的图像，能够更好地获取外界环境的信息，增大检测范围，将四个方向的图像融合为一张全景图鸟瞰图，降低了数据的重复性，提升了车位形状的一致性，降低了检测难度。

(2)   使用深度神经网络完成检测车位的任务，相比于传统的检测任务，深度学习根据大量样本的学习，泛化能力较强。

(3)   将停车位的拐点作为关键点，通过关键点的位置和方向确定车位的位置以及方向。

(4)   使用多分辨率学习的方式，同时在高分辨率和低分辨率下提取特征，并将不同分辨率的特征进行融合。通过这种多尺度融合的方式，使每一个高分标率的表征都从其他并行表示中反复接收信息，从而得到丰富的高分辨率表征。

(5)   在关键点检测网络的基础上，新增了一个用于检测关键点角度的并行分支，通过融合关键的位置信息来预测每一个关键点的角度信息。



# 第3章 方案论证与设计

## 3.1 硬件方案论证

硬件方案主要是成像系统的搭建，从相机的角度考虑，我们提出以下两种解决方案：

### 3.1.1 四个相机单独采集处理

4个相机成像系统由4个车载相机以及正常镜头组成，四个相机单独采集处理，不合并。

设计过程：将四个车载相机分别安装到车的左右后上四个合适的位置，然后只需对安装在车位上方的相机进行变换处理，这种方法采集到的图像范围较小，前期的处理工作相对比较简单，但采集到的图像信息较少，不利于检测准确性的提升。

### 3.1.2 四个相机合并采集处理

4个相机成像系统由4个车载相机以及鱼眼镜头组成，将采集到的图像拼接成全景图来处理。

设计过程：将四个车载相机分别安装到车的前后左右四个合适的位置，然后需要将鱼眼镜头进行畸变矫正，然后图像拼接成为全景图后在进行检测，前期处理过程较为复杂，畸变矫正也较为费时，但可以获取更多的图像信息，更有利于进行图像检测。

综上所述，第二种方案更为合理，故本系统采用四个相机合并采集处理的方法进行。

## 3.2 软件方案论证

### 3.2.1 基于边界框的检测方式

该方法使用边界框预测车位的标记点和一种自定义的车位头结构，然后利用几何线索匹配成对的标记点，确定停车槽的方向。最后，通过车位的类型、方位和成对标记点推断出两个不可见的顶点，得到完整的车位。该方法受限于预选框的准确性，检测准确率上限受限。

### 3.2.2 基于标记点的检测方法

该方法根据图片特征信息，在经过深度学习网络训练后直接生成车位关键点热图，然后结合关键点角度信息生成完整车位。该方法不受预选框的影响，检测速度快，准确率高。

综上所述，第二种方案更为合理，故本系统采用基于标记点的检测方法。



 

# 第4章 原理分析与硬件设计

## 4.1 硬件系统方案的设计

本系统的硬件设计主要包括图像采集系统，和图像处理系统这两个部分。图4-1为本系统的硬件系统框图。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image002.gif)

图4-1 硬件系统框图



### 4.1.1 图像采集系统

四个摄像机合并采集处理安装在车辆前后以及左右后车镜的下方。将前侧摄像头安装在汽车正前方中心处，保证摄像头的前方没有遮挡；将拍摄车位线的左右两个摄像头安装在车辆的左右后视镜下方，保证摄像头拍摄到的图像下底边与车辆左右侧前后轮中心的连线保持平行；后侧的摄像头安装在后备箱车标中心位置，将摄像头镜头向下与地面呈10~15度安装，使摄像头能够完整的拍到车线且保证该摄像头拍摄到的图像下底边与车辆后侧保持平行。

 

|      |                                                              |      |                                                              |
| ---- | ------------------------------------------------------------ | ---- | ------------------------------------------------------------ |
|      | ![前](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image005.gif) |      | ![右](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image006.gif) |
|      |                                                              |      |                                                              |


(a)前侧摄像机           (b)右侧摄像机   



 

|      |                                                              |      |                                                              |
| ---- | ------------------------------------------------------------ | ---- | ------------------------------------------------------------ |
|      | ![左](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image009.gif) |      | ![后](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image010.gif) |


(c)左位摄像机          (d)后侧摄像机



图4-2 图像采集系统安装示意图



### 4.1.2 图像处理系统

本系统最终运行在Hi3516DV300开发板上，开发板具有的1TOPS算力的深度学习模块足以满足本系统的网络模型。

摄像头使用多媒体串行链路(GMSL)技术，从摄像头的超低功耗要求、到传感器数据汇聚的宽带要求，GMSL SerDes能够满足未来系统的所有需求。先进的链路完整性和诊断功能提供可靠的链路性能监测——这对汽车安全系统的设计至关重要。GMSL串行器和解串器支持长达15m的屏蔽双绞线(STP)或同轴电缆传输，满足汽车行业最苛刻的电磁兼容(EMC)要求，使视频数据能够远距离传输。本系统使用4路摄像头，通过同轴电缆连接到视频采集模块，由底板将数据传递给Hi3516DV300进行处理。

### 4.1.3 完整的硬件系统

嵌入式系统采用直流5.5-12V宽电压供电，四个摄像头与冬电Hi3516DV300系统连接后，安装在车辆的合适位置。嵌入式系统连接关系图如图所示。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image012.jpg)

图4-3停车位应用嵌入式系统连接关系图



 

 



# 第5章 软件设计与流程

## 5.1 软件设计原理及流程图介绍

基于车载相机的停车位及车位线检测系统运行在Jetson TX2上，停车位以及车位线的识别检测通过分析视觉传感器（即四个车载相机）的信息采集来实现。整个软件架构方案采用分层、可并行的模块化结构，同时采用一些开源的框架，兼顾了经济性。最后通过显示屏进行视觉信息展示，给驾驶者提供有用的信息，最终实现车辆的泊车辅助功能。该系统的总体流程图如图5-1所示。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image014.gif)

图5-1系统功能实现流程图

系统总体流程如上图所示，按功能可以实现全景鸟瞰图的构建以及停车位的检测；系统采用开源计算机视觉库OpenCV和开源深度学习框架Pytorch实现。



## 5.2 图像采集

### 5.2.1鱼眼相机模型

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image016.jpg)
 鱼眼相机的透镜并是弧度很大的一个曲面，光线透过鱼眼相机会发生折射，这就使得这种构造的摄像头可以获取更大视角范围的画面，但同时也就导致了较大的畸变，为了得到鱼眼相机的坐标转换关系，实际上只需要求得鱼眼镜头和针孔相机镜头的转换关系即可，鱼眼相机镜头曲面主要分两种，一种是抛物面，一种是球面，工作的机理是一样的，球面透镜的模型结构如图5-2所示，从图中可以看出，p点与球心的连线和球面相交于![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image018.gif)点，通过折射于![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image020.gif)点处成像。

图5-2 球面透镜成像模型

鱼眼相机拍摄获得的画面存在较大的畸变[3,4,5]，而产生的畸变又分为径向畸变、切向畸变以及薄棱镜畸变，由于薄棱镜畸变对成像的影响相较于前两种畸变小很

多，所以不做考虑。

相机的成像过程实质上是坐标系的转换。首先空间中的点由“世界坐标系”转换到“像机坐标系”，然后再将其投影到成像平面(图像物理坐标系)，最后再将成像平面上的数据转换到 图像像素坐标系。但是由于透镜制造精度以及组装工艺的偏差会引入畸变，导致原始图像的失真。镜头的畸变分为径向畸变和切向畸变两类。

径向畸变是沿着透镜半径方向分布的畸变，产生原因是光线在原理透镜中心的地方比靠近中心的地方更加弯曲，这种畸变在普通廉价的镜头中表现更加明显，径向畸变主要包括桶形畸变和枕形畸变两种。如图5-3所示。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image022.jpg)

图5-3畸变图

成像仪光轴中心的畸变为0，沿着镜头半径方向向边缘移动，畸变越来越严重。畸变的数学模型可以用主点（principle point）周围的泰勒级数展开式的前几项进行描述，通常使用前两项，即![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image024.gif)和![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image026.gif)，对于畸变很大的镜头，如鱼眼镜头，可以增加使用第三项![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image028.gif)来进行描述，成像仪上某点根据其在径向方向上的分布位置，调节公式为：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image030.gif)=x（1+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image032.gif)+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image034.gif)）        （5-1）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image036.gif)（1+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image032.gif)+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image034.gif)）       （5-2）

式里（x0，y0）是畸变点在成像仪上的原始位置，（x，y）是畸变矫正后新的位置。

切向畸变是由于透镜本身与相机传感器平面（成像平面）或图像平面不平行而产生的，这种情况多是由于透镜被粘贴到镜头模组上的安装偏差导致。畸变模型可以用两个额外的参数![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image038.gif)和![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image040.gif)来描述：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image030.gif)=x+[2![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image038.gif)y+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image040.gif)(![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image042.gif)+2![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image044.gif))]         （5-3）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image046.gif)=y+[2![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image048.gif)(![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image042.gif)+2![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image050.gif))]         （5-4）

大体上畸变位移相对于左下—右上角的连线是对称的，说明该镜头在垂直于该方向上有一个旋转角度。

### 5.2.2张正友摄像机标定法

相对于传统标定法、自标定法而言，处于其二者之间的另一方法就是张正友标定法，张正友相机标定法[6]是张正友教授1998年提出的单平面棋盘格的相机标定方法，它需要相机从不同的方向对同一个标定模板拍摄，进而开始寻找标定板上的特征点和它处于像平面中的映射点之间的相互应对关系来进行标定。此方法模板制作简单、鲁棒性高，精确度好，应用广泛。本文就是采用张正友的标定方法。传统标定法的标定板是需要三维的，需要非常精确，这很难制作，而张正友教授提出的方法介于传统标定法和自标定法之间，但克服了传统标定法需要的高精度标定物的缺点，而仅需使用一个打印出来的棋盘格就可以。同时也相对于自标定而言，提高了精度，便于操作。因此张氏标定法被广泛应用于计算机视觉方面。

（1）单应矩阵的计算

设标定板上的某个特征点在世界坐标系和图像坐标系下的坐标分别为M=(X.Y.Z![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image052.gif)和m=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image054.gif)，由相机成像模型，可得如下公式:

  s![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image056.gif)=A[R t]![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image058.gif)=A[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image060.gif)            （5-5）

假定将标定模板所处面放置在世界坐标系Z=0的面上。则上式可变成:

s![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image056.gif)=A[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image062.gif)t]![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image064.gif)=A[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image062.gif)t]![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image064.gif)=A[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image066.gif)t]![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image068.gif)        （5-6）

式（5-6）中旋转矩阵R=[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image070.gif)]和平移向量t是摄像机的外部参数，![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image072.gif)表示旋转矩阵

R的第i列向量，A为摄像机内参数矩阵，s为尺度因子。

​      A=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image074.gif)             （5-7）

规定单应矩阵H=A[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image076.gif)设H=[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image078.gif)]，则有H=A[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image080.gif)]=[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image082.gif)]

（2）摄像机内外参数求解

由于求解的H不会与真实值H完全一样，会有一个比值误差，所以把H写成如下形式：

[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image082.gif)]=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image084.gif)A[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image080.gif)]         （5-8）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image086.gif)与![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image088.gif)为单位正交向量，有![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image090.gif)和![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image092.gif)，因此能够获得求取相机内部参数的两个条件约束：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image094.gif)=0            （5-9）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image096.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image098.gif)          （5-10）

让B=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image100.gif),B是对称矩阵，可以用6维向量b定义：b=[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image102.gif)

设H第i列向量为![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image104.gif)=[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image106.gif),则![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image108.gif)B![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image104.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image110.gif)b，其中：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image112.gif)=[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image114.gif),![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image116.gif)+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image118.gif),![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image120.gif),![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image122.gif)+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image124.gif),![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image126.gif)+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image128.gif)     （5-11）

利用约束条件可得：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image130.gif)b=0            （5-12）

如有N幅模板的图像，就能得到：Vb=0, 其中，V为2N![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image132.gif)6向量矩阵，若 N![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image134.gif)3，能够求出b，紧接着可以获得如下几个内参：

   ![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image136.gif)=（![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image138.gif)）/（![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image140.gif)      （5-13）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image084.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image142.gif)—[![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image144.gif)+![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image136.gif)（![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image138.gif)）]/![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image146.gif)      （5-14）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image148.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image150.gif)                （5-15）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image152.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image154.gif)            （5-16）

S=—![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image156.gif)/![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image084.gif)               （5-17）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image158.gif)=s![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image136.gif)/![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image152.gif)—![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image160.gif)               （5-18）

再根据单应矩阵H与内参矩阵A，通过以下公式，计算每张图片的外参数：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image162.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image164.gif)               （5-19）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image088.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image166.gif)               （5-20）

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image168.gif)=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image170.gif)               （5-21）

t=![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image172.gif)                （5-22）

### 5.2.3 图像俯视变换

由于安装好的摄像头光轴不与地面垂直，总是存在一定的夹角，所以所获得的画面也是带有一定的倾斜角度的，为了得到从空中垂直俯视的画面，需要对畸变校正后的图像进行俯视变换操作。俯视变换[7,8]主要有两种实现策略，一种是利用坐标系间的转换关系进行转换的逆投影变换法，该方法需要对相机的安装位置和角度进行精确的测量和计算，这个难度较大，很容易产生较大的误差。另一种方法是直接线性变换法，该方法不存在第一种方法所述的问题，本文采用的便是直接线性变换法。

直接线性变换法的数学表达式如(5-23)所示，其中(X,Y,Z)为世界坐标系下的这是坐标，(μ,ν)为世界坐标转换得到的图像坐标系下的图像坐标，由于地面近似于水平，这里将 Z 值取为 0，则公式(5-23)可以进一步简化为(5-24)。

 

​             ![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image174.gif)           (5-23)      

​             ![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image176.gif)            (5-24)  

 

公式中的 L1、L2、...等是未知参数，通过四对以上的世界坐标系和图像坐标系下的对应坐标即可求解出所有的未知参数，之后便可推得将原图像转换为俯视图的转换矩阵H，利用该矩阵将原图坐标转换为俯视图中的坐标，转换关系如公式(5-25)所示。

​        ![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image178.gif)      (5-25)

### 5.2.4 图像拼接与融合

360 环视系统图像的拼接[9,10]具有一定的特殊性，即四个摄像头所拍摄的画面基本上是处于同一个水平面，本文所设计的图像拼接方法正是在此基础上进行的。拼接示意图如图5-4所示，由于四个摄像头拍摄的画面经过俯视变换之后，相邻的画面之间会存在一部分重叠的画面区域，比如图中的前置摄像头和左后视镜处摄像头对应的画面于①区域重叠，本文采用图像融合算法对这部分重叠的区域进行融合处理，之后对其他重叠区域依次完成上述操作即可得到全景鸟瞰图，接下来将对拼接的具体流程做进一步地介绍。

（1）为了对图像进行拼接，首先要确定一个统一的拼接坐标系，本文所选取的坐标系为一个二维平面坐标系，坐标系原点 O 点取车辆的正中心于地面的垂直投影，由于图像坐标系 x 轴和 y 轴与常规的坐标系有所差异，所以 y 轴取前后相机连线于地面的垂直投影，x 轴取左右后视镜连线于地面的垂直投影，具体的方向如图5.4所示；

（2）将上一节得到的四幅俯视图的角度做微调之后，依次放入该坐标系下；

（3）对四幅图像中两两相邻的图像的重叠区域进行融合处理，最终得到全景鸟瞰图。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image180.jpg)

图5-4 全景拼接方案示意图

本文使用图像融合算法对相邻画面的重叠区域进行处理，图像融合算法主要包括三大类，第一类融合算法是像素级图像融合，通过对原始图像画面的像素点进行直接处理计算进行的融合，该方法可以有效地对图像细节进行保留，该种类型的方法得到了较为广泛的使用。第二类融合算法是特征级融合，该方法首先需要对图像进行预处理，提取图像的特征等信息，之后再根据所提取的特征进行融合。第三类融合算法是决策级图像融合，该算法的实现过程相对复杂，需要完成对图像完成预处理和特征提取之后，再根据一定法则和置信度最终完成图像融合。考虑到360环视系统的实时性要求以及为了对融合区域的细节进行最大化保留，本论文采用的是第一类融合方法中被广泛使用的加权平均融合算法。像素级融合法中还包括了平均值融合法，该方法将两张图像的相对应像素点的灰度值直接求平均，但这样会使得画面过度不平滑，同时在重叠区域的边界处会产生明显的痕迹，而加权平均融合算法则可以较好的解决这个问题，加权平均融合算法是依据重叠区域像素点的位置对像素点进行加权平均处理，原理如图5-5 所示。

 

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image182.jpg)
 图5-5 加权平均融合示意图

图中，p1和p2有一个共同的区域，即重叠区域，在非重叠区域每个像素点的灰度值保持不变，在重叠区域使用加权法产生新的灰度值，融合区域新的灰度值具体计算公式如下：

​       ![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image184.gif)        (5-26)

式中(x,y)为重叠区域的像素点坐标，α和β为加权系数，系数α与系数β和为 1，具体表达式如下：

​        ![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image186.gif)             (5-27)

对四个摄像头的俯视图进行加权平均融合处理之后，最终得到 360 全景俯视鸟瞰图，拼接融合的最终效果如下图5-6所示：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image188.jpg)

图5-6 图像融合后的全景图

## 5.3 停车位检测

### 5.3.1 HRNet算法

HRNet[11](High-Resolution Representations for Labeling Pixels and Regions)是中国科学技术大学与微软亚洲研究院发布的人体姿态估计模型，现有的大多数人体姿态估计的都是从高分辨率到低分辨率网络产生的低分辨率表征中恢复高分辨率表征，而HRNet在整个过程中都保持高分辨率的表征。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image190.gif)

图5-7 网络结构

网络的结构如图5-7所示，由四个阶段组成，第2，3，4个阶段由模块化的多分辨率块组成，每个多分辨率块包含一个多分辨率群卷积和多分辨率卷积，如图5-8(a)和5-8(b)。多分辨率群卷积由一组卷积扩展而成，它将输入信道分成多个信道子集，分别对不同空间分辨率下的每个子集进行规则的卷积，为增加网络的效率，每组卷积中的卷积操作都是由残差块完成的。

多分辨率卷积如图5-8(b)所示，它类似于规则卷积的多分支全连接方式，如图5-8(c)所示。输入通道被划分为几个子集，输出通道也被划分为几个子集。输入和输出子集以全连通的方式连接，每个连接都是规则的卷积。输出通道的每个子集是每个输入通道子集上的卷积输出的总和，在这一结构中，使用步长为2，3×3的卷积实现高分辨率到低分辨率的转换，使用双线性上采样实现分辨率的提高。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image192.gif)

图5-8 多分辨率块

残差块的两种结构如图5-9所示。

 

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image194.gif)

图5-9 两种残差块

经过前四个阶段后，网络的输出结果是四个不同分辨率的特征图，为得到最终结果，需要对这四个特征图进一步处理。图5-10为三种结构，(a)直接取最高分辨率的分支为最终输出结果，(b)和(c)做了一个简单而有效的修改，利用其他子集的通道输出从低分辨率卷积。其优点是充分挖掘了多分辨率卷积的能力。这个修改只增加了一个小参数和计算开销。

(b)和(c)通过双线性上采样将低分辨率表示重新调整为高分辨率，并将表示的子集连接起来，从而得到高分辨率表示，采用该表示来估计分割图/面部地标热图。在对象检测的应用中，通过将具有平均池化的高分辨率表示向下采样到多个层次来构建多层表示，如图5.10(c)所示。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image196.gif)

图5-10 HRNet输出方式

### 5.3.2 停车位检测算法

HRNet作为预测人体骨骼关键点的网络[12,13,14]，输出结果是关键点的热图，当输入的某一个位置存在关键点，输出的热图中将会存在一个以对应的位置为中心的区域，如图5-11所示。中心的值最大并且接近1，距离中心越远值越小，直到趋近于0。每个点值的大小：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image198.gif)         （5-28）

HRNet输出的热图的通道数为17，对应着人体的17个骨骼关键点，若应用与车位检测中，只需要保留2个通道，对应车位标记点的两种类型。与检测人体骨骼关键点相比，在检测车位的实际情况中，输入图像来自车载相机拼接而成的全景图，车位的大小是稳定的，不会发生剧烈的变化，所以可以适当的缩减网络的分支和深度，提升运行效率。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image200.gif)

图5-11 高斯热图

检测停车位，只检测车位的标记点是不够的，还需要得到车位的角度，因此需要对原有的网络进行修改，增加角度预测的分支。最终需要的角度是两个浮点数，分别为标记点方向的余弦值和正弦值。角度预测是一个回归任务，因此标记点的定位所使用的热图并不适用于角度预测，需要重新定义一个新的分支，专门用于预测车位标记点的角度。

通常情况下，车位的尺寸不会发生剧烈的变化，每个车位的标记点的距离相对固定，可以总结出，每一张全景图像中检测到的标记点是较为分散的，不会聚集在一处。所以在回归每一个标记点的角度时，可是使用更加低的分辨率来增加检测效率，同时能够保证网络的有效性。如图5-12为本文才用的方案。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image202.gif)

图5-12 车位检测网络

输入图片为512×512，经过两次卷积后将尺寸缩小至1/4，然后经过一个多分辨率块提取图像的特征，标记点位置和角度的预测都使用此特征。网络的第一个分支用于预测标记点的位置，它的主体由两个多分辨率块组成，在得到两个不同分辨率的特征图后，低分辨率的特征图使用双线性上采样提高分辨率与高分辨率的特征图相加，所得的特征图再经过两次卷积降低通道数，最终得到2×128×128的热图，使用非极大值抑制和阈值筛选，即可得到标记点的坐标。第二个分支与第一个分支大体相似，在第一个多分辨率块的多分辨率卷积中，不仅融合了此分支中的不同分辨率的特征，还融合了第一个分支的特征，使角度预测分支能够结合标记点的位置信息。通过两个多分辨率块和双线性上采样融合后，输出结果为32×128×128的特征图，再经过三次卷积，同时降低通道数和分辨率，使最终输出为2×16×16。两个通道分别对应标记点角度的余弦和正弦，分辨率为16×16，每一个点都对应于原图像上的一块区域，若原图像某一区域存在一个标记点，则该标记点的角度记录在特征图的某一点上。例如原图像(0:32,0:32)的区域中有一个标记点，该区域所对应的位置是(0,0),所以输出特征图的(0,0,0)和(1,0,0)分别表示该标记点的余弦和正弦值。

程序运行结果如图5-13所示，其中（a）为来自PS2.0数据集的输入图像。（b）为根据数据集的标签生成的高斯热图以及标记点所对应的角度的余弦值和正弦值，红色的热图表示T型车位标记点，绿色的热图表示L型标记点。（c）为网络的检测结果。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image204.gif)

图5-13 实验结果图

### 5.3.3 损失函数

此网络对位置和角度分别使用不同的分支进行预测[15]，两个分支的输出各自拥有一个损失函数。定位标记点的分支使用均方差损失函数，![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image206.gif)是某一处的预测值，![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image208.gif)是真实值。实际运算中，需要将2×128×128的热图展开成为1×32768的一维列表。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image210.gif)         (5-29)

在得到标记点的位置后，根据位置信息在另一个分支中找到该位置对应的角度值，在这个过程中，除了存在标记点的位置，其他位置所预测的结果并不需要关心，因此在反向传播过程中，只需要计算标记点所对应的位置。Pytorch中提供了这种方法，反向传播时，使用gradient参数，gradient是一个与网络输出的结构相同的张量，把需要的位置设为1，其余位置设为0，就可以在反向传播过程中只计算值为1的部分。预测角度分支的损失函数定义为预测结果与标签的差值的平方，![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image212.gif)为预测结果，![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image214.gif)为标签。

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image216.gif)           (5-30)

### 5.3.4 数据集

以上两种方法都是基于ps2.0数据集，该数据集中的图像是由4台低成本鱼眼相机合成的环绕视图图像。考虑了各种停车槽类型，包括垂直停车槽、平行停车槽和倾斜停车槽。采集室外样本时，考虑了不同的光照条件和天气条件。该数据集中包含的典型图像样本如图5-14所示。

该数据集包含9827个训练图像和2338个测试图像。为了测试停车槽检测算法在不同特殊条件下的性能，还将测试图像分为6类，分别为室内车位、白天室外车位、室外雨天车位、室外阴影车位、路灯下的室外车位、室外倾斜车位。

在这个数据集中，车位标记点分为T型和L型两类，T型标记点的角度定义为T的下部分所指向的方向，顺时针旋转一定角度能与另一条边重合的边的方向定义为L行标记点的方向，如图5-15所示。 

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image218.gif)

图5-14 数据样本

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image220.gif)

图5.15 标记点方向



# 第6章 系统测试与误差分析

## 6.1结果与分析

本文设计的车位检测网络在PS2.0数据上的测试结果达到了97.29%的准确率和99.1%的召回率，车位标记点的定位loss为![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image222.gif),标记点角度的loss为0.04。

除了在数据集上的测试外，我们使用自行设计的车载全景系统采集了一些行车中所遇到停车位，这些图像与数据集中的图像存在较大差异，在未经训练的情况下，检测效果仍然较好，说明网络的泛化能力较强。检测效果如图6-1所示：

![img](file:///C:/Users/shf/AppData/Local/Temp/msohtmlclip1/01/clip_image224.gif)

图6-1 车位检测结果

# 第7章 总结

由于基于超声波的自动泊车车位检测系统具有很多的缺点，比如依赖于参照

物、无法识别斜车位等，而基于图像的车位检测系统为解决上述问题提供了可能。

由于基于图像的自动泊车系统起步较晚，相关论文较少，绝大多数所设计的车位检测算法只有在光照条件良好、阴影少、图像信息简单等条件下才有效，另外，图像中所包含的信息越复杂，对算法的干扰就越大，多数论文仅仅做到从条件良好的图像中检测出大量的车位线，并没有进一步对如何根据处理出的车位线来判

断具体车位并给出车位的具体坐标值。为了解决上述问题，本文设计了一个基于深度学习和 OpenCV 的车位检测和定位系统，具体研究内容及结果如下：

(1)360 度全景环视系统的设计与实现

本文通过四个鱼眼摄像头设计并实现了一个360度全景环视系统，用来采集车辆周围的图像信息，首先对鱼眼相机进行了标定，使用张正友标定法得到相机参数，之后利用得到的参数对原始畸变画面完成校正，得到四幅无畸变的图像，之后通过直接线性变换得到了垂直于地面的俯视图，最后在所设计的拼接坐标系

下通过加权平均融合算法完成了图像拼接工作，最终得到车辆周围的 360 度全景俯视鸟瞰图，从结果来看，所获得的全景图像效果较好，可以用作车位检测和定位系统的输入；

(2)基于深度学习的车位检测系统系统设计与实现

首先，本文利用公开的全景停车位数据集PS2.0训练神经网络，在测试集上达到了97.29%的准确率和99.1%的召回率，然后在自行采集的全景图像中进行测试，在光照强度不同、车位颜色不同、路面材质不同，阴影遮挡的情况下均能取得良好的检测结果。

 

 

 

 

 

 

 

 

 

 

 

# 参考文献

[1] 李舜酩,沈峘,毛建国,等.智能车辆发展及其关键技术研究现状[J].传感器与微系统, 2009, 28(1):1-3.

[2] 《中国公路学报》编辑部中国汽车工程学术研究综述·2017中国公路学报，2017（6）:1-197．

[3] 曾建兰．摄像机标定若干问题的研究[D]．华东师范大学硕士论文，2008

[4] 谭晓波．摄像机标定及相关技术研究[D]．国防科技大学硕士论文，2004．

[5] 张广军．视觉测量[M]．北京．科学出版社，2008.

[6] Zhang Zhengyou. A Flexible New Technique for Camera Calibration[J]. IEEE Transactions on Pattern Analysis and Machine Intelligence, 2000, 22(11): 1330-1334.

[7]冯聪. 360°车载环视系统图像拼接技术研究[D].哈尔滨工业大学,2018.

[8]张聪. 基于鱼眼镜头的车载全景环视系统[D].浙江大学,2015.

[9]栾婧. 基于鱼眼摄像头的汽车环视技术研究[D].吉林大学,2018.

[10]刘新明.基于全景视觉的汽车辅助驾驶系统研究与实现[D].北京交通大学,2013.

[11] Sun K, Zhao Y, Jiang B, et al. High-resolution representations for labeling pixels and regions[J]. arXiv preprint arXiv:1904.04514, 2019.

[12] He K, Zhang X, Ren S, et al. Deep residual learning for image recognition[C]//Proceedings of the IEEE conference on computer vision and pattern recognition. 2016: 770-778.

[13] Borgefors G. Hierarchical chamfer matching: A parametric edge matching algorithm[J]. IEEE Transactions on pattern analysis and machine intelligence, 1988, 10(6): 849-865.

[14] Lee S, Seo S W. Available parking slot recognition based on slot context analysis[J]. IET Intelligent Transport Systems, 2016, 10(9): 594-604.

[15] Vestri C, Bougnoux S, Bendahan R, et al. Evaluation of a vision-based parking assistance system[C]//Proc. 8th Int. IEEE Conf. Intell. Transp. Syst. 2005: 131-135.

 

 