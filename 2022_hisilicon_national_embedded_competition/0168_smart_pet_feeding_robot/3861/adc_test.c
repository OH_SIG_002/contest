#include "adc_test.h"


#define HX_711_DOUT 6
#define HX_711_SCK 7
#define ADC_TEST_STACK_SIZE 512
#define ADC_TEST_PRIO 25
#define LED 9
#define GapValue 1300
#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0
static unsigned long count;


static void *hjx711_task(void)
{
    
    IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_12,0);

    double base_data = 0 ,hx711_weight = 0;
    //unsigned int buf[10] = {0};
    base_data = Get_Sensor_Read();//获取基准值
    printf("pc");
    while(1)
    {
        hx711_weight = (Sensor_Read() - base_data)/GapValue;//获取重量

        //if(hx711_weight > 0)//大于0时显示
        if(1)
        {
            printf("weight : %.2f\r\n",hx711_weight);
            
        }
        usleep(3000);
    }
    
}

unsigned long Sensor_Read(void)
{
    unsigned long value = 0;
    unsigned char i = 0;
    IotGpioValue input = 0;
    usleep(2);
    IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_12,0);
    usleep(2);
    IoTGpioGetInputVal(IOT_IO_NAME_GPIO_11,&input);
    //等待ad转换结束
    while(input)
    {
        IoTGpioGetInputVal(IOT_IO_NAME_GPIO_11,&input);
    }
    for(i=0;i<24;i++)
    {
        IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_12,1);
        usleep(2);
        value = value << 1;
        IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_12,0);
        usleep(2);
        IoTGpioGetInputVal(IOT_IO_NAME_GPIO_11,&input);
        if(input)
        {
            value ++;
        }
    }
    //第25个脉冲
    IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_12,1);
    usleep(2);
    value = value ^ 0x800000;
    //第25个脉冲结束
    IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_12,0);
    usleep(2);
    return value;
}   

double Get_Sensor_Read(void)
{
    double sum = 0;
    for(int i = 0;i < 10;i++)
    {
        sum += Sensor_Read();
    }
    return(sum/10.0);
}

static void ADCExampleEntry(void)
{
    osThreadAttr_t attr;

    IoSetFunc(IOT_IO_NAME_GPIO_11,IOT_IO_FUNC_GPIO_11_GPIO);
    IoTGpioSetDir(IOT_IO_NAME_GPIO_11,IOT_GPIO_DIR_IN);
    IoSetFunc(IOT_IO_NAME_GPIO_12,IOT_IO_FUNC_GPIO_12_GPIO);
    IoTGpioSetDir(IOT_IO_NAME_GPIO_12,IOT_GPIO_DIR_OUT);;             

    attr.name = "adc711_read";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = ADC_TEST_STACK_SIZE;
    attr.priority = ADC_TEST_PRIO;
            

    if (osThreadNew((osThreadFunc_t)hjx711_task(), NULL, &attr) == NULL) {
        printf("[AdcExample] Falied to create adcTask!\n");
    }
}

SYS_RUN(ADCExampleEntry);