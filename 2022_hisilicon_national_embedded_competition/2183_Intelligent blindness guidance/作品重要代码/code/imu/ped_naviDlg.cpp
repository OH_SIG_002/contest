// ped_naviDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ped_navi.h"
#include "ped_naviDlg.h"
#include "mscomm.h "
#include "unpack.h"
#include "stdio.h"
#include "align.h"
#include "math.h"
#include "navigation.h"
#include "acce_cali.h"
#include "windows.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////测试变量定义//////////////////////
//extern FILE *fn_test;//fn4


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

///////全局变量定义////////


int func_flag=0;//功能标志位 1表示对准时刻  2 表示导航时刻

int roll_front_idx=0;/*环形缓冲区前指针*/
int roll_back_idx=0;/*环形缓冲区后指针*/
unsigned long  roll_front=0;/*环形缓冲区前指针（为了解决零界归零问题）*/
unsigned long  roll_back=0;/*环形缓冲区后指针（为了解决零界归零问题）*/
unsigned char  roll_buffer[2048];/*环形缓冲区*/
FILE *imu_file;//惯性器件解包数据输出文件
FILE *in_file_test=NULL;/*原始数据输出文件*/
FILE *navi_file;//导航结果输出文件
FILE *direct_file_in;//读文件导航时使用的文件

//////////////变量定义////////////////////
//HANDLE viewthread;// 显示线程句柄  the1
//DWORD WINAPI viewfunc(LPVOID lpParameter);//显示线程函数the2



///////////////////////////////////////////
/////////////对准的变量在这里extern

extern double gyro_drift_x;
extern double gyro_drift_y;
extern double gyro_drift_z;//对准时刻陀螺的零偏的累加
extern double gyrodrift_x;
extern double gyrodrift_y;
extern double gyrodrift_z;//导航开始之前陀螺的累加之后零偏的求解
extern double acce_modi_x;
extern double acce_modi_y;
extern double acce_modi_z;//对准时刻加表的零偏
extern double acce_modi;
extern int drift_count;//计算初始对准时候的值的个数
extern double real_g;
extern double modi_g;
extern double accedrift_x;
extern double accedrift_y;
extern double accedrift_z;//导航开始之前陀螺的加表累加之后的求解
extern double acce_drift_x;
extern double acce_drift_y;
extern double acce_drift_z;//导航开始之前陀螺的加表的累加

/////////////////////////////////////////
//////////////绘图数据全局变量///////////
extern double posi[3];//位置数据，方便绘图
extern double velo[3];
extern double atti[3];
//////////////////////////////

extern double t_samp;//将采样周期引入

/////////////////////来自acce_cali.cpp的全局变量/////////////////////////////
extern double acce_basis[3];//将加表的零偏定义为全局变量方便导航解算
//////////////////////////////////////////////////////////////////////////


//////////////////////来自unpack。cpp文件中的全局变量，用于加速度计在线标定////////////
 extern double acce_db_g2[500];
             extern double acce_db_x2[500];
		     extern double acce_db_y2[500];
		     extern double acce_db_z2[500];//设置为500个，10ms的情况下为5s，最多5s
		     extern int abdcount2;           
 extern double acce_db_g3[500];
            extern double acce_db_x3[500];
		    extern double acce_db_y3[500];
		    extern double acce_db_z3[500];//设置为500个，10ms的情况下为5s，最多5s
		    extern int abdcount3;
 extern double acce_db_g1[500];
           extern   double acce_db_x1[500];
		   extern  double acce_db_y1[500];
		   extern  double acce_db_z1[500];//设置为500个，10ms的情况下为5s，最多5s
		   extern  int abdcount1;
////////////////////////////////////////////////////////////////////













class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPed_naviDlg dialog

CPed_naviDlg::CPed_naviDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPed_naviDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPed_naviDlg)
	m_testvalue1 = 0.0;
	m_atti1 = 0.0;
	m_atti2 = 0.0;
	m_atti3 = 0.0;
	m_posi1 = 0.0;
	m_posi2 = 0.0;
	m_posi3 = 0.0;
	m_velo1 = 0.0;
	m_velo2 = 0.0;
	m_velo3 = 0.0;
	m_accebasis1 = 0.0;
	m_accebasis2 = 0.0;
	m_accebasis3 = 0.0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPed_naviDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPed_naviDlg)
	DDX_Control(pDX, IDC_MSCOMM1, m_comm);
	DDX_Control(pDX, IDC_NTGRAPHCTRL1, m_ntgraph1);
	DDX_Text(pDX, IDC_EDIT1, m_testvalue1);
	DDX_Text(pDX, IDC_atti1, m_atti1);
	DDX_Text(pDX, IDC_atti2, m_atti2);
	DDX_Text(pDX, IDC_atti3, m_atti3);
	DDX_Text(pDX, IDC_posi1, m_posi1);
	DDX_Text(pDX, IDC_posi2, m_posi2);
	DDX_Text(pDX, IDC_posi3, m_posi3);
	DDX_Text(pDX, IDC_velo1, m_velo1);
	DDX_Text(pDX, IDC_velo2, m_velo2);
	DDX_Text(pDX, IDC_velo3, m_velo3);
	DDX_Text(pDX, IDC_accebasis1, m_accebasis1);
	DDX_Text(pDX, IDC_accebasis2, m_accebasis2);
	DDX_Text(pDX, IDC_accebasis3, m_accebasis3);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPed_naviDlg, CDialog)
	//{{AFX_MSG_MAP(CPed_naviDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_ACCEBEGIN, OnAccebegin)
	ON_BN_CLICKED(IDC_ACCEEND, OnAcceend)
	ON_BN_CLICKED(IDC_ACCEBEGIN2, OnAccebegin2)
	ON_BN_CLICKED(IDC_ACCEEND2, OnAcceend2)
	ON_BN_CLICKED(IDC_ACCEBEGIN3, OnAccebegin3)
	ON_BN_CLICKED(IDC_ACCEEND3, OnAcceend3)
	ON_COMMAND(guanyu, Onguanyu)
	ON_COMMAND(open, Onopen)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPed_naviDlg message handlers

BOOL CPed_naviDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
 
 
 
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPed_naviDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPed_naviDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPed_naviDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

BEGIN_EVENTSINK_MAP(CPed_naviDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CPed_naviDlg)
	ON_EVENT(CPed_naviDlg, IDC_MSCOMM1, 1 /* OnComm */, OnOnCommMscomm1, VTS_NONE)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()




//////////////////////////////////////////////////////////////
///打开串口函数----导航
///功能：设置串口属性并打开串口
/////////////////////////////////////////////////////////////
void CPed_naviDlg::OnButton1() 
{
	
// TODO: Add your control notification handler code here
	
	func_flag=2;//功能标志位设为2，表示导航状态
	////////以下是对串口的初始化/////////	
	
	m_comm.SetCommPort(4);//指定串口号
	m_comm.SetSettings("115200,N,8,1");//通信参数设置
    m_comm.SetInBufferSize(2048);//指定缓冲区大小
	m_comm.SetInBufferCount(0);//清空接收缓冲区
	m_comm.SetInputMode(1);//设置数据获取方式，1为二进制方式，0为文本方式
	m_comm.SetInputLen(0);//设置每次获取长度，0为默认值，表示全部
	m_comm.SetRThreshold(1);//产生oncomm事件的阀值
    ////////////////以下为二进制文件的创建/////////////// 
    in_file_test=fopen("com_test_data.dat","wb"); 
    imu_file=fopen("imu_data.dat","w");//文件存储位置
	navi_file=fopen("navi_data.dat","w");//文件存储位置
//	fn_test=fopen("fn_test.dat","w");//fn3
    ///////////////////////////////////////////////////////
	m_comm.SetPortOpen(1);//打开串口  
   //  viewthread=CreateThread(NULL,0,viewfunc,NULL,0,NULL);//开辟另一个线程进行位置显示   */
    SetTimer(1,100,0);//设置定时器，为了画图
//控件初始化

	m_ntgraph1.ClearGraph();
	m_ntgraph1.SetElement(0);
	m_ntgraph1.SetElementLineColor(RGB(0,255,0));//连线的颜色
	m_ntgraph1.SetElementIdentify(TRUE);
	m_ntgraph1.SetElementPointColor(RGB(255,0,0));//点的颜色
	m_ntgraph1.SetElementPointSymbol(4);//点的形状
	m_ntgraph1.SetElementWidth(1);//线条和点的宽度
	m_ntgraph1.SetElementSolidPoint(TRUE);//点是实心的还是空心的
	// m_ntgraph1.SetRange(-100,100,-100,100);//可以设置为实时变化


}



//////////////////////////////////////////////////////////////
//串口事件响应函数
///功能：当缓冲区中有数据触发事件---将数据存入环形缓冲区
/////////////////////////////////////////////////////////////
void CPed_naviDlg::OnOnCommMscomm1() 
{
	// TODO: Add your control notification handler code here
   	//////////////////////////////////////
	
	VARIANT	input1;
	BYTE rxdata[1000];
	long len1,k;
	COleSafeArray safearray1;
	//////////////////////////////////////
	
	switch(m_comm.GetCommEvent())
	{
		case 2:
		//   收到RThreshold个字符
		input1=m_comm.GetInput();
		safearray1=input1;
		len1=safearray1.GetOneDimSize();
		for(k=0;k<len1;k++)
		{
			safearray1.GetElement(&k,rxdata+k);
	        fputc(rxdata[k],in_file_test);
		    roll_front_idx=roll_front%2048;
		    roll_buffer[roll_front_idx]=rxdata[k];
		  	roll_front++;
			fflush(in_file_test);
		}	

	     ////////////////以下为解包程序///////////////    
        unpack_data();//解包程序（将接收数据解包数据放入同一个线程之中了）
      //此处可以加上解算程序
   


	}//switch的大括号
  
}


//////////////////////////////////////////////////////////////
///关闭串口函数-----导航关闭
///功能：关闭串口
/////////////////////////////////////////////////////////////

void CPed_naviDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	m_comm.SetPortOpen(0);//关闭串口
    func_flag=0;//解包标志位设为0，结束解包
	/*CloseHandle(datathread);//关闭线程*/
	fcloseall();//关闭所有打开的文件
   ///////////////以下为初始化缓冲区的操作，使得每一关闭缓冲区后再打开缓冲区纯净///////////////////////
	roll_front=0;
	roll_back=0;
	roll_front_idx=0;
	roll_back_idx=0;//将环形缓冲区的指针都设为零   
	KillTimer(1);
}



//////////////////////////////////////////////////////////////
///开始对准的函数---打开串口函数---和button1类似
///功能：设置串口属性并打开串口
/////////////////////////////////////////////////////////////

void CPed_naviDlg::OnButton3() 
{
	// TODO: Add your control notification handler code here
	
	func_flag=1;//功能标志位设为1，表示对准状态
    ////////////////////初始化对准参数/////////////////
         
     gyro_drift_x=0;
     gyro_drift_y=0;
     gyro_drift_z=0;//对准时刻陀螺的零偏的累加
     gyrodrift_x=0;
     gyrodrift_y=0;
     gyrodrift_z=0;//导航开始之前陀螺的零偏的求解
     acce_modi_x=0;
     acce_modi_y=0;
     acce_modi_z=0;//对准时刻加表的零偏
     drift_count=0;//计算初始对准时候的值的个数
     modi_g=0;
	 accedrift_x=0;
	 accedrift_y=0;
	 accedrift_z=0;//导航开始之前陀螺的加表累加之后的求平均
	 acce_drift_x=0;
	 acce_drift_y=0;
	 acce_drift_z=0;//导航开始之前陀螺的加表的累加



	///////////////////////////////////////////////
	////////以下是对串口的初始化/////////	
	
	m_comm.SetCommPort(4);//指定串口号
	m_comm.SetSettings("115200,N,8,1");//通信参数设置
    m_comm.SetInBufferSize(2048);//指定缓冲区大小
	m_comm.SetInBufferCount(0);//清空接收缓冲区
	m_comm.SetInputMode(1);//设置数据获取方式，1为二进制方式，0为文本方式
	m_comm.SetInputLen(0);//设置每次获取长度，0为默认值，表示全部
	m_comm.SetRThreshold(1);//产生oncomm事件的阀值
    ////////////////以下为二进制文件的创建/////////////// 
    in_file_test=fopen("com_test_data_dz.dat","wb"); 
    imu_file=fopen("imu_data._dz.dat","w");//文件存储位置
    ///////////////////////////////////////////////////////
	m_comm.SetPortOpen(1);//打开串口  

}


//////////////////////////////////////////////////////////////
///关闭串口函数------对准后的关闭---和button2类似
///功能：关闭串口
/////////////////////////////////////////////////////////////

void CPed_naviDlg::OnButton4() 
{
	
// TODO: Add your control notification handler code here
	m_comm.SetPortOpen(0);//关闭串口
    func_flag=0;//解包标志位设为0，结束解包
	/*CloseHandle(datathread);//关闭线程*/
	fcloseall();//关闭所有打开的文件
   ///////////////以下为初始化缓冲区的操作，使得每一关闭缓冲区后再打开缓冲区纯净///////////////////////
	roll_front=0;
	roll_back=0;
	roll_front_idx=0;
	roll_back_idx=0;//将环形缓冲区的指针都设为零   
   
	
	////////////////////导航开始前求解一些预处理参数 如偏置，真实的g//////////////////

	    gyrodrift_x=gyro_drift_x/drift_count;
		gyrodrift_y=gyro_drift_y/drift_count;
		gyrodrift_z=gyro_drift_z/drift_count;//求解偏置方便后面减去
		modi_g=drift_count/acce_modi*real_g;//导航开始时刻对陀螺的零偏及加速度计中真正的G进行求解
	    accedrift_x=acce_drift_x/drift_count;
		accedrift_y=acce_drift_y/drift_count;
	    accedrift_z=acce_drift_z/drift_count;//求解偏置，方便后面进行姿态对准

		AlignAtti(accedrift_x,accedrift_y,accedrift_z);//求解初始的姿态角
        AlignPosi();//初始值设置--位置
		AlignVelo();//初始值设置--速度

	
         

}


//DWORD WINAPI viewfunc(LPVOID lpParameter)//显示线程函数
//{
// m_ntgraph1.plotxy()
//}

void CPed_naviDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	/////////////////////////
	 if(nIDEvent==1)//第一个子对话框这里对应到SetTimer的第一个参数
    {
          	static int x_max=2,x_min=-2,y_max=2,y_min=-2;
                  
	if(posi[0]>x_max)
	{
		x_max=int(posi[0]+5);
	}
	if(posi[0]<x_min)
	{
		x_min=int(posi[0]-5);
	}
	if(posi[1]>y_max)
	{
		y_max=int(posi[1]+5);
	}
	if(posi[1]<y_min)
	{
		y_min=int(posi[1]-5);
 	}//实时改变坐标

	m_ntgraph1.PlotXY(posi[0],posi[1],0);

    m_ntgraph1.SetRange(x_min,x_max,y_min,y_max);//可以设置为实时变化
     /* m_ntgraph1.SetRange(-20,20,-20,20);//可以设置为实时变化*/

    m_testvalue1=modi_g;
	m_posi1=posi[0];
	m_posi2=posi[1];
	m_posi3=posi[2];
	m_velo1=velo[0];
	m_velo2=velo[1];
	m_velo3=velo[2];
	m_atti1=atti[0];
	m_atti2=atti[1];
	m_atti3=atti[2];

	UpdateData(false);
    }
   
    if(nIDEvent==2)
	{ 
	   static double file_msg[14];//定义读取文件存放地址
	   static int align_number=0;//定义计数变量，方便于判断导航还是对准阶段

   while(!feof(direct_file_in))/////采用if语句，每次启动定时器，解算的速度太慢，因而能定时器启动后一次性解算完成然后kill定时器
   {
       ////////////////////////////////////////////////////////////
		//////读入文件数据///////////////////////
		fscanf(direct_file_in,"%lf %lf %lf %lf  %lf  %lf  %lf  %lf %lf  %lf  %lf  %lf %lf  %lf ",
		&file_msg[0],&file_msg[1],&file_msg[2],&file_msg[3],&file_msg[4],&file_msg[5],&file_msg[6],
		&file_msg[7],&file_msg[8],&file_msg[9],&file_msg[10],&file_msg[11],&file_msg[12],&file_msg[13]);

		if (align_number<(10/t_samp))
		{
           
		   gyro_drift_x=file_msg[5]+gyro_drift_x;
		   gyro_drift_y=file_msg[6]+gyro_drift_y;
		   gyro_drift_z=file_msg[7]+gyro_drift_z;//imu_msg的前三个为加表，后三个为陀螺，然后以此为磁感。。。。
           drift_count=drift_count+1;
           acce_modi_x=file_msg[2];
		   acce_modi_y=file_msg[3];
		   acce_modi_z=file_msg[4];
   		   acce_modi=acce_modi+sqrt(acce_modi_x*acce_modi_x+acce_modi_y*acce_modi_y+acce_modi_z*acce_modi_z);
           acce_drift_x=file_msg[2]+acce_drift_x;
		   acce_drift_y=file_msg[3]+acce_drift_y;
		   acce_drift_z=file_msg[4]+acce_drift_z;
		   align_number++;

		}
		else if (align_number==(10/t_samp) ) 
		{
		gyrodrift_x=gyro_drift_x/drift_count;
		gyrodrift_y=gyro_drift_y/drift_count;
		gyrodrift_z=gyro_drift_z/drift_count;//求解偏置方便后面减去
		modi_g=drift_count/acce_modi*real_g;//导航开始时刻对陀螺的零偏及加速度计中真正的G进行求解
	    accedrift_x=acce_drift_x/drift_count;
		accedrift_y=acce_drift_y/drift_count;
	    accedrift_z=acce_drift_z/drift_count;//求解偏置，方便后面进行姿态对准

		AlignAtti(accedrift_x,accedrift_y,accedrift_z);//求解初始的姿态角
        AlignPosi();//初始值设置--位置
		AlignVelo();//初始值设置--速度
	    align_number++;
		}
		else
		{

		/////////////////////////////先将陀螺减去偏置//////////////////////////////////
        file_msg[5]=file_msg[5]-gyrodrift_x;
		file_msg[6]=file_msg[6]-gyrodrift_y;
		file_msg[7]=file_msg[7]-gyrodrift_z;//陀螺减去偏置
		PedNavi(file_msg);//将参数传入后面解算
		align_number++;
		}
          /////////////////////将参数显示在界面上（以下）/////////////////////////
			
			static int x_max=2,x_min=-2,y_max=2,y_min=-2;
                  
			if(posi[0]>x_max)
			{
				x_max=int(posi[0]+5);
			}
			if(posi[0]<x_min)
			{
				x_min=int(posi[0]-5);
			}
			if(posi[1]>y_max)
			{
				y_max=int(posi[1]+5);
			}
			if(posi[1]<y_min)
			{
				y_min=int(posi[1]-5);
 			}//实时改变坐标

			m_ntgraph1.PlotXY(posi[0],posi[1],0);

			m_ntgraph1.SetRange(x_min,x_max,y_min,y_max);//可以设置为实时变化
		
		    m_testvalue1=modi_g;
			m_posi1=posi[0];
			m_posi2=posi[1];
			m_posi3=posi[2];
			m_velo1=velo[0];
			m_velo2=velo[1];
			m_velo3=velo[2];
			m_atti1=atti[0];
			m_atti2=atti[1];
			m_atti3=atti[2];

			UpdateData(false);
		//	Sleep(200);

		////////////////////将参数显示在界面上（以上）//////////////

  

	  /////////////////////////////////////////////////////////////
   }
 
   
	   fcloseall();//关闭所有文件
       KillTimer(2);
   

   }

	

	CDialog::OnTimer(nIDEvent);
}



//////////////////////////////////////////////////////////////
///读文件导航函数，此函数直接从文件中读取数据进行导航
///功能：读文件导航
/////////////////////////////////////////////////////////////

void CPed_naviDlg::OnButton5() 
{
	// TODO: Add your control notification handler code here
	static int file_open_flag=0;
	
	
	
	if(file_open_flag==0)//判断是否第一次打开文件
	{
	direct_file_in=fopen("imu_data21.dat","r");//读入文件的路径
	navi_file=fopen("navi_data.dat","w");//导航数据输出路径
	file_open_flag=1;

	
	/////////////////////////////启动定时器并初始化画图///////////////////////
    
    //控件初始化

	m_ntgraph1.ClearGraph();
	m_ntgraph1.SetElement(0);
	m_ntgraph1.SetElementLineColor(RGB(0,255,0));//连线的颜色
	m_ntgraph1.SetElementIdentify(TRUE);
	m_ntgraph1.SetElementPointColor(RGB(255,0,0));//点的颜色
	m_ntgraph1.SetElementPointSymbol(4);//点的形状
	m_ntgraph1.SetElementWidth(1);//线条和点的宽度
	m_ntgraph1.SetElementSolidPoint(TRUE);//点是实心的还是空心的
    /////////////////////////////////////////////////////////////////////////////////

	}

	 gyro_drift_x=0;
     gyro_drift_y=0;
     gyro_drift_z=0;//对准时刻陀螺的零偏的累加
     gyrodrift_x=0;
     gyrodrift_y=0;
     gyrodrift_z=0;//导航开始之前陀螺的零偏的求解
     acce_modi_x=0;
     acce_modi_y=0;
     acce_modi_z=0;//对准时刻加表的零偏
     drift_count=0;//计算初始对准时候的值的个数
     modi_g=0;
	 accedrift_x=0;
	 accedrift_y=0;
	 accedrift_z=0;//导航开始之前陀螺的加表累加之后的求平均
	 acce_drift_x=0;
	 acce_drift_y=0;
	 acce_drift_z=0;//导航开始之前陀螺的加表的累加

	 

    ////////////////////////////////////////////////////////////////////////
    SetTimer(2,1000,NULL);//启动定时器进行读数据解算
	////////////////////////////////////////////////////////////////////////


}
 
 
//////////////////////////////////////////////////////////////
///函数名：OnAccebegin() 
///实现功能：进行加速度计的在线标定，求出零偏代入程序
/////////////////////////////////////////////////////////////


void CPed_naviDlg::OnAccebegin() 
{
	// TODO: Add your control notification handler code here
	
    func_flag=3;//功能标志位设为3，表示加速度计在线标定状态
	////////以下是对串口的初始化/////////	
	m_comm.SetCommPort(4);//指定串口号
	m_comm.SetSettings("115200,N,8,1");//通信参数设置
    m_comm.SetInBufferSize(2048);//指定缓冲区大小
	m_comm.SetInBufferCount(0);//清空接收缓冲区
	m_comm.SetInputMode(1);//设置数据获取方式，1为二进制方式，0为文本方式
	m_comm.SetInputLen(0);//设置每次获取长度，0为默认值，表示全部
	m_comm.SetRThreshold(1);//产生oncomm事件的阀值
   ////////设置完成以后打开串口进行数据的读取/////////
	m_comm.SetPortOpen(1);//打开串口  
    //////////////二进制数据文件的打开////////////////
    in_file_test=fopen("com_test_data_bd1.dat","wb");/////加速度计标定阶段二进制文件的存放位置
	imu_file=fopen("imu_data_bd1.dat","w");//文件存储位置 

}


//////////////////////////////////////////////////////////////
///函数名：OnAcceend() 
///实现功能：结束加表标定过程----一些结束的收尾工作，标志位置零工作在unpack。cpp中已经进行
///此外，该步还可以将零偏值显示在页面上
//在unpack。cpp中对func_flag置零，这里关闭串口读文件，会带来的问题有，_db文件中会有一些数据拖尾，但影响应该不大--2013-05-08by wan
/////////////////////////////////////////////////////////////
void CPed_naviDlg::OnAcceend() 
{
	// TODO: Add your control notification handler code here
	m_comm.SetPortOpen(0);//关闭串口
	fcloseall();//关闭文件

    func_flag=0;
	roll_front=0;
	roll_back=0;
	roll_front_idx=0;
	roll_back_idx=0;//将环形缓冲区的指针都设为零   


}

void CPed_naviDlg::OnAccebegin2() //位置2标定开始
{
	// TODO: Add your control notification handler code here
    
 

	
	func_flag=4;//功能标志位设为4，表示加速度计在线标定2状态
	////////以下是对串口的初始化/////////	
	m_comm.SetCommPort(4);//指定串口号
	m_comm.SetSettings("115200,N,8,1");//通信参数设置
    m_comm.SetInBufferSize(2048);//指定缓冲区大小
	m_comm.SetInBufferCount(0);//清空接收缓冲区
	m_comm.SetInputMode(1);//设置数据获取方式，1为二进制方式，0为文本方式
	m_comm.SetInputLen(0);//设置每次获取长度，0为默认值，表示全部
	m_comm.SetRThreshold(1);//产生oncomm事件的阀值
   ////////设置完成以后打开串口进行数据的读取/////////
	m_comm.SetPortOpen(1);//打开串口  
    //////////////二进制数据文件的打开////////////////
    in_file_test=fopen("com_test_data_bd2.dat","wb");/////加速度计标定阶段二进制文件的存放位置
	imu_file=fopen("imu_data_bd2.dat","w");//文件存储位置 	
}

void CPed_naviDlg::OnAcceend2() //位置2标定结束
{
	// TODO: Add your control notification handler code here
		m_comm.SetPortOpen(0);//关闭串口
	    fcloseall();//关闭文件
		func_flag=0;
		roll_front=0;
		roll_back=0;
		roll_front_idx=0;
		roll_back_idx=0;//将环形缓冲区的指针都设为零   
}

void CPed_naviDlg::OnAccebegin3() //位置3标定开始
{
	// TODO: Add your control notification handler code here
		// TODO: Add your control notification handler code here
   

	
	func_flag=5;//功能标志位设为5，表示加速度计在线标定3状态
	////////以下是对串口的初始化/////////	
	m_comm.SetCommPort(4);//指定串口号
	m_comm.SetSettings("115200,N,8,1");//通信参数设置
    m_comm.SetInBufferSize(2048);//指定缓冲区大小
	m_comm.SetInBufferCount(0);//清空接收缓冲区
	m_comm.SetInputMode(1);//设置数据获取方式，1为二进制方式，0为文本方式
	m_comm.SetInputLen(0);//设置每次获取长度，0为默认值，表示全部
	m_comm.SetRThreshold(1);//产生oncomm事件的阀值
   ////////设置完成以后打开串口进行数据的读取/////////
	m_comm.SetPortOpen(1);//打开串口  
    //////////////二进制数据文件的打开////////////////
    in_file_test=fopen("com_test_data_bd3.dat","wb");/////加速度计标定阶段二进制文件的存放位置
	imu_file=fopen("imu_data_bd3.dat","w");//文件存储位置 	
	
}

void CPed_naviDlg::OnAcceend3() //位置3标定结束
{
	// TODO: Add your control notification handler code here
	m_comm.SetPortOpen(0);//关闭串口
	fcloseall();//关闭文件	

	func_flag=0;
	roll_front=0;
	roll_back=0;
	roll_front_idx=0;
	roll_back_idx=0;//将环形缓冲区的指针都设为零   

    double acce_db[3][3]={{0,0,0},{0,0,0},{0,0,0}};//用于存放静止时刻的均值
    int i=0;
	int j=0;
		for(i=0;i<3;i++)	
		{
			for(j=0;j<3;j++)
				{
				acce_db[i][j]=0.0f;
				}
		}


		FILE *ttest;
		ttest=fopen("ttest.dat","w");


	
		
	for(i=0;i<(abdcount1);i++)
	{
	acce_db[0][0]=acce_db[0][0]+acce_db_x1[i];
	acce_db[0][1]=acce_db[0][1]+acce_db_y1[i];
	acce_db[0][2]=acce_db[0][2]+acce_db_z1[i];
  

	}
	for(i=0;i<(abdcount2);i++)
	{
	acce_db[1][0]=acce_db[1][0]+acce_db_x2[i];
	acce_db[1][1]=acce_db[1][1]+acce_db_y2[i];
	acce_db[1][2]=acce_db[1][2]+acce_db_z2[i];

	}
	for(i=0;i<(abdcount3);i++)
	{
	acce_db[2][0]=acce_db[2][0]+acce_db_x3[i];
	acce_db[2][1]=acce_db[2][1]+acce_db_y3[i];
	acce_db[2][2]=acce_db[2][2]+acce_db_z3[i];
	}







   
acce_db[0][0]=acce_db[0][0]/(abdcount1);
	    acce_db[0][1]=acce_db[0][1]/(abdcount1);
		    acce_db[0][2]=acce_db[0][2]/(abdcount1);

  acce_db[1][0]=acce_db[1][0]/(abdcount2);
	    acce_db[1][1]=acce_db[1][1]/(abdcount2);
		    acce_db[1][2]=acce_db[1][2]/(abdcount2);

  acce_db[2][0]=acce_db[2][0]/(abdcount3);
	    acce_db[2][1]=acce_db[2][1]/(abdcount3);
		    acce_db[2][2]=acce_db[2][2]/(abdcount3);
	 fprintf(ttest,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",acce_db[0][0],acce_db[0][1],acce_db[0][2],acce_db[1][0],acce_db[1][1]
		 ,acce_db[1][2],acce_db[2][0],acce_db[2][1],acce_db[2][2]); 
     acce_cali(acce_db);


//	  FILE *testtt;
// 	
// 
//  testtt=fopen("testtt.dat","w");
//  
//   
// 	fprintf(testtt,"%d %d %d/n ",abdcount1,abdcount2,abdcount3);


    m_accebasis1=acce_basis[0];
	m_accebasis2=acce_basis[1];
	m_accebasis3=acce_basis[2];//将零偏值显示在界面上
	UpdateData(false);
}

void CPed_naviDlg::Onguanyu() 
{
	// TODO: Add your command handler code here
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();

	
}

void CPed_naviDlg::Onopen() 
{
	// TODO: Add your command handler code here
	
	// fopen( "navi_data.dat" , "rb" );
  ::ShellExecute(NULL, "open", "C:\\Users\\wan\\Desktop\\ins\\ped_navi\\navi_data.dat", NULL, NULL, SW_SHOWNORMAL);

}

//void CPed_naviDlg::OnButton8() 
//{
	// TODO: Add your control notification handler code here
//	::ShellExecute(NULL, "open", "C:\Users\wan\Desktop\惯性个人定位软件\ped_navi\\1.dat", NULL, NULL, SW_SHOWNORMAL);
//}
 