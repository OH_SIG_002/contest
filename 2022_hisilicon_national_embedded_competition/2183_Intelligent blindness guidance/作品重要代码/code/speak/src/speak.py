#! /usr/bin/python
# encoding=utf-8
from tts_sdk.iflytek_tts import IflytekTTS
import rospy
from std_msgs.msg._String import String
import os
import time

APP_ID = '9567bf17'
def get_str(content):
    # speak = '这是'+content.data
    speak = content.data
    print content.data
    tts = IflytekTTS(APP_ID)
    tts.text2wav(speak, './speak.wav')
    time.sleep(0.1)
    os.system('play ./speak.wav')

if __name__ == '__main__':
    rospy.init_node('speak')
    rospy.Subscriber('/bibi',String,callback=get_str,queue_size=1)
    rospy.spin()

