/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IOT_CONFIG_H
#define IOT_CONFIG_H

// <CONFIG THE LOG
/* if you need the iot log for the development , please enable it, else please comment it */
#define CONFIG_LINKLOG_ENABLE   1

// < CONFIG THE WIFI
/* Please modify the ssid and pwd for the own */
#define CONFIG_AP_SSID  "Redmi" // WIFI SSID
#define CONFIG_AP_PWD   "20090441090WZh" // WIFI PWD

#define CONFIG_USER_ID    "YQYG57YFM0Hi3861;12010126;d73f5;1664899200"
#define CONFIG_USER_PWD   "754e9df89bc5ca3fb14e1225790eae36fa321292236eecb6944f468ff8661154;hmacsha256"
#define CN_CLIENTID     "YQYG57YFM0Hi3861" // Tencent cloud ClientID format: Product ID + device name

#endif
