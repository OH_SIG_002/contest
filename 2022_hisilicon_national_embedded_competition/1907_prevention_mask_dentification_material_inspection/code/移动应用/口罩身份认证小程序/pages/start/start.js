const app = getApp()

Page({
  data: {
    swiper_num: '',
    windowHeight: "",
    windowWidth: "",
    button_height: "",
    fun_info: [
      {
        icon:"location",
        title:"人脸入库",
        eng: "Measurement",
        color: 'rgb(63,68,136)',
      },
      {
        icon:"safe",
        title:"口罩检测",
        eng: "Epidemic",
        color: 'rgb(27,33,123)',
      }
    ]
  },
  function1: function () {
    wx.navigateTo({
      url: '../face/facelist/facelist',
    })
  },
  function2: function () {
    wx.navigateTo({
      url: '../mask/masklist/masklist',
    })
  },

  onLoad: function (options) {
    var that = this
    //获取信息
    wx.getSystemInfo({
      success(res) {
        that.setData({
          windowWidth: res.windowWidth
        })
        that.setData({
          windowHeight: res.windowHeight
        })
        var button_height = res.windowHeight * 750 / res.windowWidth - 615
        that.setData({
          button_height: button_height
        })
      }
    })
  }
})