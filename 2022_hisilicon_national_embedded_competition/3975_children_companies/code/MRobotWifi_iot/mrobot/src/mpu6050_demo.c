/**
 * 
 *    采集MPU 6050 原始数据 
 *处理通过串口发出，使用python脚本解析后执行相应动作
 * Code By： HelloKun  2021.09.06
 * 
 */
#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_errno.h" //==IOT_SUCCESS =0  

#include "hi_io.h"   //上拉、复用
#include "hi_gpio.h" //hi_gpio_set_dir()、hi_gpio_set(get)_output(input)_val()
#include "iot_gpio.h"//gpioInit
#include "hi_time.h"
#include "hi_i2c.h"
#include "mpu6050.h"

static void MPUTask(void *arg)
{
    (void) arg;
    short aacx,aacy,aacz;		//加速度传感器原始数据
	short gyrox,gyroy,gyroz;	//陀螺仪原始数据
	uint8_t temp;				//温度	    



    IoTGpioInit(9);
    IoTGpioSetDir(9,IOT_GPIO_DIR_OUT);         //==载板led初始化
    IoTGpioSetOutputVal(9,0);// 只要一直初始化不成功，则灯不闪
    
    MPU_Init(); //返回0 成功
    while (MPU_Init())
    {
        printf("MPU Initialize Faild \n");
    }
    
    while(1) //连接成功 led 一直闪烁
    {   
        static char text[128] = {0};
        temp = MPU_Get_Temperature();
        MPU_Get_Gyroscope(&gyrox,&gyroy,&gyroz);
        MPU_Get_Accelerometer(&aacx,&aacy,&aacz);
        if(aacx<4000 && aacx>1000)
        {
            //printf("a");
            if(aacy<-2000)
            {
             printf("al\n");
            }
            else if(aacy>3000)
            {
             printf("ar\n");
            }
            else printf("a\n");
        }
        else if(aacx>4000 && aacx<7000) 
        {
            if(aacy<-2000)
            {
             printf("zl\n");
            }
            else if(aacy>3000)
            {
             printf("zr\n");
            }
            else printf("z\n");
        }
        else if((aacx>7000) && (aacy<1000) && (aacy>-1000))
        {
            printf("t\n");
        } 
        else 
        {
            printf("t\n");
        }
        // printf("temp: %d\n",temp);
        // printf("gyrox: %d\n",gyrox);
        // printf("gyroy: %d\n",gyroy);
        // printf("gyroz: %d\n",gyroz); 
        //printf("%d:%d\n",aacx,aacy);
        //printf("aacz: %d\n",aacz);

        IoTGpioSetOutputVal(9,0); 
        usleep(30000);  
        IoTGpioSetOutputVal(9,1); 
        usleep(30000); 
        //Print_Original_MPU_Data();
        usleep(500*1000);  //不要发太快，不然python算不过来
    }
}

static void MPUDemo(void)
{
    osThreadAttr_t attr;

    attr.name = "MPUTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = osPriorityNormal;

    if (osThreadNew(MPUTask, NULL, &attr) == NULL) {
        printf("[MPUDemo] Falied to create MPUTask!\n");
    }
}
SYS_RUN(MPUDemo);

 
/*** hi3861 2.0版本 i2c 使用流程（以oled、mpu6050为例）2021.09.27 ByHelloKun
 *   相关函数见： hi_i2c.h  i2c.c
 * 
 *  //==i2c1 
 *  IoTGpioInit(I2C1_SDA_IO13);
    IoTGpioInit(I2C1_SCL_IO14); //初始化io接口
    hi_io_set_func(I2C1_SDA_IO13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(I2C1_SCL_IO14 , HI_IO_FUNC_GPIO_14_I2C0_SCL); //==GPIO 复用为I2C0
    hi_i2c_init(OLED_I2C_IDX, OLED_I2C_BAUDRATE);                //==使能i2c1设置传输速率
    hi_i2c_write((hi_i2c_idx)id, OLED_I2C_ADDR, &i2cData);
    

    //==i2c0 
    IoTGpioInit(MPU_SDA_IO0);
    IoTGpioInit(MPU_SCL_IO1);
    hi_io_set_func(MPU_SDA_IO0, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(MPU_SCL_IO1, HI_IO_FUNC_GPIO_14_I2C0_SCL);
    hi_i2c_init(MPU_I2C_IDX, MPU_I2C_BAUDRATE);
    hi_i2c_write((hi_i2c_idx)id, ((MPU_ADDR<<1)|0), &i2cData);  //==发送器件地址+写命令+目标寄存器reg + 写入的data
    hi_i2c_read((hi_i2c_idx)id,((MPU_ADDR<<1)|1), &i2cData);    //==发送器件地址+读命令+目标寄存器reg + 读取的data


 */   