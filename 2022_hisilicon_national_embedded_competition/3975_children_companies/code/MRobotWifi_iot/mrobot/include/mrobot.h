#ifndef _MROBOT_H__
#define _MROBOT_H__

#define TURN_ON_FAN "turn on fan"
#define TURN_OFF_FAN "turn off fan"
#define TURN_ON_LAMB "turn on lamb"
#define TURN_OFF_LAMB "turn off lamb"
#define OPEN_DOOR "open door"
#define CLOSE_DOOR "close door"
#define GO_AHEAD_WATER "go ahead water"
#define STOP_WATER "stop_water"
#define TURN_LEFT "L"
#define TURN_RIGHT "R"
#define FORWARD "F"
#define BACKWARD "B"
#define RUN_CIRCLE "C"


#endif