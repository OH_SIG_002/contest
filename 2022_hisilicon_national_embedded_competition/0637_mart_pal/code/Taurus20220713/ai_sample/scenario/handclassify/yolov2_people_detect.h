/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef YOLOV2_PEOPLE_DETECT_H
#define YOLOV2_PEOPLE_DETECT_H

#include "hi_type.h"

#if __cplusplus
extern "C" {
#endif


HI_S32 PeopleDetectCal(IVE_IMAGE_S *srcYuv, DetectObjInfo resArr[]);
HI_S32 PeopleDetectInit();
HI_S32 PeopleDetectExit();

HI_S32 BarcodeDetectCal(IVE_IMAGE_S *srcYuv, DetectObjInfo resArr[]);
HI_S32 BarcodeDetectInit();
HI_S32 BarcodeExit();




#ifdef __cplusplus
}
#endif
#endif
