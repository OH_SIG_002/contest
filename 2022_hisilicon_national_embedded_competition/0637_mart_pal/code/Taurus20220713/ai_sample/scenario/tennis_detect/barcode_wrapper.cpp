#include "barcode_wrapper.h"
#include "tennis_detect.h"
#include "sample_comm_nnie.h"

#ifdef __cplusplus
extern "C" {
#endif

// struct tag_tennis_detect
// {
//     tennis_detect tag;
// };

// struct tag_tennis_detect *GetBarcodeStruct(void)
// {
//     return new struct tag_tennis_detect;
// }

// void ReleaseBarcodeStruct(struct tag_tennis_detect **ppStruct)
// {
//     delete *ppStruct;
//     *ppStruct = 0;
// }

HI_S32 TennisDetectCal_C(struct tennis_detect *p,uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm,long long int * info_out)//, VIDEO_FRAME_INFO_S *dstFrm)
{
    return p->TennisDetectCal(model,srcFrm,info_out);//,dstFrm);
     SAMPLE_PRT("-------- * info_out in TennisDetectCal_C : %lld --------\n", * info_out);
}


#ifdef __cplusplus
}
#endif