/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef Trash_CLASSIFY_H
#define Trash_CLASSIFY_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "hi_comm_video.h"
#include "osd_img.h"



#if __cplusplus
extern "C" {
#endif

#define ARGB1555_RED        0xFC00 // 1 11111 00000 00000
#define ARGB1555_GREEN      0x83E0 // 1 00000 11111 00000
#define ARGB1555_BLUE       0x801F // 1 00000 00000 11111
#define ARGB1555_YELLOW     0xFFE0 // 1 11111 11111 00000
#define ARGB1555_YELLOW2    0xFF00 // 1 11111 11111 00000
#define ARGB1555_WHITE      0xFFFF // 1 11111 11111 11111
#define ARGB1555_BLACK      0x8000 // 1 00000 00000 00000

#define RGB888_RoyalBlue        ((uint32_t)0x4876FF)
#define RGB888_Gold             ((uint32_t)0xFFD700)

/* load Trash detect and classify model */
HI_S32 Yolo2TrashDetectResnetClassifyLoad(uintptr_t* model, OsdSet* osds);

/* unload Trash detect and classify model */
HI_S32 Yolo2TrashDetectResnetClassifyUnload(uintptr_t model);

/* Trash detect and classify calculation */
HI_S32 Yolo2TrashDetectResnetClassifyCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm);

#ifdef __cplusplus
}
#endif
#endif
