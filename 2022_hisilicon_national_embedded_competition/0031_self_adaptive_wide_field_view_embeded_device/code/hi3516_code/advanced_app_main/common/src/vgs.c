#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hi_type.h"
#include "hi_comm_video.h"
#include "hi_comm_vgs.h"

#include "ive.h"
#include "queue.h"
#include "vgs.h"


int SAMPLE_COMM_VGS_FillRect(VIDEO_FRAME_INFO_S *pstFrmInfo, SAMPLE_RECT_ARRAY_S *pstRect, HI_U32 u32Color)
{
    int VgsHandle = -1;
    int s32Ret = HI_SUCCESS;
    unsigned short i;
    VGS_TASK_ATTR_S stVgsTask;
    VGS_ADD_COVER_S stVgsAddCover;

    if(pstFrmInfo == NULL)
    {
        printf("[FAIL] pstFrmInfo is NULL!\n");
        return HI_FAILURE;
    }
    if(pstRect == NULL)
    {
        printf("[FAIL] pstRect is NULL!\n");
        return HI_FAILURE;
    }

    if (pstRect->u16Num == 0) 
    {
        return s32Ret;
    }
    s32Ret = HI_MPI_VGS_BeginJob(&VgsHandle);
    if (s32Ret != HI_SUCCESS) 
    {
        printf("[FAIL] Vgs begin job fail! in ive.c\n");
        return s32Ret;
    }

    (void)memcpy_s(&stVgsTask.stImgIn, sizeof(VIDEO_FRAME_INFO_S), pstFrmInfo, sizeof(VIDEO_FRAME_INFO_S));
    (void)memcpy_s(&stVgsTask.stImgOut, sizeof(VIDEO_FRAME_INFO_S), pstFrmInfo, sizeof(VIDEO_FRAME_INFO_S));

    stVgsAddCover.enCoverType = COVER_QUAD_RANGLE;
    stVgsAddCover.u32Color = u32Color;
    for (i = 0; i < pstRect->u16Num; i++)
    {
        stVgsAddCover.stQuadRangle.bSolid = HI_FALSE;
        stVgsAddCover.stQuadRangle.u32Thick = 2; /* thick value: 2 */
        (void)memcpy_s(stVgsAddCover.stQuadRangle.stPoint, sizeof(pstRect->astRect[i].astPoint),
            pstRect->astRect[i].astPoint, sizeof(pstRect->astRect[i].astPoint));
        
        s32Ret = HI_MPI_VGS_AddCoverTask(VgsHandle, &stVgsTask, &stVgsAddCover);
        if (s32Ret != HI_SUCCESS)
        {
            printf("[FAIL] HI_MPI_VGS_AddCoverTask fail: %x\n", s32Ret);                        
            HI_MPI_VGS_CancelJob(VgsHandle);
            return s32Ret;
        }
    }

    s32Ret = HI_MPI_VGS_EndJob(VgsHandle);
    if (s32Ret != HI_SUCCESS) 
    {
        printf("[FAIL] HI_MPI_VGS_EndJob fail: %x\n", s32Ret);
        HI_MPI_VGS_CancelJob(VgsHandle);
        return s32Ret;
    }

    return s32Ret;
}
