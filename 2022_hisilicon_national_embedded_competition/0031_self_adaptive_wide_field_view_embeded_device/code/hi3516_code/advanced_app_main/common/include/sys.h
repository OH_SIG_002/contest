#ifndef __COMMON_SYS_H__
#define __COMMON_SYS_H__

#include "hi_types.h"

HI_S32 SYS_Init();

HI_VOID SYS_DeInit();

#endif