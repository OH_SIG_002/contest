#ifndef __COMMON_VPSS_H__
#define __COMMON_VPSS_H__

#include "hi_types.h"
#include "hi_common.h"

HI_S32 VPSS_Init(VPSS_GRP VpssGrp, VPSS_CHN VpssChn);

HI_VOID VPSS_DeInit(VPSS_GRP VpssGrp, VPSS_CHN VpssChn);

#endif