from PyQt5.QtCore import QObject, QThread, QDateTime, pyqtSignal
from PyQt5.QtWidgets import QWidget, QPushButton, QApplication, QDialog, QLineEdit
from huaweicloudsdkcore.auth.credentials import BasicCredentials
from huaweicloudsdkiotda.v5.region.iotda_region import IoTDARegion
from huaweicloudsdkcore.exceptions import exceptions
from huaweicloudsdkiotda.v5 import *
VERTUALDEVICE = False
import time

class CommandSender():
    def __init__(self):
        if VERTUALDEVICE:
            self.ak = "64EWXS9MKAQHXJ1R4PR4"  # "<YOUR AK>"
            self.sk = "HmSPDcDAJci2xiygQQNmqGTmZtkWs8BftBIJXNRa"  # "<YOUR SK>"
            self.deviceId = "6291c85cacbbb071175113c6_1653723364791"
        else:
            self.ak = "NHWSTUWPK2N3PC2QCVEN" # "<YOUR AK>"
            self.sk = "JZyO1HBjrSJauAuEgN8ZW1ioBbmCRw4UvvB0Jaje"  # "<YOUR SK>"
            self.deviceId = "625bbdffecf9c41c382130bd_hispark_trafficlight"

        self.credentials = BasicCredentials(self.ak, self.sk)
        self.client = IoTDAClient.new_builder() \
            .with_credentials(self.credentials) \
            .with_region(IoTDARegion.value_of("cn-north-4")) \
            .build()

    def sendautomodecommand(self):
        try:
            request = CreateCommandRequest()
            request.device_id = self.deviceId
            request.body = DeviceCommandRequest(
                command_name="AutoModule",
                service_id="TrafficLight"
            )
            self.client.create_command(request)
            return True
        except:
            print("send auto command error")
            return False

    def sendcontrolcommand(self, direction, time_int):
        try:
            request = CreateCommandRequest()
            request.device_id = self.deviceId
            request.body = DeviceCommandRequest(
                paras={direction: time_int},
                command_name="ControlModule",
                service_id="TrafficLight"
            )
            self.client.create_command(request)
            return True
        except:
            print("send control command error")
            return False
            pass