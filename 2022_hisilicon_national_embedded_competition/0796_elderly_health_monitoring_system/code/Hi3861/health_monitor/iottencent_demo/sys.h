

#ifndef __sys_h
#define __sys_h



typedef unsigned          char uint8_t;
typedef unsigned short     int uint16_t;
typedef unsigned           int uint32_t;

//typedef unsigned       __int64 uint64_t;
typedef unsigned char           hi_uchar;
typedef unsigned char           hi_u8;
typedef unsigned short          hi_u16;
typedef unsigned int            hi_u32;
#define u8 hi_u8
#define u16 hi_u16
#define u32 hi_u32


typedef   signed          char int8_t;
typedef   signed short     int int16_t;
typedef   signed           int int32_t;
//typedef   signed       __int64 int64_t;

#endif /* __sys_h */

/* end of stdint.h */



