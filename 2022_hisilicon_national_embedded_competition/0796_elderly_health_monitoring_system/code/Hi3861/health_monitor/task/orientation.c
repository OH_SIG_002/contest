



#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <ohos_init.h>
#include <cmsis_os2.h>
#include <iot_i2c.h>
#include <iot_gpio.h>
#include <iot_errno.h>
#include <hi_uart.h>
#include <hi_io.h>
#include "orientation.h"
#include <iot_gpio_ex.h>
#include <hi_task.h>







static void *OrienTask(const char *arg)
{	
	(void)arg;
    GPS_init();
	while(1)
	{	
		USART_Read();
		parseGpsBuffer();
		printGpsBuffer();
        //printf("one loop end\n");
		//printf("\n");
	}
}

static void OrienDemo(void)
{
	
    osThreadAttr_t attr;
    attr.name = "OrienTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /*С4096 */
    attr.priority = osPriorityNormal;
    if (osThreadNew((osThreadFunc_t)OrienTask, NULL, &attr) == NULL) {
        printf("[UartDemo] Falied to create OrienTask!\n");
    }
}
SYS_RUN(OrienDemo);

//APP_FEATURE_INIT(OrienDemo);


