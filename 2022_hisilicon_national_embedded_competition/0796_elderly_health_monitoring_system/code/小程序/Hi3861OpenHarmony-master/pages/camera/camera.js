Page({

  /**
   * 页面的初始数据
   */
  data: {
    video_data: {}, //要播放的视频对象
    fullScreen: false, //视频是否全屏
    recommendListArr: [],

  },

  //弹出视频图层。播放视频
  showVideo(e) {
    for (let i = 0; i < this.data.recommendListArr.length; i++) {
      if (this.data.recommendListArr[i].activityid === e.currentTarget.id) {
        var temprecommendList = this.data.recommendListArr[i]
      }
    }
    this.setData({
      video_data: temprecommendList,
    })

    var videoContext = wx.createVideoContext('myvideo', this);
    videoContext.requestFullScreen();

    this.setData({
      fullScreen: true
    })

  },

  closeVideo() {
    //执行退出全屏方法
    var videoContext = wx.createVideoContext('myvideo', this);
    videoContext.exitFullScreen();
  },

})