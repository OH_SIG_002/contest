# 基于视觉的自助停车场收费管理系统

### 赵家琦；徐昊；董博弢

## 第一部分 设计概述

### 1.1 设计目的
本设计旨在以海思Taurus & Pegasus AI计算机视觉基础开发套件为平台，
利用Taurus对图像处理的高性能，实现混合自助车场收费管理系统对车辆车
型的自主分类与车牌识别记录功能。该系统采用 **YoloV2** 目标检测算法，在
道闸处对出入车辆车型进行检测，识别车型后再采用 **EasyPR** 车牌识别算法，
识别车辆车牌完善车辆收费管理信息，实现混合车场自助收费管理，并可有
效减少人为盗刷现象，同时系统可将车场收费管理信息实时发送至管理平台，
方便监管。
### 1.2 应用领域
随着科技的不断发展，汽车已成为人们日常生活中不可或缺的重要部分，
为人们的生活提供了极大的便利。但根据调查显示，随着生活中车辆的不断
增加，各类停车场管理良莠不齐，自助车场仅识别车辆车牌作为收费管理信
息，现实生活中常出现混合车场收费管理困难、自助车场车牌图片盗刷等问
题。为解决上述问题，本作品提出一种基于视觉的自助停车场收费管理系统，
该系统在检测识别出目标车辆车型后再进行目标车辆车牌识别，同时获取车
辆车型与车牌信息，便于混合车场收费管理，并可有效减少人为盗刷现象。
### 1.3 主要技术特点
本系统采用 **YOLOv2** 目标检测网络模型，通过NNIE加速部署到板端，
并通过自制数据集训练，完成网络模型对不同车辆车型的检测，实现混合车
场智能收费管理。同时调用Opencv库，通过 **EasyPR** 车牌识别算法获取车
辆车牌信息，采用车牌检测算法通过人工神经网络（ANN）中的多层感知机
（MLP）模型识别输入视频流中目标车辆的车牌位置信息，并采用字符识别
算法通过光学字符识别（OCR）实现对车辆车牌的识别，实现车场自助收费
管理。
### 1.4 关键性能指标
本设计采用YoloV2目标检测算法，在车场道闸处对出入车场车辆进行
目标检测，识别车辆车型，同时采用EasyPR车牌识别算法，对识别出车型
的目标车辆进行检测，获取车辆车牌信息。因而本系统关键性能指标为
YoloV2网络模型对车型的识别准确率及EasyPR网络对车辆车牌识别的精确
度。目前，YoloV2网络对车辆车型检测准确率为 76 %左右，EasyPR网络对
车辆车牌识别准确率为 87 %左右，车牌识别精度为 84 %左右。同时可综合考
虑检测速度。
### 1.5 主要创新点
（ 1 ）本系统采用 **YoloV2** 目标检测算法，在车场道闸处对出入车场车辆进行
目标检测，识别车辆车型，实现混合车场收费管理，并可有效减少人为盗刷
现象发生。通过自制数据集训练，网络模型对车辆车型检测识别率可达 75 %
以上。
（ 2 ）本系统采用 EasyPR 车牌识别算法，对YoloV2目标检测算法识别出车
型的目标车辆再进行车牌识别，获取车辆车牌信息，实现车场自助收费管理，
EasyPR网络车牌字符识别精度可达 85 %以上。
（ 3 ）本作品通过自制数据集对 YoloV2 目标检测网络模型进行训练，使
YoloV2网络能检测出目标车辆车型信息，并通过EasyPR车牌识别算法获取
目标车辆车牌信息，同时将车场收费管理信息上传至监管平台，实现混合自
助车场智能化、便捷化收费管理。

## 第二部分 系统组成及功能说明

### 2.1 整体介绍
本系统以海思Taurus & Pegasus AI计算机视觉基础开发套件为平台，利
用Taurus对图像处理的高性能，实现混合自助车场收费管理系统对车辆车型
的自主分类与车牌识别记录功能。
```
图 1 海思Taurus & Pegasus AI计算机视觉基础开发套件连接图
```
该系统采用YoloV2目标检测算法，通过NNIE加速部署到Taurus板端，
在道闸处对出入车辆车型进行检测，识别车型后再采用EasyPR车牌识别算
法，识别车辆车牌完善车辆收费管理信息，实现混合车场自助收费管理，并
可有效减少人为盗刷现象，同时通过Taurus & Pegasus串口互联将车辆信息
传输至OLED屏显示，系统可将车场收费管理信息实时发送至管理平台，方
便监管。系统流程图如下。
```
图 2 系统整体流程图
```
### 2.2 各模块介绍
本系统以海思Taurus & Pegasus AI计算机视觉基础开发套件为平台，实
现对出入车场车辆车型的自主分类与车牌识别记录功能。该系统采用YoloV2
目标检测算法，在道闸处对出入车辆车型进行检测，识别车型后再采用
EasyPR 车牌识别算法，识别车辆车牌完善车辆收费管理信息，同时系统可
将车场收费管理信息通过Taurus & Pegasus串口互联实时发送并通过OLED
屏显示，方便监管。
#### 2 .2.1基于YoloV2的车辆车型识别
本系统基于Taurus对图像处理的高性能，采用YoloV2网络模型检测出
入车场车辆车型信息，实时检测Sensor视频流，并将检测到的车辆车型信息
通过串口互联实时发送同步至Pegasus板端。
#### 2. 2 .2基于EasyPR的车辆车牌识别
本系统基于Taurus对图像处理的高性能，采用EasyPR车牌识别算法对
识别出车型的目标车辆车牌进行检测，获取目标车辆车牌信息。并将检测到
的车辆车牌信息通过串口互联实时发送同步至Pegasus板端。
#### 2. 2. 3 基于Taurus & Pegasus串口互联的收费管理信息处理
本系统参考开发指南 Taurus & Pegasus 串口互联实验部分内容，将
Pegasus外设扩展板上的串口通过连接线与Taurus串口相连，将Taurus识别
检测出的车辆收费管理信息通过串口互联传输至 Pegasus Hi 3861 V100核心
板，并通过Pegasus OLED板显示串口获取到的车辆收费管理信息，同时可
将获取到的收费管理信息通过串口上传至上位机，方便监管，实现基于
**Taurus & Pegasus** 串口互联的车场车辆收费管理信息处理。
```
图 3 车场车辆收费管理信息互联互联显示示意图
```

## 第三部分 完成情况及性能参数

### 3.1 完成情况
本设计采用YoloV2目标检测算法，在车场道闸处对出入车场车辆进行
目标检测，识别出入车场车辆车型，同时采用EasyPR车牌识别算法对识别
出车型的目标车辆车牌进行检测，获取车辆车牌信息，实现混合自助车场智
能化收费管理。
#### 3.1.1 基于YoloV2的车辆车型检测
本系统参考开发指南ai sample部分内容搭建后续检测网所需环境，同时
参考手部检测及手势识别部分源码，构建基于YoloV2的车辆车型检测网络
模型，并完成检测网的板端部署，实时检测Sensor视频流。同时通过自制数
据集与训练，实现对车辆车型的实时检测。
#### 3 .1.2基于EasyPR的车辆车牌识别
本系统采用 EasyPR车牌识别算法对识别出车型的目标车辆车牌进行检
测，获取车辆车牌信息。EasyPR是一个基于Opencv库的开源中文车牌识别
系统，将EasyPR编译生成的动态库依赖文件libeastpr.so移植至板端，并在
主程序中对easypr方法进行调用，通过人工神经网络（ **ANN** ）中的多层感知
机（ **MLP** ）模型识别输入视频流中目标车辆的车牌位置信息，并采用字符识
别算法通过光学字符识别（ **OCR** ）实现对车辆车牌的识别，实现目标车辆车
牌信息识别，并将车辆信息通过串口互联发送至 Pegasus，方便后续收费管
理。
#### 3 .1.3基于YoloV 2 车辆车型检测的数据集制作与训练
本系统基于YoloV 2 网络模型对车辆车型进行实时检测，通过实景拍摄、
网络收集等方式获取数据集图片，并对数据集图片进行标注自制训练数据集，
同时对标注后的数据集进行了旋转、镜像、拉伸、亮度变化等数据增强处理，
提高了数据集中目标样本的多样性，有效增强了模型的鲁棒性及在不同光照
环境下模型的识别能力。
为保证模型训练效果，通过数据增强后自制数据集共包含图片 **4454** 张，
大小约 **900 MB** ，数据集囊括了日常生活中常见的几种车型，包括汽车（car），
面包车（van），巴士（bus），卡车（truck）等混合车场收费管理常涉及到的
不同车型。
```
图 4 数据集标注示意图
```
通过海思服务器对自制数据集进行训练，并通过对得到的权值文件进行
一系列转换、量化操作，最终将模型权值文件挂载至板端，实现对车辆车型
的检测。
```
图 5 模型量化示意图
```

### 3.2 性能参数
本设计采用YoloV2目标检测算法，在车场道闸处对出入车场车辆进行
目标检测，识别车辆车型，同时采用EasyPR车牌识别算法，对识别出车型
的目标车辆进行检测，获取车辆车牌信息。因而本系统关键性能指标为
YoloV2网络模型对车型的识别准确率及EasyPR网络对车辆车牌识别的精确
度。同时可综合考虑网络检测速度。
#### 3.2.1基于YoloV2的车辆车型识别
YoloV2网络模型采用了一个新特征提取器Darknet- 19 ，包括 19 个卷积
层和 5 个maxpooling层，每个卷积层后使用batch norm层加快收敛速度，降
低模型过拟合。
```
表 1 Darknet- 19 网络结构表
```
在ImageNet分类数据集上，Darknet- 19 的top- 1 准确度为72.9%，top- 5
准确度为91.2%，在使用Darknet- 19 之后，YoloV2计算量可减少约33%，有
效提高网络模型性能及检测速度。
本系统采用了在Caffe模型框架下的YoloV2检测网络对视频流中车辆车
型进行实时检测，检测包括日常生活中常见的几种车型，如汽车（car），面
包车（van），巴士（bus），卡车（truck）等合车场收费管理常涉及到的不同
车型。
训练测试数据集IOU平均高于0. 85 ，在板端运行时识别率成功率可达75%
左右，以下为常见车型检测识别结果。
```
（a）Car检测结果图
（b）Van Truck检测结果图
图 6 YoloV 2 车型识别结果图
```
#### 3.2.2 基于EasyPR的车辆车牌识别
为实现对视频流中目标车辆车牌的实时检测识别，本系统采用 EasyPR
车牌识别算法对识别出车型的目标车辆车牌进行检测，获取车辆车牌信息。
EasyPR主要包含车牌检测（Plate Detection）与字符识别（Chars Recognition）
两个重要步骤，算法流程图如下。
```
图 7 EasyPR车牌识别流程图
```
车牌检测部分对一个包含目标车辆的车牌的图像或视频流进行分析，通
过人工神经网络（ **ANN** ）中的多层感知机（ **MLP** ）模型识别输入视频流中
目标车辆的车牌位置信息，最终截取出只包含车牌的图块输出，可有效降低
在车牌识别过程中的计算量，并提升检测速率。
字符识别部分对车牌检测部分输出的处理后只包含车牌的图像，通过光
学字符识别（OCR）实现对车辆车牌的识别，提取车牌字符信息并通过串口
互联将车辆信息传输至Pegasus平台。
EasyPR车牌检测算法检测性能测试结果如下。
```
图 8 EasyPR检测性能测试结果图
```
当输入包含车牌的车辆图像时，网络会检测并识别车牌，同时输出识别
出的车牌字符串，EasyPR车牌识别效果如下。
```
图 9 EasyPR车牌识别结果图
图 10 EasyPR车牌检测结果图
```

## 第四部分 总结

### 4.1 可扩展之处
（ 1 ）提高YoloV2目标检测网络模型检测性能，通过优化自制数据集、添加
背景集、带权迭代训练等方法，提高YoloV2目标检测网络模型训练及检测
性能，并扩展可识别车辆车型数量，适应更复杂环境需求。
（ 2 ）便捷化混合自助车场收费管理流程。为实现混合自助车场收费管理便
捷化智能化，系统后续可通过开发 Pegasus，采用近场通信技术（ **NFC** ）将
车场收费管理信息实时上传至目标手机；同时开发混合自助车场收费管理小
程序，实现出入场车辆信息、收费管理信息等车场信息实时远程监控，实现
混合自助车场收费管理智能化，有效减少车场管理成本。
### 4.2 心得体会
初步设计时，并没有考虑到在安装系统和配置开发环境时会遇到各种问
题，遇到了不少 error走了不少弯路，但整个过程也极大的锻炼了我们的耐
性，让我们初步了解了嵌入式这个领域，对嵌入式开发初步入了门。
在项目的开发过程中，我们对于嵌入式系统的功能进行了深度的分析，
明确了小组中每个成员的工作及定位，合理的对开发任务进行了分工，加强
小组成员间的合作以及解决问题的能力。
调代码可以说是整个项目最令人头疼的部分。虽然这个过程给我们带来
了不少痛苦，可是从中大家也收获了许多，比如编程的一些实用技巧，亦或
是操作系统的一些快捷操作，甚至锻炼出了一颗不言放弃的坚定的心。学会
如何处理代码中 bug 是最主要的任务之一，通常调试 bug 时首先处理前面
的报错会减少不必要的工作量。
冲刺阶段的报告撰写也令我们收获颇多。不同于学校课堂上的实验报告，
竞赛作品的报告对我们来说也是极大的考验。在老师的指导下，我们不断地
删除、修正，最终写出一份颇具内涵的作品报告，从中我们收获了很多，为
以后的一系列论文撰写打下了基础。
总之，参加嵌入式大赛是一段非常充实且有意义的时光，大家在一起交
流、不断学习，曾一度想要放弃但抱着在坚持一下的想法，最后终于是完成
了这样一个作品，尽管不完美但结果是令我们满意的，这些经验会对今后我
们学习或是工作都会有所帮助。

## 第五部分 参考文献
[1]2022年嵌入式大赛海思赛道开发指导
[2]车牌识别easypr的详细介绍
[3]WANG Jian-lin, FU Xue-song, HUANG Zhan-chao, et al. Multi-type cooperative
targets detection using improved YOLOv2 convolutional neural network[J].
Guangxue Jingmi Gongcheng/Optics and Precision Engineering, 2020,
28(1):251-260.
[4] Li R , Yang J. Improved YOLOv2 Object Detection Model[C]// 2018 6th
International Conference on Multimedia Computing and Systems (ICMCS). 2018.
[5]李云鹏,侯凌燕,王超.基于 YOLOv2 的复杂场景下车辆目标检测[J].电视技
术,2018,42(05):100-106.DOI:10.16280/j.videoe.2018.05.023.
[6]龚静,曹立,亓琳,李良荣.基于 YOLOv2 算法的运动车辆目标检测方法研究[J].
电子科技,2018,31(06):5-8+12.DOI:10.16180/j.cnki.issn1007-7820.2018.06.002.
[7]王建林,付雪松,黄展超,郭永奇,王汝童,赵利强.改进 YOLOv2 卷积神经网络的
多类型合作目标检测[J].光学精密工程,2020,28(01):251-260.
[8]金宇尘,罗娜.结合多尺度特征的改进YOLOv2 车辆实时检测算法[J].计算机工
程与设计,2019,40(05):1457-1463+1476.DOI:10.16208/j.issn1000-7024.2019.05.047.
[9]雷维卓. 基于YOLOv2的实时目标检测研究[D].重庆大学,2018.
[10]李珣,时斌斌,刘洋,张蕾,王晓华.基于改进YOLOv2模型的多目标识别方法[J].
激光与光电子学进展,2020,57(10):113-122.
[11]刘智勇,刘迎建.车牌识别(LPR)中的图像提取及分割[J].中文信息学
报,2000(04):29-34.
[12]骆雪超,刘桂雄,冯云庆,申柏华.一种基于车牌特征信息的车牌识别方法[J].华
南理工大学学报(自然科学版),2003(04):70-73.
[13]阎建国,高亮,卢京潮.图象处理技术在车牌识别中的应用[J].电子技术应
用,2000(01):17-18.DOI:10.16157/j.issn.0258-7998.2000.01.006.

## 第六部分 附录
carclassify.c
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "sample_comm_nnie.h"
#include "sample_media_ai.h"
#include "ai_infer_process.h"
#include "yolov2_car_detect.h"
#include "vgs_img.h"
#include "ive_img.h"
#include "misc_util.h"
#include "hisignalling.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#define CAR_FRM_WIDTH 640
#define CAR_FRM_HEIGHT 384
#define DETECT_OBJ_MAX 4
#define RET_NUM_MAX 4
// Draw the width of the line
#define DRAW_RETC_THICK 2
#define WIDTH_LIMIT 32
#define HEIGHT_LIMIT 32
// The resolution of the model IMAGE sent to the classification is 224*
#define IMAGE_WIDTH 224


#define IMAGE_HEIGHT 224

// #define MODEL_FILE_GESTURE
"/userdata/models/hand_classify/hand_gesture.wk"

static int biggestBoxIndex;
static IVE_IMAGE_S img;
static DetectObjInfo objs[DETECT_OBJ_MAX] = {0};
static RectBox boxs[DETECT_OBJ_MAX] = {0};
static RectBox objBoxs[DETECT_OBJ_MAX] = {0};
static RectBox remainingBoxs[DETECT_OBJ_MAX] = {0};
static RectBox cnnBoxs[DETECT_OBJ_MAX] = {0}; // Store the results of the
classification network
static RecogNumInfo numInfo[RET_NUM_MAX] = {0};
static IVE_IMAGE_S imgIn;
static IVE_IMAGE_S imgDst;
static VIDEO_FRAME_INFO_S frmIn;
static VIDEO_FRAME_INFO_S frmDst;
int uartFd = 0;

HI_S32 Yolo2CarDetectResnetClassifyLoad(uintptr_t* model)
{
SAMPLE_SVP_NNIE_CFG_S *self = NULL;
HI_S32 ret = 0;

// ret = CnnCreate(&self, MODEL_FILE_GESTURE);
// SAMPLE_PRT("MODEL_FILE_GESTURE is %s\n",
MODEL_FILE_GESTURE);
// *model = ret < 0? 0 : (uintptr_t)self;

*model = 1;
CarDetectInit(); // Initialize the hand detection model
SAMPLE_PRT("Load car detect claasify model success\n");

/* uart open init */
uartFd = UartOpenInit();
if (uartFd < 0)
{
printf("uart1 open failed\r\n");
}
else
{


printf("uart1 open successed\r\n");
}
return ret;
}

HI_S32 Yolo2CarDetectResnetClassifyUnload(uintptr_t model)
{
// CnnDestroy((SAMPLE_SVP_NNIE_CFG_S*)model);
CarDetectExit(); // Uninitialize the hand detection model
SAMPLE_PRT("Unload hand detect claasify model success\n");
close(uartFd);

return 0;
}

/* Get the maximum hand */
static HI_S32 GetBiggestCarIndex(RectBox boxs[], int detectNum)
{
HI_S32 carIndex = 0;
HI_S32 biggestBoxIndex = carIndex;
HI_S32 biggestBoxWidth = boxs[carIndex].xmax - boxs[carIndex].xmin + 1;
HI_S32 biggestBoxHeight = boxs[carIndex].ymax - boxs[carIndex].ymin + 1;
HI_S32 biggestBoxArea = biggestBoxWidth * biggestBoxHeight;

for (carIndex = 1; carIndex < detectNum; carIndex++)
{
HI_S32 boxWidth = boxs[carIndex].xmax - boxs[carIndex].xmin + 1;
HI_S32 boxHeight = boxs[carIndex].ymax - boxs[carIndex].ymin + 1;
HI_S32 boxArea = boxWidth * boxHeight;
if (biggestBoxArea < boxArea)
{
biggestBoxArea = boxArea;
biggestBoxIndex = carIndex;
}
biggestBoxWidth = boxs[biggestBoxIndex].xmax -
boxs[biggestBoxIndex].xmin + 1;
biggestBoxHeight = boxs[biggestBoxIndex].ymax -
boxs[biggestBoxIndex].ymin + 1;
}

if ((biggestBoxWidth == 1) || (biggestBoxHeight == 1) || (detectNum == 0))
{


biggestBoxIndex = -1;
}

return biggestBoxIndex;
}

/* hand gesture recognition info */
static void CarDetectFlag(const RecogNumInfo resBuf)
{
HI_CHAR *carType = NULL;
switch (resBuf.num)
{
case 01:
carType = "car";
UartSendRead(uartFd, car);
SAMPLE_PRT("----gesture name----:%s\n", carType);
break;
case 02:
carType = "bus";
UartSendRead(uartFd, truck);
SAMPLE_PRT("----gesture name----:%s\n", carType);
break;
case 03:
carType = "minibus";
UartSendRead(uartFd, minibus);
SAMPLE_PRT("----gesture name----:%s\n", carType);
break;
case 04:
carType = "truck";
UartSendRead(uartFd, truck);
SAMPLE_PRT("----gesture name----:%s\n", carType);
break;
default:
carType = "unknown";
UartSendRead(uartFd, InvalidType); // 无效值
SAMPLE_PRT("----gesture name----:%s\n", carType);
break;
}
SAMPLE_PRT("car type success\n");
}

HI_S32 Yolo2CarDetectResnetClassifyCal(uintptr_t model,


VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm)
{
SAMPLE_SVP_NNIE_CFG_S *self = (SAMPLE_SVP_NNIE_CFG_S*)model;
HI_S32 resLen = 0;
int objNum;
int ret;
int num = 0;

ret = FrmToOrigImg((VIDEO_FRAME_INFO_S*)srcFrm, &img);
SAMPLE_CHECK_EXPR_RET(ret != HI_SUCCESS, ret, "car detect for YUV
Frm to Img FAIL, ret=%#x\n", ret);

objNum = CarDetectCal(&img, objs); // Send IMG to the detection net for
reasoning
for (int i = 0; i < objNum; i++)
{
cnnBoxs[i] = objs[i].box;
RectBox *box = &objs[i].box;
RectBoxTran(box, CAR_FRM_WIDTH, CAR_FRM_HEIGHT,
dstFrm->stVFrame.u32Width, dstFrm->stVFrame.u32Height);
SAMPLE_PRT("yolo2_out: {%d, %d, %d, %d}\n", box->xmin, box->ymin,
box->xmax, box->ymax);
boxs[i] = *box;
}
biggestBoxIndex = GetBiggestCarIndex(boxs, objNum);
SAMPLE_PRT("biggestBoxIndex:%d, objNum:%d\n", biggestBoxIndex,
objNum);

// When an object is detected, a rectangle is drawn in the DSTFRM
if (biggestBoxIndex >= 0)
{
objBoxs[0] = boxs[biggestBoxIndex];
MppFrmDrawRects(dstFrm, objBoxs, 1, RGB888_GREEN,
DRAW_RETC_THICK); // Target hand objnum is equal to 1

for (int j = 0; (j < objNum) && (objNum > 1); j++)
{
if (j != biggestBoxIndex)
{
remainingBoxs[num++] = boxs[j];
// others hand objnum is equal to objnum - 1
MppFrmDrawRects(dstFrm, remainingBoxs, objNum - 1,

RGB888_RED, DRAW_RETC_THICK);

}

}

// Crop the image to classification network
// ret = ImgYuvCrop(&img, &imgIn, &cnnBoxs[biggestBoxIndex]);
// SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "ImgYuvCrop FAIL,
ret=%#x\n", ret);

// if ((imgIn.u32Width >= WIDTH_LIMIT) && (imgIn.u32Height >=
HEIGHT_LIMIT))
// {
// COMPRESS_MODE_E enCompressMode =
srcFrm->stVFrame.enCompressMode;
// ret = OrigImgToFrm(&imgIn, &frmIn);
// frmIn.stVFrame.enCompressMode = enCompressMode;
// SAMPLE_PRT("crop u32Width = %d, img.u32Height = %d\n",
imgIn.u32Width, imgIn.u32Height);
// ret = MppFrmResize(&frmIn, &frmDst, IMAGE_WIDTH,
IMAGE_HEIGHT);
// ret = FrmToOrigImg(&frmDst, &imgDst);
// ret = CnnCalU8c1Img(self, &imgDst, numInfo, sizeof(numInfo) /
sizeof((numInfo)[0]), &resLen);
// SAMPLE_CHECK_EXPR_RET(ret < 0, ret, "CnnCalU8c1Img
FAIL, ret=%#x\n", ret);
// HI_ASSERT(resLen <= sizeof(numInfo) / sizeof(numInfo[0]));
// HandDetectFlag(numInfo[0]);
// MppFrmDestroy(&frmDst);
// }
// IveImgDestroy(&imgIn);
}

return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
easypr.cpp
#include <iostream>


#include "easypr.h"

using namespace easypr;

HI_S32 EasyPR()
{
int i;

CPlateRecognize pr; //创建一个CPlateRecognize类型对象pr

//设置EasyPR是否打开结果展示窗口
pr.setResultShow(false);
//设置EasyPR车牌定位算法
pr.setDetectType(PR_DETECT_CMSER); //CMER 文字定位方法,SOBEL
和COLOR边缘和颜色定位方法,可以通过"|"符号结合
//pr.setDetectType(PR_DETECT_COLOR | PR_DETECT_SOBEL);
//设置开启生活模式,在SOBEL时增大搜索范围,提高鲁棒性
//pr.setLifemode(true);
//设置EasyPR最多查找车牌数,最终输出可能性最高的n个车牌
pr.setMaxPlates(1);

//plateRecognize()方法有两个参数,第一个代表输入图像,第二个代表输出的
车牌CPlate集合
vector<CPlate> plateVec;
Mat src = imread("car.jpg");
int result = pr.plateRecognize(src, plateVec);

for(i = 0;i < plateVec.size();i++)
{
//车牌信息
CPlate plate = plateVec.at(i);
Mat plateMat = plate.getPlateMat(); //车牌图像
RotatedRect rrect = plate.getPlatePos(); //车牌可旋转矩形位置
string license = plate.getPlateStr(); //车牌字符串

// std::cout << "\ncar id: " << license.data() << "\n";
UartSendRead(uartFd, license.data());
}
}
