#include <iostream>

#include "easypr.h"

using namespace easypr;

HI_S32 EasyPR()
{
	int i;

	CPlateRecognize pr;		//创建一个CPlateRecognize类型对象pr

	//设置EasyPR是否打开结果展示窗口
	pr.setResultShow(false);
	//设置EasyPR车牌定位算法
	pr.setDetectType(PR_DETECT_CMSER);		//CMER文字定位方法,SOBEL和COLOR边缘和颜色定位方法,可以通过"|"符号结合
	//pr.setDetectType(PR_DETECT_COLOR | PR_DETECT_SOBEL);
	//设置开启生活模式,在SOBEL时增大搜索范围,提高鲁棒性
	//pr.setLifemode(true);
	//设置EasyPR最多查找车牌数,最终输出可能性最高的n个车牌
	pr.setMaxPlates(1);

	//plateRecognize()方法有两个参数,第一个代表输入图像,第二个代表输出的车牌CPlate集合
	vector<CPlate> plateVec;
	Mat src = imread("car.jpg");
	int result = pr.plateRecognize(src, plateVec);

	for(i = 0;i < plateVec.size();i++)
	{
		//车牌信息
		CPlate plate = plateVec.at(i);
		Mat plateMat = plate.getPlateMat();		//车牌图像
		RotatedRect rrect = plate.getPlatePos();		//车牌可旋转矩形位置
		string license = plate.getPlateStr();		//车牌字符串
		
		// std::cout << "\ncar id: " << license.data() << "\n";
		UartSendRead(uartFd, license.data());
	}
}

//无车牌代表定位不成功,No string代表定位成功但字符分割失败
