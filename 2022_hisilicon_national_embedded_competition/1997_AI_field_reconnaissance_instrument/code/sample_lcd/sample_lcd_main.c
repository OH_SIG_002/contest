#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "hi_common.h"
#include "sample_lcd.h"
#include "mpi_sys.h"
#include "./SDL/SDL.h"
#include "./SDL/SDL_ttf.h"
/******************************************************************************
* function    : main()
* Description : main
******************************************************************************/
#ifdef __HuaweiLite__
int app_main(int argc, char *argv[]);
#else


int main(int argc, char *argv[])
#endif
{
    HI_S32 s32Ret = HI_FAILURE;
    /* MIPI is GPIO55, Turn on the backlight of the LCD screen */
	system("cd /sys/class/gpio/;echo 55 > export;echo out > gpio55/direction;echo 1 > gpio55/value");
    s32Ret = SAMPLE_VIO_VPSS_VO_MIPI();
    return s32Ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
