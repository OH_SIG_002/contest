#ifndef SAMPLE_NNIE_MAIN_H
#define SAMPLE_NNIE_MAIN_H


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */
#include "hi_type.h"

#include <sample_comm_nnie.h>

/******************************************************************************
    模型配置参数
******************************************************************************/
typedef struct RfcnCfg {
    uint32_t classNum;
}   RfcnCfg;

typedef struct Yolo1Cfg {
    uint32_t classNum;
    uint32_t gridNumWidth;
    uint32_t gridNumHeight;
}   Yolo1Cfg;

extern RfcnCfg g_rfcnCfg;
extern Yolo1Cfg g_yolo1Cfg;

/*cnn para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stCnnModel;
static SAMPLE_SVP_NNIE_PARAM_S s_stCnnNnieParam;
static SAMPLE_SVP_NNIE_CNN_SOFTWARE_PARAM_S s_stCnnSoftwareParam;
/*segment para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stSegnetModel;
static SAMPLE_SVP_NNIE_PARAM_S s_stSegnetNnieParam;
/*fasterrcnn para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stFasterRcnnModel;
static SAMPLE_SVP_NNIE_PARAM_S s_stFasterRcnnNnieParam;
static SAMPLE_SVP_NNIE_FASTERRCNN_SOFTWARE_PARAM_S s_stFasterRcnnSoftwareParam;
static SAMPLE_SVP_NNIE_NET_TYPE_E s_enNetType;
/*rfcn para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stRfcnModel;
static SAMPLE_SVP_NNIE_PARAM_S s_stRfcnNnieParam;
static SAMPLE_SVP_NNIE_RFCN_SOFTWARE_PARAM_S s_stRfcnSoftwareParam;
static SAMPLE_IVE_SWITCH_S s_stRfcnSwitch = {HI_FALSE,HI_FALSE};
static HI_BOOL s_bNnieStopSignal = HI_FALSE;
static pthread_t s_hNnieThread;
static SAMPLE_VI_CONFIG_S s_stViConfig;

/*ssd para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stSsdModel;
static SAMPLE_SVP_NNIE_PARAM_S s_stSsdNnieParam;
static SAMPLE_SVP_NNIE_SSD_SOFTWARE_PARAM_S s_stSsdSoftwareParam;
/*yolov1 para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stYolov1Model;
static SAMPLE_SVP_NNIE_PARAM_S s_stYolov1NnieParam;
static SAMPLE_SVP_NNIE_YOLOV1_SOFTWARE_PARAM_S s_stYolov1SoftwareParam;
/*yolov2 para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stYolov2Model;
static SAMPLE_SVP_NNIE_PARAM_S s_stYolov2NnieParam;
static SAMPLE_SVP_NNIE_YOLOV2_SOFTWARE_PARAM_S s_stYolov2SoftwareParam;
/*yolov3 para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stYolov3Model;
static SAMPLE_SVP_NNIE_PARAM_S s_stYolov3NnieParam;
static SAMPLE_SVP_NNIE_YOLOV3_SOFTWARE_PARAM_S s_stYolov3SoftwareParam;
/*lstm para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stLstmModel;
static SAMPLE_SVP_NNIE_PARAM_S s_stLstmNnieParam;
/*pvanet para*/
static SAMPLE_SVP_NNIE_MODEL_S s_stPvanetModel;
static SAMPLE_SVP_NNIE_PARAM_S s_stPvanetNnieParam;
static SAMPLE_SVP_NNIE_FASTERRCNN_SOFTWARE_PARAM_S s_stPvanetSoftwareParam;

#ifdef SAMPLE_SVP_NNIE_PERF_STAT
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_CLREAR()  memset(&s_stOpForwardPerfTmp,0,sizeof(s_stOpForwardPerfTmp));
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_SRC_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_GET_DIFF_TIME(s_stOpForwardPerfTmp.u64SrcFlushTime)
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_PRE_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_GET_DIFF_TIME(s_stOpForwardPerfTmp.u64PreDstFulshTime)
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_AFTER_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_GET_DIFF_TIME(s_stOpForwardPerfTmp.u64AferDstFulshTime)
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_OP_TIME() SAMPLE_SVP_NNIE_PERF_STAT_GET_DIFF_TIME(s_stOpForwardPerfTmp.u64OPTime)

/*YoloV1*/
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_SRC_FLUSH_TIME() s_stYolov1Perf.stForwardPerf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_PRE_DST_FLUSH_TIME() s_stYolov1Perf.stForwardPerf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_AFTER_DST_FLUSH_TIME() s_stYolov1Perf.stForwardPerf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_OP_TIME() s_stYolov1Perf.stForwardPerf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_SRC_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov1Perf.stGRPerf.u64SrcFlushTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_PRE_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov1Perf.stGRPerf.u64PreDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_AFTER_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov1Perf.stGRPerf.u64AferDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_OP_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov1Perf.stGRPerf.u64OPTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_PRINT() printf("Yolov1 Forward time: %llu us,GR time:%llu us,Flush time: %llu us\n",\
    s_stYolov1Perf.stForwardPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,s_stYolov1Perf.stGRPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    (s_stYolov1Perf.stForwardPerf.u64SrcFlushTime + s_stYolov1Perf.stForwardPerf.u64PreDstFulshTime + s_stYolov1Perf.stForwardPerf.u64AferDstFulshTime\
    + s_stYolov1Perf.stGRPerf.u64SrcFlushTime + s_stYolov1Perf.stGRPerf.u64PreDstFulshTime + s_stYolov1Perf.stGRPerf.u64AferDstFulshTime)/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES );
/*Yolov2*/
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_SRC_FLUSH_TIME() s_stYolov2Perf.stForwardPerf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_PRE_DST_FLUSH_TIME() s_stYolov2Perf.stForwardPerf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_AFTER_DST_FLUSH_TIME() s_stYolov2Perf.stForwardPerf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_OP_TIME() s_stYolov2Perf.stForwardPerf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_SRC_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov2Perf.stGRPerf.u64SrcFlushTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_PRE_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov2Perf.stGRPerf.u64PreDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_AFTER_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov2Perf.stGRPerf.u64AferDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_OP_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stYolov2Perf.stGRPerf.u64OPTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_PRINT() printf("Yolov2 Forward time: %llu us,GR time:%llu us,Flush time: %llu us\n",\
    s_stYolov2Perf.stForwardPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,s_stYolov2Perf.stGRPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    (s_stYolov2Perf.stForwardPerf.u64SrcFlushTime + s_stYolov2Perf.stForwardPerf.u64PreDstFulshTime + s_stYolov2Perf.stForwardPerf.u64AferDstFulshTime\
    + s_stYolov2Perf.stGRPerf.u64SrcFlushTime + s_stYolov2Perf.stGRPerf.u64PreDstFulshTime + s_stYolov2Perf.stGRPerf.u64AferDstFulshTime)/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES );

/*SSD*/
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_SRC_FLUSH_TIME() s_stSsdPerf.stForwardPerf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_PRE_DST_FLUSH_TIME() s_stSsdPerf.stForwardPerf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_AFTER_DST_FLUSH_TIME() s_stSsdPerf.stForwardPerf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_OP_TIME() s_stSsdPerf.stForwardPerf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_SRC_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stSsdPerf.stGRPerf.u64SrcFlushTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_PRE_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stSsdPerf.stGRPerf.u64PreDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_AFTER_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stSsdPerf.stGRPerf.u64AferDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_OP_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stSsdPerf.stGRPerf.u64OPTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_PRINT() printf("SSD Forward time: %llu us,GR time:%llu us,Flush time: %llu us\n",\
    s_stSsdPerf.stForwardPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,s_stSsdPerf.stGRPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    (s_stSsdPerf.stForwardPerf.u64SrcFlushTime + s_stSsdPerf.stForwardPerf.u64PreDstFulshTime + s_stSsdPerf.stForwardPerf.u64AferDstFulshTime\
    + s_stSsdPerf.stGRPerf.u64SrcFlushTime + s_stSsdPerf.stGRPerf.u64PreDstFulshTime + s_stSsdPerf.stGRPerf.u64AferDstFulshTime)/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES );

/*Pvanet*/
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_SRC_FLUSH_TIME() s_stPvanetPerf.stForwardPerf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_PRE_DST_FLUSH_TIME() s_stPvanetPerf.stForwardPerf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_AFTER_DST_FLUSH_TIME() s_stPvanetPerf.stForwardPerf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_OP_TIME() s_stPvanetPerf.stForwardPerf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;

#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_SRC_FLUSH_TIME() s_stPvanetPerf.stRpnPerf.u64SrcFlushTime += g_stOpRpnPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_PRE_DST_FLUSH_TIME() s_stPvanetPerf.stRpnPerf.u64PreDstFulshTime += g_stOpRpnPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_AFTER_DST_FLUSH_TIME() s_stPvanetPerf.stRpnPerf.u64AferDstFulshTime += g_stOpRpnPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_OP_TIME() s_stPvanetPerf.stRpnPerf.u64OPTime += g_stOpRpnPerfTmp.u64OPTime;

#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_SRC_FLUSH_TIME() s_stPvanetPerf.stRoiPoolingPerf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_PRE_DST_FLUSH_TIME() s_stPvanetPerf.stRoiPoolingPerf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_AFTER_DST_FLUSH_TIME() s_stPvanetPerf.stRoiPoolingPerf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_OP_TIME() s_stPvanetPerf.stRoiPoolingPerf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;

#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_SRC_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stPvanetPerf.stGRPerf.u64SrcFlushTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_PRE_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stPvanetPerf.stGRPerf.u64PreDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_AFTER_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stPvanetPerf.stGRPerf.u64AferDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_OP_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stPvanetPerf.stGRPerf.u64OPTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_PRINT() printf("Pvanet Forward time: %llu us,Rpn time:%llu us,RoiPooling time:%llu us,GR time:%llu us,Flush time: %llu us\n",\
    s_stPvanetPerf.stForwardPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,s_stPvanetPerf.stRpnPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    s_stPvanetPerf.stRoiPoolingPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES, s_stPvanetPerf.stGRPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    (s_stPvanetPerf.stForwardPerf.u64SrcFlushTime + s_stPvanetPerf.stForwardPerf.u64PreDstFulshTime + s_stPvanetPerf.stForwardPerf.u64AferDstFulshTime\
    + s_stPvanetPerf.stRpnPerf.u64SrcFlushTime + s_stPvanetPerf.stRpnPerf.u64PreDstFulshTime + s_stPvanetPerf.stRpnPerf.u64AferDstFulshTime\
    + s_stPvanetPerf.stRoiPoolingPerf.u64SrcFlushTime + s_stPvanetPerf.stRoiPoolingPerf.u64PreDstFulshTime + s_stPvanetPerf.stRoiPoolingPerf.u64AferDstFulshTime\
    + s_stPvanetPerf.stGRPerf.u64SrcFlushTime + s_stPvanetPerf.stGRPerf.u64PreDstFulshTime + s_stPvanetPerf.stGRPerf.u64AferDstFulshTime)/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES );

/*RFCN*/
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_SRC_FLUSH_TIME() s_stRfcnPerf.stForwardPerf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_PRE_DST_FLUSH_TIME() s_stRfcnPerf.stForwardPerf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_AFTER_DST_FLUSH_TIME() s_stRfcnPerf.stForwardPerf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_OP_TIME() s_stRfcnPerf.stForwardPerf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_SRC_FLUSH_TIME() s_stRfcnPerf.stRpnPerf.u64SrcFlushTime += g_stOpRpnPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_PRE_DST_FLUSH_TIME() s_stRfcnPerf.stRpnPerf.u64PreDstFulshTime += g_stOpRpnPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_AFTER_DST_FLUSH_TIME() s_stRfcnPerf.stRpnPerf.u64AferDstFulshTime += g_stOpRpnPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_OP_TIME() s_stRfcnPerf.stRpnPerf.u64OPTime += g_stOpRpnPerfTmp.u64OPTime;

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_SRC_FLUSH_TIME() s_stRfcnPerf.stPsRoiPooling1Perf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_PRE_DST_FLUSH_TIME() s_stRfcnPerf.stPsRoiPooling1Perf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_AFTER_DST_FLUSH_TIME() s_stRfcnPerf.stPsRoiPooling1Perf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_OP_TIME() s_stRfcnPerf.stPsRoiPooling1Perf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_SRC_FLUSH_TIME() s_stRfcnPerf.stPsRoiPooling2Perf.u64SrcFlushTime += s_stOpForwardPerfTmp.u64SrcFlushTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_PRE_DST_FLUSH_TIME() s_stRfcnPerf.stPsRoiPooling2Perf.u64PreDstFulshTime += s_stOpForwardPerfTmp.u64PreDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_AFTER_DST_FLUSH_TIME() s_stRfcnPerf.stPsRoiPooling2Perf.u64AferDstFulshTime += s_stOpForwardPerfTmp.u64AferDstFulshTime;
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_OP_TIME() s_stRfcnPerf.stPsRoiPooling2Perf.u64OPTime += s_stOpForwardPerfTmp.u64OPTime;

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_SRC_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stRfcnPerf.stGRPerf.u64SrcFlushTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_PRE_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stRfcnPerf.stGRPerf.u64PreDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_AFTER_DST_FLUSH_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stRfcnPerf.stGRPerf.u64AferDstFulshTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_OP_TIME() SAMPLE_SVP_NNIE_PERF_STAT_ADD_DIFF_TIME(s_stRfcnPerf.stGRPerf.u64OPTime);
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PRINT_VITOVO() printf("Rfcn_ViToVo Forward time: %llu us,Rpn time:%llu us,PsRoiPooling1 time:%llu us,PsRoiPooling2 time:%llu us,GR time:%llu us,Flush time: %llu us\n",\
    s_stRfcnPerf.stForwardPerf.u64OPTime,s_stRfcnPerf.stRpnPerf.u64OPTime,\
    s_stRfcnPerf.stPsRoiPooling1Perf.u64OPTime, s_stRfcnPerf.stPsRoiPooling2Perf.u64OPTime,s_stRfcnPerf.stGRPerf.u64OPTime,\
    (s_stRfcnPerf.stForwardPerf.u64SrcFlushTime + s_stRfcnPerf.stForwardPerf.u64PreDstFulshTime + s_stRfcnPerf.stForwardPerf.u64AferDstFulshTime\
    + s_stRfcnPerf.stRpnPerf.u64SrcFlushTime + s_stRfcnPerf.stRpnPerf.u64PreDstFulshTime + s_stRfcnPerf.stRpnPerf.u64AferDstFulshTime\
    + s_stRfcnPerf.stPsRoiPooling1Perf.u64SrcFlushTime + s_stRfcnPerf.stPsRoiPooling1Perf.u64PreDstFulshTime + s_stRfcnPerf.stPsRoiPooling1Perf.u64AferDstFulshTime\
    + s_stRfcnPerf.stPsRoiPooling2Perf.u64SrcFlushTime + s_stRfcnPerf.stPsRoiPooling2Perf.u64PreDstFulshTime + s_stRfcnPerf.stPsRoiPooling2Perf.u64AferDstFulshTime\
    + s_stRfcnPerf.stGRPerf.u64SrcFlushTime + s_stRfcnPerf.stGRPerf.u64PreDstFulshTime + s_stRfcnPerf.stGRPerf.u64AferDstFulshTime));

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PRINT_FILE() printf("Rfcn_File Forward time: %llu us,Rpn time:%llu us,PsRoiPooling1 time:%llu us,PsRoiPooling2 time:%llu us,GR time:%llu us,Flush time: %llu us\n",\
    s_stRfcnPerf.stForwardPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,s_stRfcnPerf.stRpnPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    s_stRfcnPerf.stPsRoiPooling1Perf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,s_stRfcnPerf.stPsRoiPooling2Perf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    s_stRfcnPerf.stGRPerf.u64OPTime/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES,\
    (s_stRfcnPerf.stForwardPerf.u64SrcFlushTime + s_stRfcnPerf.stForwardPerf.u64PreDstFulshTime + s_stRfcnPerf.stForwardPerf.u64AferDstFulshTime\
    + s_stRfcnPerf.stRpnPerf.u64SrcFlushTime + s_stRfcnPerf.stRpnPerf.u64PreDstFulshTime + s_stRfcnPerf.stRpnPerf.u64AferDstFulshTime\
    + s_stRfcnPerf.stPsRoiPooling1Perf.u64SrcFlushTime + s_stRfcnPerf.stPsRoiPooling1Perf.u64PreDstFulshTime + s_stRfcnPerf.stPsRoiPooling1Perf.u64AferDstFulshTime\
    + s_stRfcnPerf.stPsRoiPooling2Perf.u64SrcFlushTime + s_stRfcnPerf.stPsRoiPooling2Perf.u64PreDstFulshTime + s_stRfcnPerf.stPsRoiPooling2Perf.u64AferDstFulshTime\
    + s_stRfcnPerf.stGRPerf.u64SrcFlushTime + s_stRfcnPerf.stGRPerf.u64PreDstFulshTime + s_stRfcnPerf.stGRPerf.u64AferDstFulshTime)/SAMPLE_SVP_NNIE_PERF_STAT_LOOP_TIMES);



static SAMPLE_SVP_NNIE_YOLO_PERF_STAT_S s_stYolov1Perf = {0};
static SAMPLE_SVP_NNIE_YOLO_PERF_STAT_S s_stYolov2Perf = {0};
static SAMPLE_SVP_NNIE_SSD_PERF_STAT_S  s_stSsdPerf = {0};
static SAMPLE_SVP_NNIE_PVANET_PERF_STAT_S s_stPvanetPerf = {0};
static SAMPLE_SVP_NNIE_RFCN_PERF_STAT_S s_stRfcnPerf = {0};

static SAMPLE_SVP_NNIE_OP_PERF_STAT_S   s_stOpForwardPerfTmp = {0};
extern SAMPLE_SVP_NNIE_OP_PERF_STAT_S   g_stOpRpnPerfTmp;
#else

#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_CLREAR()
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_OP_FORWARD_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_FORWARD_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_GR_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV1_PRINT()

/*Yolov2*/
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_FORWARD_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_GR_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_YOLOV2_PRINT()
/*SSD*/
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_FORWARD_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_GR_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_SSD_PRINT()

/*Pvanet*/
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_FORWARD_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_RPN_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_ROIPOOLING_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_GR_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_PVANET_PRINT()

/*RFCN*/
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_FORWARD_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_RPN_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING1_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PSROIPOOLING2_OP_TIME()

#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_SRC_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_PRE_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_AFTER_DST_FLUSH_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_GR_OP_TIME()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PRINT_VITOVO()
#define SAMPLE_SVP_NNIE_PERF_STAT_RFCN_PRINT_FILE()

#endif

/******************************************************************************
* function : show Cnn sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Cnn(void);

/******************************************************************************
* function : show Segnet sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Segnet(void);

/******************************************************************************
* function : show FasterRcnn sample
******************************************************************************/
void SAMPLE_SVP_NNIE_FasterRcnn(void);

/******************************************************************************
* function : show fasterrcnn double roipooling sample
******************************************************************************/
void SAMPLE_SVP_NNIE_FasterRcnn_DoubleRoiPooling(void);

/******************************************************************************
* function : show RFCN sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Rfcn(void);
/******************************************************************************
* function : Rfcn Read file
******************************************************************************/
void SAMPLE_SVP_NNIE_Rfcn_File(void);
/******************************************************************************
* function : show SSD sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Ssd(void);

/******************************************************************************
* function : show YOLOV1 sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Yolov1(void);

/******************************************************************************
* function : show YOLOV2 sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Yolov2(void);

/******************************************************************************
* function : show YOLOV3 sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Yolov3(void);

/******************************************************************************
* function : show Lstm sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Lstm(void);

/******************************************************************************
* function : show Pvanet FasterRcnn sample
******************************************************************************/
void SAMPLE_SVP_NNIE_Pvanet(void);



/******************************************************************************
* function : Cnn sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Cnn_HandleSig(void);

/******************************************************************************
* function : Segnet sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Segnet_HandleSig(void);

/******************************************************************************
* function : fasterRcnn sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_FasterRcnn_HandleSig(void);

/******************************************************************************
* function : rfcn sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Rfcn_HandleSig(void);

/******************************************************************************
* function : rfcn sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Rfcn_HandleSig_File(void);
/******************************************************************************
* function : SSD sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Ssd_HandleSig(void);

/******************************************************************************
* function : Yolov1 sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Yolov1_HandleSig(void);

/******************************************************************************
* function : Yolov2 sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Yolov2_HandleSig(void);

/******************************************************************************
* function : Yolov3 sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Yolov3_HandleSig(void);

/******************************************************************************
* function : Lstm sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Lstm_HandleSig(void);


/******************************************************************************
* function : Pvanet sample signal handle
******************************************************************************/
void SAMPLE_SVP_NNIE_Pvanet_HandleSig(void);

/******************************************************************************
* function : Cnn init
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_Cnn_ParamInit(SAMPLE_SVP_NNIE_CFG_S* pstNnieCfg,
    SAMPLE_SVP_NNIE_PARAM_S *pstCnnPara, SAMPLE_SVP_NNIE_CNN_SOFTWARE_PARAM_S* pstCnnSoftWarePara);

/******************************************************************************
* function : Cnn Deinit
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_Cnn_Deinit(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam,
    SAMPLE_SVP_NNIE_CNN_SOFTWARE_PARAM_S* pstSoftWareParam,SAMPLE_SVP_NNIE_MODEL_S* pstNnieModel);

/******************************************************************************
* function : Fill Src Data
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_FillSrcData(SAMPLE_SVP_NNIE_CFG_S* pstNnieCfg,
    SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam, SAMPLE_SVP_NNIE_INPUT_DATA_INDEX_S* pstInputDataIdx);

/******************************************************************************
* function : Rfcn init
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_Rfcn_ParamInit(SAMPLE_SVP_NNIE_CFG_S* pstCfg,
    SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam, SAMPLE_SVP_NNIE_RFCN_SOFTWARE_PARAM_S* pstSoftWareParam);

/******************************************************************************
* function : Rfcn Deinit
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_Rfcn_Deinit(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam,
    SAMPLE_SVP_NNIE_RFCN_SOFTWARE_PARAM_S* pstSoftWareParam,SAMPLE_SVP_NNIE_MODEL_S *pstNnieModel);

/******************************************************************************
* function : Rfcn Proc
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_Rfcn_Proc(SAMPLE_SVP_NNIE_PARAM_S *pstParam,
    SAMPLE_SVP_NNIE_RFCN_SOFTWARE_PARAM_S *pstSwParam);

/******************************************************************************
* function : Yolov1 init
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_Yolov1_ParamInit(SAMPLE_SVP_NNIE_CFG_S* pstCfg,
    SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam, SAMPLE_SVP_NNIE_YOLOV1_SOFTWARE_PARAM_S* pstSoftWareParam);

/******************************************************************************
* function : Yolov1 Deinit
******************************************************************************/
extern HI_S32 SAMPLE_SVP_NNIE_Yolov1_Deinit(SAMPLE_SVP_NNIE_PARAM_S *pstNnieParam,
    SAMPLE_SVP_NNIE_YOLOV1_SOFTWARE_PARAM_S* pstSoftWareParam,SAMPLE_SVP_NNIE_MODEL_S *pstNnieModel);

void  SAMPLE_SVP_NNIE_Yolov3_Vivo(void);
void SAMPLE_SVP_NNIE_Yolov3_Vivo_HandleSig(void);
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __SAMPLE_SVP_MAIN_H__ */
