# 基于AI视觉的电梯内电动车识别报警系统

## 1. 概述

这是一个应用于电梯内的对电动车进行识别的系统，在Taurus端部署了通过自制数据集训练后的Wk模型，并且通过串口互联方式将识别后的结果数据字段输出到Pegasus端，Pegasus具有报警灯闪烁和屏幕信息显示功能。

## 2.目录

```
│  hi3516_code                    
  ├─README.md
  ├─hand_classify.c
  ├─hand_classify.h
  ├─yolov2_ebike_detect.c
  └─yolov2_ebike_detect.h
│  hi3861_code
  ├─app_demo_uart.c
  ├─app_demo_uart.h
  ├─README.md
  ├─BUILD.gn
  ├─hal_iot_gpio.ex.c
  ├─hisignalling_protocol.c
  ├─hisignalling_protocol.h
  ├─iot_gpio.ex.h
  ├─oled_demo.c
  ├─oled_ssd1306.c
  ├─oled_fonts.h
  ├─oled_ssd1306.h
```

