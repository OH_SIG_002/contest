#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "string.h"
#include "iot_gpio_ex.h"
#include "iot_gpio.h"
#include "iot_uart.h"
#include "iot_errno.h"
#include <hi_gpio.h>
#include <hi_io.h>
#include <hi_uart.h>

unsigned char transmit_data[] = "UART TEST\r\n";

static void *UartTask(const char *arg){
    (void)arg;
    unsigned int ret = 0;
    IoTGpioInit(IOT_IO_NAME_GPIO_0);
    IoTGpioInit(IOT_IO_NAME_GPIO_1);
    IoSetFunc(IOT_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(IOT_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
    IotUartAttribute uart_attr;
    uart_attr.baudRate = 9600;
    uart_attr.dataBits = 8;
    uart_attr.stopBits = 1;
    uart_attr.parity = 0;
    ret = IoTUartInit(1, &uart_attr);
    if(ret!=IOT_SUCCESS){
        printf("Failed to init uart\r\n");
        return;
    }
    printf("UART init Success\r\n");
    while(1){
        IoTUartWrite(HI_UART_IDX_1,transmit_data,strlen(transmit_data));
        usleep(500000);
    }
}

static void UartTaskEntry(void){
    osThreadAttr_t attr2;
    attr2.name = "UartTask";
	attr2.attr_bits = 0U;
	attr2.cb_mem = NULL;
	attr2.cb_size = 0U;
	attr2.stack_mem = NULL;
	attr2.stack_size = 4096;
	attr2.priority = 25;

    if (osThreadNew((osThreadFunc_t)UartTask, NULL, &attr2) == NULL)
	{
		printf("Falied to create UartTask!\n");
	}
}
SYS_RUN(UartTaskEntry);